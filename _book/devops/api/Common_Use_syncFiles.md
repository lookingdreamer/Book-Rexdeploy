Common_Use_syncFiles

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    无   |
|files |  是  |    string   |    无   |
|G |  是  |    string   |    无   |
|random |  是  |    string   |    无   |

**请求示例**     

`action=Common:Use:syncFiles&G=group&files=/data/RexdeployV3/config/config.yml:/tmp/config.yml&random=syncFiles`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
     }, 
     "param" : { //请求参数
          "G" : "group",  //执行主机分组,通过rex -T查询,通过config/iplists.ini定义
          "action" : "Common:Use:syncFiles",  //请求模块
          "files" : "/data/RexdeployV3/config/config.yml:/tmp/config.yml", 
          "random" : "syncFiles",  //随机数
          "token" : "1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "-G "group"",  //请求参数前缀
          "requestCmd" : "Common:Use:syncFiles,--files="/data/RexdeployV3/config/config.yml:/tmp/config.yml",--random="syncFiles",--token="1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF -G "group" Common:Use:syncFiles --files="/data/RexdeployV3/config/config.yml:/tmp/config.yml" --random="syncFiles" --token="1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "4"  //执行命令花费时间
 }

```

result


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    无   |
|random |  是  |    string   |    无   |
|delete |  是  |    int   |    无   |


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "1", 
          "count" : "2",  //返回总数量
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "changeFile" : { 
                              }, 
                              "code" : "0",  //0 成功 非0 失败
                              "nochangeFile" : { 
                                   "0" : "/tmp/config.yml",  //返回第1个数据
                              }, 
                              "params" : { 
                                   "files" : "/data/RexdeployV3/config/config.yml:/tmp/config.yml" 
                              }, 
                              "server" : "172.17.0.2"  //操作服务器地址
                         }, 
                    }, 
                    "msg" : "同步成功" 
               }, 
               "1" : {  //返回第2个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "changeFile" : { 
                              }, 
                              "code" : "0",  //0 成功 非0 失败
                              "nochangeFile" : { 
                                   "0" : "/tmp/config.yml",  //返回第1个数据
                              }, 
                              "params" : { 
                                   "files" : "/data/RexdeployV3/config/config.yml:/tmp/config.yml" 
                              }, 
                              "server" : "172.17.0.3"  //操作服务器地址
                         }, 
                    }, 
                    "msg" : "同步成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { //请求参数
          "action" : "json",  //请求模块
          "delete" : "1",  //Common:Use:getJson获取结果之后是否删除内存中保存的数据,为1时删除
          "random" : "syncFiles",  //随机数
          "token" : "1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "json,--delete="1",--random="syncFiles",--token="1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  json --delete="1" --random="syncFiles" --token="1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "1"  //执行命令花费时间
 }

```