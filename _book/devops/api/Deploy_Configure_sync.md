Deploy_Configure_sync

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    无   |
|k |  是  |    string   |    无   |

**请求示例**     

`action=Deploy:Configure:sync&k=server`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "conf" : { 
                         "0" : {  //返回第1个数据
                              "app_key" : "server1",  //返回服务器唯一名字
                              "auto_deloy" : "1", 
                              "backupdir_same_level" : "0", 
                              "checkdir" : "test,b.txt", 
                              "checkurl_status" : "1", 
                              "config_dir" : "/data/www/config1", 
                              "configure_file_list" : { 
                              }, 
                              "configure_file_status" : "0", 
                              "container_dir" : "/usr/local/tomcat-server1", 
                              "cpu" : "4核", 
                              "created_time" : "2018-03-21 10:29:50", 
                              "depart_name" : "", 
                              "deploydir_same_level" : "0", 
                              "differcount" : "{"app_key":"server1","prodiffercount":"0,0,0,0","confdiffercount":"0,0,0,0"}", 
                              "disk" : "100G(本地盘)", 
                              "entrance_server" : "", 
                              "env" : "test", 
                              "external_ip" : "127.0.0.1", 
                              "groupby" : "server", 
                              "header" : "Content-Type:application/json", 
                              "huanjin_name" : "", 
                              "id" : "1", 
                              "is_deloy_dir" : "2", 
                              "is_restart_status" : "0", 
                              "java_confirm" : "", 
                              "java_home" : "", 
                              "java_versoin" : "", 
                              "jifang_name" : { 
                              }, 
                              "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                              "local_conf_cmd" : "time=$(date '+%s') &&  njsver="1.1.${time}" && jsver=$(cat config.properties |grep -n  'jsver' |head -n 1 | grep -P '\d{1,}.\d{1,}.\d{1,}' -o) && sed -i "s/${jsver}/${njsver}/" config.properties ", 
                              "local_name" : "server", 
                              "local_pro_cmd" : { 
                              }, 
                              "log_dir" : "/data/log/server1", 
                              "logfile" : "catalina.#%Y-%m-%d.log", 
                              "mem" : "8GB", 
                              "mirr_id" : { 
                              }, 
                              "network_ip" : "192.168.0.207", 
                              "note" : "", 
                              "params" : "{"cmd":"uptime"}", 
                              "predir" : { 
                              }, 
                              "pro_dir" : "/data/www/html1", 
                              "pro_init" : "/etc/init.d/tomcat-server1", 
                              "pro_key" : "tomcat-server1", 
                              "pro_port" : "6080", 
                              "pro_type" : "tomcat", 
                              "require" : "inputStr", 
                              "requirecode" : "200", 
                              "server_id" : { 
                              }, 
                              "server_name" : "后台服务集群1", 
                              "status" : "1", 
                              "system_type" : "centos7.1", 
                              "updated_time" : "2018-03-21 10:29:50", 
                              "url" : "http://127.0.0.1:6080/packController/runCmd.do?cmd=uptime", 
                              "weight" : "10,10", 
                              "yewu_name" : "" 
                         }, 
                         "1" : {  //返回第2个数据
                              "app_key" : "server2",  //返回服务器唯一名字
                              "auto_deloy" : "1", 
                              "backupdir_same_level" : "0", 
                              "checkdir" : "test,b.txt", 
                              "checkurl_status" : "1", 
                              "config_dir" : "/data/www/config2", 
                              "configure_file_list" : { 
                              }, 
                              "configure_file_status" : "0", 
                              "container_dir" : "/usr/local/tomcat-server2", 
                              "cpu" : "4核", 
                              "created_time" : "2018-03-21 10:30:15", 
                              "depart_name" : "", 
                              "deploydir_same_level" : "0", 
                              "differcount" : "{"app_key":"server2","prodiffercount":"0,0,0,0","confdiffercount":"0,0,0,0"}", 
                              "disk" : "100G(本地盘)", 
                              "entrance_server" : "", 
                              "env" : "test", 
                              "external_ip" : "127.0.0.1", 
                              "groupby" : "server", 
                              "header" : "Content-Type:application/json", 
                              "huanjin_name" : "", 
                              "id" : "2", 
                              "is_deloy_dir" : "2", 
                              "is_restart_status" : "0", 
                              "java_confirm" : "", 
                              "java_home" : "", 
                              "java_versoin" : "", 
                              "jifang_name" : { 
                              }, 
                              "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                              "local_conf_cmd" : "time=$(date '+%s') &&  njsver="1.1.${time}" && jsver=$(cat config.properties |grep -n  'jsver' |head -n 1 | grep -P '\d{1,}.\d{1,}.\d{1,}' -o) && sed -i "s/${jsver}/${njsver}/" config.properties ", 
                              "local_name" : "server", 
                              "local_pro_cmd" : { 
                              }, 
                              "log_dir" : "/data/log/server2", 
                              "logfile" : "catalina.#%Y-%m-%d.log", 
                              "mem" : "8GB", 
                              "mirr_id" : { 
                              }, 
                              "network_ip" : "192.168.0.76", 
                              "note" : "", 
                              "params" : "{"cmd":"uptime"}", 
                              "predir" : { 
                              }, 
                              "pro_dir" : "/data/www/html2", 
                              "pro_init" : "/etc/init.d/tomcat-server2", 
                              "pro_key" : "tomcat-server2", 
                              "pro_port" : "7080", 
                              "pro_type" : "tomcat", 
                              "require" : "inputStr", 
                              "requirecode" : "200", 
                              "server_id" : { 
                              }, 
                              "server_name" : "后台服务集群2", 
                              "status" : "1", 
                              "system_type" : "centos7.1", 
                              "updated_time" : "2018-03-21 10:30:15", 
                              "url" : "http://127.0.0.1:7080/packController/runCmd.do?cmd=uptime", 
                              "weight" : "10,10", 
                              "yewu_name" : "" 
                         }, 
                    }, 
                    "msg" : "执行配置同步成功" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Deploy:Configure:sync",  //请求模块
          "k" : "server",  //名字
          "token" : "1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Deploy:Configure:sync,--k="server",--token="1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Deploy:Configure:sync --k="server" --token="1529935960lkrlxtkmreaeoowfkbnwtpdsybrgnhgf" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "11"  //执行命令花费时间
 }

```
