# 安装

该自动化平台只需要安装MYSQL以及perl语言对应的模块，甚至不安装MYSQL也可以使用大部分的功能，开箱即用。

## 说明

> docker的安装方式默认带了4个模拟客户端，其他的安装方式只安装核心应用，4个模拟客户端只是方便测试使用，对应用安装没有影响。4个模拟客户端在文档中都用作案例使用（仅体验核心应用的话，可以试用centos平台安装）

4个客户端模拟拓扑图
![](/assets/servermap.png)

## 4个模拟客户端信息
<style type="text/css">
        .markdown-section {
            overflow: auto;   
        }
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>


| 唯一名字 | 服务名 | 地址 | 应用类型 | 配置文件路径 | 工程文件路径 | 进程关键词 | 应用启动脚本 | 分组名称 | 
| :---: | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :---: | 
| server1 | 后台服务集群1 | 127.0.0.1 | tomcat | /data/www/config1 | /data/www/html1 | tomcat-server1 | /etc/init.d/tomcat-server1 | server | 
| server2 | 后台服务集群2 | 127.0.0.1 | tomcat | /data/www/config2 | /data/www/html2 | tomcat-server2 | /etc/init.d/tomcat-server2 | server | 
| flow1 | 调度服务集群1 | 127.0.0.1 | tomcat | /data/www/flow1/WEB-INF/classes/ | /data/www/flow1 | tomcat-flow1 | /etc/init.d/tomcat-flow1 | flow |
| flow2 | 调度服务集群2 | 127.0.0.1 | tomcat | /data/www/flow2/WEB-INF/classes/ | /data/www/flow2 | tomcat-flow2 | /etc/init.d/tomcat-flow2 | flow |


