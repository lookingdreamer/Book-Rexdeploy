#升级Rexdeploy

如果你已经安装了旧版本，可以采用手动升级方法升级到最新即可

* 手动升级

##### 第1步 初始化模块

```bash
#安装依赖包
wget  https://download.osichina.net/tools/cpanm -O /usr/bin/cpanm; chmod +x /usr/bin/cpanm
cpanm Archive::Zip LWP::Protocol::https JSON  DBD::SQLite  DBIx::Simple SQL::Abstract IPC::System::Options

#查询Rex模块安装路径
perldoc -l Rex::Commands 
##/usr/local/Cellar/perl518/5.18.2/lib/site_perl/5.18.2/Rex/Commands.pm
##对应Rex模块的路径为 /usr/local/Cellar/perl518/5.18.2/lib/site_perl/5.18.2/Rex

cd RexDeployV3
\cp src/Rex/* /usr/local/Cellar/perl518/5.18.2/lib/site_perl/5.18.2/Rex/ -ar
```

#####  第2步 灌入更新数据并修改配置

(1) 备份RexdeployV3目录的 lib 、 config 、Rexfile 这个2个目录和1个文件

(2) 修改对应的数据库配置
文件路径: `lib/Deploy/Db/__module__.pm` 和 `lib/Deploy/Core/__module__.pm` 

(3) 执行更新的SQL文件
数据库文件路径: `install/sql/update_2018_0503.sql` 

(4) 对比原配置文件，修改或者新增对应的配置项
配置文件路径: `config/config.yml` 
灰度发布相关可自定义，具体参照下面章节的灰度发布配置项

##### 第3步  验证
初始化配置之后执行 `cd RexDeployV3 && rex -T`, 看到正常的返回提示，则证明安装已经成功.

##### 第4步. 数据录入和校验
如果是灰度发布,则需要录入以下数据   
灰度发布需要录入灰度相关数据（关于数据库字段的解释，可以参照SQL的注释）   
录入示例 (灰度相关需要插入3条相关数据)

(1).插入负载均衡ID

```
UPDATE pre_server_detail
SET loadBalancerId = '负载均衡id1,负载均衡id2,负载均衡id..'
WHERE
    app_key IN ('server1', 'server2');
```
> 如果你的集群绑定多了负载均衡，loadBalancerId 可以添加多个，以英文,分割。loadBalancerId 可以在腾讯云负载均衡控制台查询。因为有可能1个应用绑定了内网负载和外网负载或者更多。

(2).插入URL校验数据
```sql
UPDATE pre_server_detail
SET
 url = 'http://127.0.0.1:6080/packController/runCmd.do?cmd=uptime',
 header = 'Content-Type:application/json',
 params = '{"cmd":"uptime"}',
 `require` = 'inputStr',
 requirecode = '200'
WHERE
    app_key = 'server1';

UPDATE pre_server_detail
SET
 url = 'http://127.0.0.1:7080/packController/runCmd.do?cmd=uptime',
 header = 'Content-Type:application/json',
 params = '{"cmd":"uptime"}',
 `require` = 'inputStr',
 requirecode = '200'
WHERE
    app_key = 'server2';
```
> 这个数据主要是发布完成后校验url，只有校验url正常之后才能把节点加上去。
> 其中当params 不传时为get请求，当params 有参数为post请求。header有多个时用分号;间隔。
> require的意思是返回内容校验，requirecode 返回状态码校验。如上示例，校验返回内容是否有inputStr
> 以及返回状态是否为200，如果其中任何1个不成立，该灰度发布就会终止。不会把异常的节点加上去。

(3).插入灰度发布顺序
```
INSERT INTO load_key_sorts (local_name, app_key_sort)
VALUES
    ('server','server1;server2');
```
> 指定灰度发布的顺序，如上，会先发布server1，然后再发布server2。当然可以有多个同时发。
如values为 ('server','server1 server2 server3;server5 server6 server7') ; 会先发布server1 server2 server3然后再发布server5 server6 server7。

* 自动升级
暂未支持，可以重新安装，参照: https://book.osichina.net/an-zhuang.html
