# windows平台安装

> windows上推荐使用Docker快速安装体验


## Docker方式安装

docker环境的安装，这里就不过过多介绍，不知道安装docker环境请撮: [https://get.daocloud.io/](https://get.daocloud.io/)

Tips: docker在执行docker run之后会自动进行数据初始化，大概需要30S的时间  
通过`netstat -tpln`,就可以看到启动了3306、22、873、80的端口

### Docker快速体验

#### Docker 从阿里云拉取镜像运行
```
docker pull registry.cn-hangzhou.aliyuncs.com/kaoming/rexdeployv3:3.0
docker run -it -d rexdeploy:3.0
docker ps #查询容器id 
docker exec -it [containerID] bash #使用容器ID登陆
```

进入到容器之后执行 `rex -T`, 看到以下提示，则证明安装已经成功。![](/assets/rexTt.png)

> 注意: 如果你安装的docker版本是[Docker Toolbox](https://www.docker.com/toolbox) ,那么请按照如下的方法进入容器操作。主要的原因是因为windows版本对某些特殊字符不支持，遇到特殊字符，容器就会自动退出，建议进入到宿主机进行操作。
>
> * Docker Toolbox操作如下:
>
> ```
> docker pull registry.cn-hangzhou.aliyuncs.com/kaoming/rexdeployv3:3.0
> docker run -it -d rexdeploy:3.0
> docker ps #查询容器id 
> docker-machine ls #查询容器宿主机的name
> docker-machine ssh [machine-name] #默认为default,如果你的docker-machine的name为default,可以不加该参数
> docker exec -it [containerID] bash #使用容器ID登陆
> ```









