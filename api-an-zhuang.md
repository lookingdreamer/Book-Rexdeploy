# API安装和配置

安装主要包含2大部分,一个是Rexdeploy的安装,一个是Rexdeploy API的安装。     

## API安装
### Rexdeploy的安装
参照CLI的安装方法 [https://book.osichina.net/an-zhuang.html](https://book.osichina.net/an-zhuang.html)

### Rexdeploy API 安装
如果是参照CLI的docker版本,默认就安装了API版本,以下是手动安装API的方法

#### 安装
如果是先安装的rexdeploy核心版本,比如centos下的安装。     
安装API 只需要执行以下安装命令即可     
```
cpanm JSON  DBD::SQLite  DBIx::Simple SQL::Abstract IPC::System::Options
```

#### 启动

如果是本地开发模式,建议使用morbo启动,morbo启动的时候,会自动监听本地程序文件,有修改时会自动重启,但是仅限于本地调试,因为它是单进程的

本地开发模式下启动方法: 
```
cd /data/RexdeployV3
morbo web/script/fastnotes.pl 
```

生产应用启动方法:(prefork模式)
```
cd /data/RexdeployV3
web/script/fastnotes.pl prefork -m production -w 10 -c 100
```

其中 prefork模式下参数可以自定义调整,比如启动端口,work数量,客户端最大数量,超时时间等     
通过`web/script/fastnotes.pl prefork -h`即可查询和自定义.     
生产使用强烈建议使用nginx + prefork模式 或者 nginx + gunicorn + prefork模式     

nginx 参考配置    

```
upstream myapp {
  server 127.0.0.1:3000;
}
server {
  listen 80;
  server_name localhost;
  location / {
    proxy_pass http://myapp;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }
}
```

#### 关闭
找到进程kill即可      

## 配置

api配置文件: `web/fastnotes.json`

```
    "username": "admin", //获取token的用户
    "password": "admin20180529#@!",//获取token的用户密码
    "expire": "3600",//token的有效期
    "rex": "/usr/bin/rex", //rex的绝对路径
    "pwd": "/data/RexdeployV3",//rex的执行路径
    "uploadDir":"/data/RexdeployV3/upload/web",//api上传文件的默认路径
    "downloadDir":"/data/RexdeployV3/download/web",//api下载文件的默认路径
    "rexConfig":"/data/RexdeployV3/config/config.yml",//rex的配置文件路径
    "ipListsFile":"/data/RexdeployV3/config/ip_lists.ini",//rex的goupFile路径
    "print_stdout": 0, //全局默认是否输出cmd结果 0不输出 1输出
    "precmdAllow": ["b","e","E","G","g","H","z","K","P","p","u","d","ddd","m","o","q","qw","Q","T","Tm","Tv","Ty","c","C","f","F","h","M","O","s","S","t","v","n","l"], //rex允许的前缀命令
    "allow": [ //rex执行cmd的参数限定
        {
            "action": "Common:Use:download",
            "mustParams": "dir1,dir2",
            "mustOneParams": [["H","g","G"]]
        }...
```










