# 参数详解

在之前的介绍当中，我们使用了 -t -H -G 参数 
当然除了这些参数以外，官方还提供了以下参数  

该章节讲解的参数是指全局参数(注意: 这些参数的添加位置在模块之前 rex命令之后)      

执行`rex -h`，就可以看到以下参数 

```
[root@9ad2c46f179f RexDeployV3]# rex -h
usage:
  rex [<options>] [-H <host>] [-G <group>] <task> [<task-options>]
  rex -T[m|y|v] [<string>]

  -b     Run batch
  -e     Run the given code fragment
  -E     Execute a task on the given environment
  -G|-g  Execute a task on the given server groups
  -H     Execute a task on the given hosts (space delimited)
  -z     Execute a task on hosts from this command's output

  -K     Public key file for the ssh connection
  -P     Private key file for the ssh connection
  -p     Password for the ssh connection
  -u     Username for the ssh connection

  -d     Show debug output
  -ddd   Show more debug output (includes profiling output)
  -m     Monochrome output: no colors
  -o     Output format
  -q     Quiet mode: no log output
  -qw    Quiet mode: only output warnings and errors
  -Q     Really quiet: output nothing

  -T     List tasks
  -Tm    List tasks in machine-readable format
  -Tv    List tasks verbosely
  -Ty    List tasks in YAML format

  -c     Turn cache ON
  -C     Turn cache OFF
  -f     Use this file instead of Rexfile
  -F     Force: disregard lock file
  -h     Display this help message
  -M     Load this module instead of Rexfile
  -O     Pass additional options, like CMDB path
  -s     Use sudo for every command
  -S     Password for sudo
  -t     Number of threads to use (aka 'parallelism' param)
  -v     Display (R)?ex version

[root@9ad2c46f179f RexDeployV3]#
```

## 常用参数
我就常用的参数简要的介绍，其他参数可以自行调试  

-n 指定配置文件后缀名称,比如n为test,则使用config/config_test.yml的配置文件             
-l 指定配置文件中环境变量,比如l为dev,则使用config.yml中dev的配置                 

-d 调试模式  
-F 强制忽略锁  
-t 多线程数量  
-s 使用sudo   
-S sudo的密码  
-g/G 使用分组名  
-K 公钥  
-P 私钥  
-p 用户密码  
-u 用户名  



