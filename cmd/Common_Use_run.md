# 基于IP或分组批量命令执行


## 基于IP或分组批量命令执行
- Common:Use:run

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名批量命令执行| rex -G '[group]' [module] --cmd='[cmd]' | 
|通过多个IP批量命令执行| rex -H '[IP1 IP2...]' [module] --cmd='[cmd]' | 

**示例**  
查询分组名为group的服务器的启动时间
```
rex -G group Common:Use:run --cmd="uptime"
```


**参数用法**  
分组名可以通过`rex -T`命令查询,同时在ip_lists.ini文件中设置。   
cmd: 执行命令  （必选)      


**备注**  


**截图**  
示例
![](../assets/Common_Use_run.gif)


