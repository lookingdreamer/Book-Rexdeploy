# 基于单个IP执行命令

**简要描述：** 

- 基于单个IP执行命令


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527661143jounhnkfhpsfynvkphcryehonhhlnjhm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Common:Use:run   |
|cmd |  是  |    string   |    执行命令   |
|H |  是  |    string   |    执行主机,单个   |


**请求示例**      

`H=127.0.0.1&action=Common:Use:run&cmd=uptime`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败 //0成功 非0失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "server" : "127.0.0.1",  //执行主机 //操作服务器地址
                    "stdout" : " 03:54:01 up 2 days,  4:12,  3 users,  load average: 0.12, 0.10, 0.06"  //执行返回命令 //最终执行命令标准输出
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "H" : "127.0.0.1",  //执行主机,多个以空格间隔
          "action" : "Common:Use:run",  //请求模块
          "cmd" : "uptime",  //执行命令
          "token" : "1527661143jounhnkfhpsfynvkphcryehonhhlnjhm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "-H "127.0.0.1"",  //请求参数前缀
          "requestCmd" : "Common:Use:run,--cmd="uptime",--token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF -H "127.0.0.1" Common:Use:run --cmd="uptime" --token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "3"  //执行命令花费时间
 }

```

