# 基于名字批量命令执行

**简要描述：** 

- 基于名字批量命令执行

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    run   |
|k |  是  |    string   |    名字或者名字分组 <br>参见数据库字段的app_key或local_name<br>如果多个app_key,用空格隔开   |
|cmd |  是  |    string   |    执行命令   |


**请求示例**      
`action=run&cmd=uptime&k=server`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "app_key" : "server1",  //返回服务器唯一名字
                                   "server" : "192.168.0.207",  //操作服务器地址
                                   "stdout" : " 16:13:02 up 2 days,  5:15,  0 users,  load average: 1.82, 1.42, 0.89"  //执行命令标准输出
                              }, 
                              "mainProcess" : "23308"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "app_key" : "server2",  //返回服务器唯一名字
                                   "server" : "192.168.0.76",  //操作服务器地址
                                   "stdout" : " 16:13:03 up 2 days,  5:16,  0 users,  load average: 1.82, 1.42, 0.89"  //执行命令标准输出
                              }, 
                              "mainProcess" : "23308"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "23308",  //当前命令的进程ID
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "run",  //请求模块
          "cmd" : "uptime",  //执行命令
          "k" : "server",  //名字
          "token" : "1529853651coqjifffyedvdqfnrghhphcadjgctexi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "run,--cmd="uptime",--k="server",--token="1529853651coqjifffyedvdqfnrghhphcadjgctexi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  run --cmd="uptime" --k="server" --token="1529853651coqjifffyedvdqfnrghhphcadjgctexi" --w="1"",  //执行命令
          "code" : "1", //校验命令 1成功 非1失败
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "4"  //执行命令花费时间
 }

```

