# 基于名字服务批量命令执行

## 基于名字服务批量命令执行
- run

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字批量命令执行| rex  [module] --k='[k]' --cmd='[cmd]'| 


**示例**  
查询server服务器的启动时间
```
rex run --k="server" --cmd="uptime"
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
cmd: 执行命令  （必选）        
w: 等于1,JSON格式输出结果


**备注**  


**截图**  
示例
![](../assets/run1.gif)


