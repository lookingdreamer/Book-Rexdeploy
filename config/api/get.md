# 配置查询

**简要描述：** 

- 配置查询


**请求URL：** 
- ` http://127.0.0.1:3000/rex/config?token=1531404113cstlosvrpmakarjairyckfwuxskownsp `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|confFile |  否  |    string   |    配置文件config.yml路径,不传时默认fastnotes.json中rexConfig配置   |
|section |  否  |    string   |    不传时默认查询config.yml中所有的配置   |
|key |  否  |    string   |    当传了section,key才有参考意义,多个key以,分隔   |

**请求示例** 

`section=dev&key=download_method,backup_dir`

**返回示例**

``` 
{ 
     "code" : "0", 
     "conf" : "/data/RexdeployV3/config/config.yml", 
     "data" : { 
          "data" : { 
               "0" : { 
                    "download_method" : "rsync" 
               }, 
               "1" : { 
                    "backup_dir" : "/data/backup" 
               }, 
          } 
     }, 
     "msg" : "成功", 
     "param" : { 
          "confFile" : "", 
          "key" : { 
               "0" : "download_method", 
               "1" : "backup_dir", 
          }, 
          "section" : "dev" 
     }, 
     "start_time" : "2018/07/12 14:05:42", 
     "take" : "0" 
 }

```
