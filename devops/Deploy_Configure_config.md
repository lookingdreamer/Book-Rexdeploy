# 基于名字配置文件初始化


## 基于名字配置文件初始化
- Deploy:Configure:config

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字配置文件初始化| rex  [module] --k='[k]'| 


**示例1**  
名字分组为server的应用: 发布应用之前自动修改版本js版本号
```
rex Deploy:Configure:config --k='server'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）                  
w: 等于1,JSON格式输出结果


**备注**  
- 该模块在执行[自动发布](https://book.osichina.net/devops/release.html)以及[灰度自动发布](https://book.osichina.net/devops/deploy.html)会自动执行该模块           


比如发布server应用,每次发布之前时候都需要手动修改一下js版本号,保障更新之后客户端会自动取到最新的js文件。     
这个模块就是自动处理发布之前添加版本号等相关功能的     
操作步骤如下:     
(1)添加数据库记录          
```
UPDATE pre_server_detail
SET `local_conf_cmd` = 'time=$(date \'+%s\') &&  njsver=\"1.1.${time}\" && jsver=$(cat config.properties |grep -n  \'jsver\' |head -n 1 | grep -P \'\\d{1,}.\\d{1,}.\\d{1,}\' -o) && sed -i \"s/${jsver}/${njsver}/\" config.properties '
WHERE
    local_name = 'server';
```    

(2)执行配置文件初始化     
`rex Deploy:Configure:config --k='server'`

上述操作其实就是把config.properties 中jsver的版本号进行动态修改    
注意的是     
local_conf_cmd: 在配置文件的路径中执行,如上则会在configuredir/server/server1/和configuredir/server/server2中分别执行          
local_pro_cmd: 在工程文件的路径中执行,如上则会在soft/server/中执行     

**截图** 

示例1
![](../assets/Deploy_Configure_config.gif) 



