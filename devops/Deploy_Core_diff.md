# 基于名字校验程序和配置包


## 基于名字校验程序和配置包
- Deploy:Core:diff

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字校验程序和配置包| rex  [module] --k='[k]'| 


**示例1**  
名字为server1 server2的应用: 校验下载目录程序和配置和待发布目录差异
```
rex Deploy:Core:diff --k='server1 server2' 
```


**参数用法**  
k: 名字(app_key)或者名字分组(local_name) 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）                       
w: 等于1,JSON格式输出结果


**备注** 
- 该模块在执行[自动发布](https://book.osichina.net/devops/release.html)以及[灰度自动发布](https://book.osichina.net/devops/deploy.html)会自动执行该模块           


校验实际上是调用diff命令,校验待发布的目录和配置和原目录的差异       


**截图**  
![](../assets/Deploy_Core_diff.gif)


