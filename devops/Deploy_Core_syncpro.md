# 基于名字程序和配置包同步


## 基于名字程序和配置包本地同步
- Deploy:Core:syncpro

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字程序和配置包同步| rex  [module] --k='[k]' --update='0/1'| 


**示例1(同步remotedir目录到待发布目录)**    
名字分组为server的应用:同步本地(远程download)的程序和配置=>待发布目录
```
rex Deploy:Core:syncpro --k='server'
```

**示例2(同步updatedir目录到待发布目录)**  
名字分组为server的应用:同步本地(updatedir)的程序和配置=>待发布目录
```
rex Deploy:Core:syncpro --k='server' --update='1'
```


**参数用法**  
k: 名字(app_key)或者名字分组(local_name) 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）             
update: 为0时或者不传时,同步remotecomdir到soft和configure目录;为1时同步updatedir目录中程序和配置到soft和configure目录                    
w: 等于1,JSON格式输出结果


**备注**  
- 该模块在执行[自动发布](https://book.osichina.net/devops/release.html)以及[灰度自动发布](https://book.osichina.net/devops/deploy.html)会自动执行该模块,都会将同步updatedir目录到待发布目录                         



remotecomdir以及updatedir目录和soft可以通过配置文件config.yml自定义          
这个同步过程会自动处理工程和配置不统一的情况     
可参考 [接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)        
其中 类型二:配置和应用集成部署    
当configure_file_status=1，configure_file_list为配置文件列表且有值,会在执行这个模块单独分离configure_file_list中的配置到待发布的配置目录中   

**截图**  
示例1 && 示例2
![](../assets/Deploy_Core_syncpro.gif)


