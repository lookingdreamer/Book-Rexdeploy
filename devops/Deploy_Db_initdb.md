# 应用发布初始化


## 应用发布初始化
- Deploy:Db:initdb

| 描述 | 命令 | 
| :---: | :--- | 
|应用发布初始化| rex  [module]| 


**示例1**  
初始化发布程序
```
rex Deploy:Db:initdb 
```



**参数用法**  
该模块没有参数

**备注**  
当执行自动发布或者灰度发布时,默认会对该应用加1个全局锁,比如A用户正在发布server应用。    
那么B用户在执行发布server应用时会提示正在发布.也就是说A用户发布server应用时会对这个     
应用加1个锁,如果B用户需要强制发布的话,可以执行`rex Deploy:Db:initdb`,强制解锁发布    
另一种情况就是如果在发布server应用异常的时候,是不会自动解锁的。异常之后再次执行发布的    
时候,还会提示该server应用正在发布。这是需要执行`rex Deploy:Db:initdb`手动解锁    

**截图**  
示例1
![](../assets/Deploy_Db_initdb.gif)


