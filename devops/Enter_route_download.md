# 基于名字下载程序和配置包


## 基于名字下载程序和配置包
- Enter:route:download

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字下载程序和配置包| rex  [module] --k='[k]'| 


**示例1**  
将server应用的程序和配置下载到本地remotecomdir目录
```
rex Enter:route:download --k='server' 
```

**示例2**  
将server应用的程序和配置下载到本地remotecomdir目录,同时同步到updatedir目录中
```
rex Enter:route:download --k='server' --update='1'
```

**示例3**  
将uat环境的server应用的程序同步下载到remotecomdir目录中
```
rex Enter:route:download --k='server' --senv='uat' --type='pro'
```

**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
update: 为0时或者不传时,只下载到remotecomdir对应目录中;为1时同时拷贝一份到updatedir目录中     
senv: 同步下载的环境,参见config.yml中解释,可自定义添加    
type: 当只传k参数时,默认下载程序和配置;当senv为真时,type为pro,默认只下载程序;当type=pro,
只下载程序;当type=conf,只下载配置;当type=all,程序和配置都下载         
w: 等于1,JSON格式输出结果


**备注**  
- 该模块在执行[自动发布](https://book.osichina.net/devops/release.html)以及[灰度自动发布](https://book.osichina.net/devops/deploy.html)会自动执行该模块                            




remotecomdir以及updatedir目录可以通过配置文件config.yml自定义 


**截图**  
示例1 && 示例2 && 示例3
![](../assets/Enter_route_download.gif)


