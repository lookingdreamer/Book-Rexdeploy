# 基于名字下载http包并合并


## 基于名字下载http包并合并
- Enter:route:downloadCombile

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字下载http包并合并| rex  [module] --k='[k]' --url=''| 


**示例1(下载http包到本地)**  
下载server应用的http包到本地
```
rex Enter:route:downloadCombile --k='server' --url='http://10.0.0.3/test.tar.gz'
```

**示例2(下载http包到本地并合并到待发布目录中)**  
下载server应用的http包到本地,并合并到待发布目录中
```
rex Enter:route:downloadCombile --k='server' --url='http://10.0.0.3/test.tar.gz' --full='1' --dest='1'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
url: http下载包的路径 （必选）          
full: 合并包时: 1增量合并 2全量合并           
dest: 1合并到待发布目录中  2合并到updatedir目录中     
downdir: 自定义下载目录,如果不传时会自动取配置文件中download_dir配置+日期随机目录               
w: 等于1,JSON格式输出结果


**备注**  
- 该模块在执行[自动发布](https://book.osichina.net/devops/release.html)以及[灰度自动发布](https://book.osichina.net/devops/deploy.html)会自动综合了该模块           


**截图**  
示例1
![](../assets/Enter_route_downloadCombile.gif)

示例2
![](../assets/Enter_route_downloadCombile2.gif)

