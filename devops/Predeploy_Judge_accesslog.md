# 基于名字校验日志文件md5值


## 基于名字校验日志文件md5值
- Predeploy:Judge:accesslog

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字校验日志文件md5值| rex  [module] --k='[k]'| 


**示例1**  
校验日志文件的md5值
```
rex Predeploy:Judge:accesslog --k='server1 server2'
```


**参数用法**  
k: 名字参见数据库字段的[app_key](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开   （必选）        
log: 不传时默认取数据库配置的日志参数,有值时取传参的日志路径                 
w: 等于1,JSON格式输出结果


**备注**  
log参数不传时,取pre_server_detail中access_log字段。     
同时access_log支持日期的正则表达式如 /data/log/tomcat/catalina.#%Y-%m-%d.log


**截图**  



