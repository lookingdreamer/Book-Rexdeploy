
Common:Use:getJson


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527661143jounhnkfhpsfynvkphcryehonhhlnjhm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    无   |
|random |  是  |    string   |    无   |
|delete |  是  |    int   |    无   |

**请求示例**     

`action=Common:Use:getJson&delete=1&random=randomstring`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "1", 
          "count" : "2",  //返回总数量
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "server" : "127.0.0.1",  //操作服务器地址
                              "stdout" : " 03:55:53 up 2 days,  4:14,  3 users,  load average: 0.02, 0.07, 0.05"  //最终执行命令标准输出
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
               "1" : {  //返回第2个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "server" : "10.0.2.15",  //操作服务器地址
                              "stdout" : " 03:55:53 up 2 days,  4:14,  4 users,  load average: 0.02, 0.07, 0.05"  //最终执行命令标准输出
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { //请求参数
          "action" : "Common:Use:getJson",  //请求模块
          "delete" : "1",  //Common:Use:getJson获取结果之后是否删除内存中保存的数据,为1时删除
          "random" : "randomstring",  //随机数
          "token" : "1527661143jounhnkfhpsfynvkphcryehonhhlnjhm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Common:Use:getJson,--delete="1",--random="randomstring",--token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Common:Use:getJson --delete="1" --random="randomstring" --token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "3"  //执行命令花费时间
 }

```


