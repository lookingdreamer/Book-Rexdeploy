# 基于名字配置文件初始化

**简要描述：** 

- 根据关键词发布之应用和配置初始化



**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Deploy:Configure:config    |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开    |

**详细说明：**     

比如发布server应用,每次发布之前时候都需要手动修改一下js版本号,保障更新之后客户端会自动取到最新的js文件。     
这个模块就是自动处理发布之前添加版本号等相关功能的     
操作步骤如下:     
(1)添加数据库记录          
```
UPDATE pre_server_detail
SET `local_conf_cmd` = 'time=$(date \'+%s\') &&  njsver=\"1.1.${time}\" && jsver=$(cat config.properties |grep -n  \'jsver\' |head -n 1 | grep -P \'\\d{1,}.\\d{1,}.\\d{1,}\' -o) && sed -i \"s/${jsver}/${njsver}/\" config.properties '
WHERE
    local_name = 'server';
```    

(2)执行配置文件初始化     
`action=Deploy:Configure:config&k='server'`

上述操作其实就是把config.properties 中jsver的版本号进行动态修改    
注意的是     
local_conf_cmd: 在配置文件的路径中执行,如上则会在configuredir/server/server1/和configuredir/server/server2中分别执行          
local_pro_cmd: 在工程文件的路径中执行,如上则会在soft/server/中执行     


**请求示例**     

`action=Deploy:Configure:config&k=server`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "diff_conf" : { //配置执行信息
                         "0" : {  //返回第1个数据
                              "app_key" : "server1",  //返回服务器唯一名字
                              "local_conf_cmd" : "uptime && exit 0 ", //配置初始化执行命令
                              "local_name" : "server" 
                         }, 
                    }, 
                    "diff_pro" : { //工程执行信息
                         "0" : {  //返回第1个数据
                              "local_name" : "server", 
                              "local_pro_cmd" : "uptime && exit 0 " //工程初始化执行命令
                         }, 
                    }, 
                    "msg" : "run init pro and config success", 
                    "run_conf_cmd" : {  //配置初始化执行结果
                         "code" : "0",  //0 成功 非0 失败
                         "data" : {  //返回数据
                              "0" : {  //返回第1个数据
                                   "conf_cmd" : "uptime && exit 0 ", 
                                   "local_name" : "server", 
                                   "run_dir" : "configuredir//server/server1" 
                              }, 
                         }, 
                         "msg" : "run cmd success", 
                         "runres" : " 05:34:37 up 2 days,  5:53,  2 users,  load average: 0.17, 0.14, 0.14" 
                    }, 
                    "run_pro_cmd" : { //工程初始化执行结果
                         "code" : "0",  //0 成功 非0 失败
                         "data" : {  //返回数据
                              "0" : {  //返回第1个数据
                                   "local_name" : "server", 
                                   "pro_cmd" : "uptime && exit 0 ", 
                                   "run_dir" : "softdir//server" 
                              }, 
                         }, 
                         "msg" : "run cmd success", 
                         "runres" : " 05:34:37 up 2 days,  5:53,  2 users,  load average: 0.17, 0.14, 0.14" 
                    } 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Deploy:Configure:config",  //请求模块
          "k" : "server",  //名字
          "token" : "1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Deploy:Configure:config,--k="server",--token="1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Deploy:Configure:config --k="server" --token="1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "3"  //执行命令花费时间
 }

```

