# 基于名字校验程序和配置包

**简要描述：** 

- 根据关键词发布之应用校验


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527669337mxchcptwhwevbwgyiahtilfadcycyfxm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Deploy:Core:diff  |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |

**详细说明：**     
校验实际上是调用diff命令,校验待发布的目录和配置和原目录的差异     

**请求示例**     

`action=Deploy:Core:diff&k=server1,server2`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "1",  //1校验成功 
                    "confChange" : "[server1配置:0],[server2配置:0]",  //0代表没有差异 比如1就是存在1个文件差异有可能是添加/删除/修改
                    "differcount" : { 
                         "0" : {  //返回第1个数据
                              "0" : "{"app_key":"server1","prodiffercount":"0,0,0,0","confdiffercount":"0,0,0,0"}",  // 返回server1的diff数据,合计变化的文件数: 0 变化的文件数:0 删除的文件数:0 新增文件数:0 
                              "1" : "{"app_key":"server2","prodiffercount":"0,0,0,0","confdiffercount":"0,0,0,0"}",  //返回server2的diff数据,合计变化的文件数: 0 变化的文件数:0 删除的文件数:0 新增文件数:0
                         }, 
                    }, 
                    "enconfChange" : "[server1 conf:0],[server2 conf:0]", 
                    "enproChange" : "[server1 pro:0],[server2 pro:0]", 
                    "errDownloadconf" : "", 
                    "errDownloadpro" : "", 
                    "errconf" : "", 
                    "errpro" : "", 
                    "proChange" : "[server1程序:0],[server2程序:0]" //0代表没有差异 比如1就是存在1个文件差异有可能是添加/删除/修改
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Deploy:Core:diff",  //请求模块
          "k" : "server1 server2",  //名字
          "token" : "1527669337mxchcptwhwevbwgyiahtilfadcycyfxm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Deploy:Core:diff,--k="server1 server2",--token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Deploy:Core:diff --k="server1 server2" --token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "28"  //执行命令花费时间
 }

```

