# 基于名字程序和配置包同步

**简要描述：**     

- 基于名字程序和配置包同步     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527669337mxchcptwhwevbwgyiahtilfadcycyfxm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Deploy:Core:syncpro   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |
|update |  否  |    string   |    为0时或者不传时,同步remotecomdir到soft和configure目录;<br>为1时同步updatedir目录中程序和配置到soft和configure目录   |

**详细说明：**     


* 示例1(同步remotedir目录到待发布目录)         
名字分组为server的应用:同步本地(远程download)的程序和配置=>待发布目录
```
action=Deploy:Core:syncpro&k='server'
```

* 示例2(同步updatedir目录到待发布目录)      
名字分组为server的应用:同步本地(updatedir)的程序和配置=>待发布目录
```
action=Deploy:Core:syncpro&k='server'&update='1'
```



remotecomdir以及updatedir目录和soft可以通过配置文件config.yml自定义          
这个同步过程会自动处理工程和配置不统一的情况     
可参考 [接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)        
其中 类型二:配置和应用集成部署    
当configure_file_status=1，configure_file_list为配置文件列表且有值,会在执行这个模块单独分离configure_file_list中的配置到待发布的配置目录中     

**请求示例**     

`action=Deploy:Core:syncpro&k=server1,server2`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : "1",  //返回第1个数据 为1    只要第1个值为1则全部成功
               "1" : "",  //返回第2个数据 为空
               "2" : "",  //返回第3个数据 为空 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Deploy:Core:syncpro",  //请求模块
          "k" : "server1 server2",  //名字
          "token" : "1527669337mxchcptwhwevbwgyiahtilfadcycyfxm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Deploy:Core:syncpro,--k="server1 server2",--token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Deploy:Core:syncpro --k="server1 server2" --token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "25"  //执行命令花费时间
 }

```

