# 应用发布初始化

**简要描述：**     

- 应用发布初始化    


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527669337mxchcptwhwevbwgyiahtilfadcycyfxm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Deploy:Db:initdb    |

**详细说明：**     

该模块没有具体的返回数据,只要ret的返回值为0即执行成功         

当执行自动发布或者灰度发布时,默认会对该应用加1个全局锁,比如A用户正在发布server应用。    
那么B用户在执行发布server应用时会提示正在发布.也就是说A用户发布server应用时会对这个     
应用加1个锁,如果B用户需要强制发布的话,可以执行`Deploy:Db:initdb`,强制解锁发布    
另一种情况就是如果在发布server应用异常的时候,是不会自动解锁的。异常之后再次执行发布的    
时候,还会提示该server应用正在发布。这是需要执行`Deploy:Db:initdb`手动解锁   

**请求示例**     

`action=Deploy:Db:initdb`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : "1",  //返回第1个数据
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Deploy:Db:initdb",  //请求模块
          "token" : "1527669337mxchcptwhwevbwgyiahtilfadcycyfxm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Deploy:Db:initdb,--token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Deploy:Db:initdb --token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "2"  //执行命令花费时间
 }

```

