# 基于名字手动灰度发布

**简要描述：**     

- 基于名字手动灰度发布


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527674253lfffgjmmiafqvtylikeqpbetwowsuuym `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Enter:deploy:main   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开    |
|senv |  否  |    string   |    自动取包源环境,该环境在config.yml中配置,你可以自定义其他环境。   |
|url |  否  |    string   |    http下载更新包   |
|full |  否  |    string   |    1增量更新 2全量更新   |   


**详细说明：**     

* 示例1(跨环境自动取包发布)    
   
将uat环境的server应用同步发布带当前生产环境  

```
action=Enter:deploy:main&k='server1'&senv='uat'

action=Enter:deploy:main&k='server2'&senv='uat'
```


* 示例2(手动上传更新包发布)  
     
手动将代码和配置上传到当前的程序和配置目录,半手动灰度发布   
   
```
action=Enter:deploy:main&k='server1'

action=Enter:deploy:main&k='server2'
```


手动灰度发布和自动灰度发布唯一区别就是手动定义灰度的系统     
自动灰度发布是根据load_key_sorts(可在配置中自定义)数据表定义的local_name自动发布哪些系统     
手动灰度发布是根据手工制定先发布哪些应用然后再发布哪些应用     
   
同时手动灰度发布主要支持2种模式，如上的示例 1 2  

- 示例1(跨环境自动取包发布)  
比如你在开发环境(dev)和预生产环境(uat)同时部署了rexEnter:deploy:main.      同时在config.yml中配置了uat和dev环境的数据([参考配置](https://book.osichina.net/an-zhuang/pei-zhi.html))    
现在要将dev环境的server应用同步发布到uat环境,在uat环境执行`action=Enter:deploy:main&k='server'&senv='dev'`即可   

- 示例2(手动上传更新包发布)   
这个发布相对前2步多了一个步骤,需要把程序和配置包上传到updatedir对应的程序和配置目录.      
比如发布server应用    
(1)执行同步下载程序和配置      
`action=Enter:route:download&k='server'&update='1'`      
该步骤会把正在使用的server应用的程序和配置下载到updatedir/softdir/server/和updatedir/configuredir/server目录     
(2)覆盖程序和修改配置        
上传包在 updatedir/softdir/server/ 上传配置在 updatedir/configuredir/server     
(3)执行发布    
`action=Enter:deploy:main&k='server'`      

**请求示例**     

`action=Enter:deploy:main&k=server1&senv=uat`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
     }, 
     "param" : { //请求参数
          "action" : "Enter:deploy:main",  //请求模块
          "k" : "server1",  //名字
          "senv" : "uat", 
          "token" : "1527674253lfffgjmmiafqvtylikeqpbetwowsuuym"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Enter:deploy:main,--k="server1",--senv="uat",--token="1527674253lfffgjmmiafqvtylikeqpbetwowsuuym",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Enter:deploy:main --k="server1" --senv="uat" --token="1527674253lfffgjmmiafqvtylikeqpbetwowsuuym" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "163"  //执行命令花费时间
 }

```