# 根据名字下载程序和配置包

**简要描述：**     

- 根据名字下载程序和配置包     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Enter:route:download   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |
|update |  否  |    int   |    为0时或者不传时,只下载到remotecomdir对应目录中;<br>为1时同时拷贝一份到updatedir目录中    |
|senv |  否  |    string   |   同步下载的环境,参见config.yml中解释,可自定义添加    |
|type |  否  |    string   |   当senv为真时,type不传时,默认只下载程序;<br>当senv为真且type=pro,只下载程序;<br>当senv为真且type=conf,只下载配置;<br>当senv为真且type=all,程序和配置都下载  |
|usetype |  否  |    string   |   当senv假时,usetyp不传时,默认下载程序和配置;<br>当senv假且usetype=pro,只下载程序;<br>当senv假且usetype=conf,只下载配置;<br>当senv假且usetype=all,程序和配置都下载   |


**详细说明：**     


* 示例1  
将server应用的程序和配置下载到本地remotecomdir目录
```
action=Enter:route:download&k='server' 
```

* 示例2 
将server应用的程序和配置下载到本地remotecomdir目录,同时同步到updatedir目录中
```
action=Enter:route:download&k='server'&update='1'
```

* 示例3 
将uat环境的server应用的程序同步下载到remotecomdir目录中
```
action=Enter:route:download&k='server'&senv='uat'&type='pro'
```

**请求示例**     

`action=Enter:route:download&k=server`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "confresult" : { 
                                        "code" : "0",  //0 成功 非0 失败
                                        "download" : {  //下载结果校验信息
                                             "code" : "0",  //0 成功 非0 失败
                                             "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                                             "localPath" : "remotecomdir/configuredir/server1//applicationContext.xml"  //下载成功后文件或目录路径
                                        }, 
                                        "msg" : "download success",  //下载成功消息提示
                                        "params" : { 
                                             "app_key" : {  //返回服务器唯一名字
                                             }, 
                                             "dir1" : "/data/www/config1/",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                             "dir2" : "remotecomdir/configuredir/server1/",  //download时:本地目录,upload时:远程目录
                                             "http" : {  //是否http下载
                                             }, 
                                             "ipsep" : {  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                                             }, 
                                             "w" : { 
                                             } 
                                        }, 
                                        "server" : "192.168.0.76",  //操作服务器地址
                                        "size" : "24K",  //文件或目录大小
                                        "take" : "1"  //执行命令花费时间
                                   }, 
                                   "download_all" : "false", 
                                   "end_time" : "1527837586",  //执行发布的结束时间
                                   "local_config_dir" : "remotecomdir/configuredir/server1/", 
                                   "local_config_dir_size" : "21K", 
                                   "localdir" : "remotecomdir/softdir/server1/", 
                                   "localdir_size" : "31M", 
                                   "params" : { 
                                        "config" : { 
                                             "access_log" : "/data/log/server1/catalina.#%Y-%m-%d#.log", 
                                             "app_key" : "server1",  //返回服务器唯一名字
                                             "auto_deloy" : "1", 
                                             "backupdir_same_level" : "0", 
                                             "config_dir" : "/data/www/config1", 
                                             "container_dir" : "/usr/local/tomcat-server1", 
                                             "cpu" : "4核", 
                                             "created_time" : "2018-03-21 10:29:50", 
                                             "depart_name" : "", 
                                             "deploydir_same_level" : "0", 
                                             "disk" : "100G(本地盘)", 
                                             "header" : "Content-Type:application/json", 
                                             "id" : "1", 
                                             "is_deloy_dir" : "2", 
                                             "loadBalancerId" : "lb-7pbxcxiq,lb-7h3nl8kw", 
                                             "local_name" : "server", 
                                             "log_dir" : "/data/log/server1", 
                                             "mask" : { 
                                             }, 
                                             "mem" : "8GB", 
                                             "network_ip" : "192.168.0.76", 
                                             "note" : "", 
                                             "params" : "{"account":"1000636912345","password":"123456"}", 
                                             "pro_dir" : "/data/www/html1", 
                                             "pro_init" : "/etc/init.d/tomcat-server1", 
                                             "pro_key" : "tomcat-server1", 
                                             "pro_port" : "6080", 
                                             "pro_type" : "tomcat", 
                                             "require" : "success", 
                                             "requirecode" : "200", 
                                             "server_name" : "后台服务集群1", 
                                             "status" : "1", 
                                             "system_type" : "centos7.1", 
                                             "updated_time" : "2018-03-21 10:29:50", 
                                             "url" : "http://192.168.235.58:6080/login" 
                                        }, 
                                        "k" : "server1",  //名字
                                        "local_name" : "server1", 
                                        "network_ip" : "192.168.0.76", 
                                        "pro_key_array" : { 
                                             "0" : "server1",  //返回第1个数据
                                        }, 
                                        "relocal_name" : "server", 
                                        "remote_confir_dir" : "/data/www/config1", 
                                        "remotedir" : "/data/www/html1", 
                                        "senv" : { 
                                        }, 
                                        "type" : { 
                                        }, 
                                        "update" : { 
                                        }, 
                                        "usetype" : { 
                                        } 
                                   }, 
                                   "proresult" : { 
                                        "code" : "0",  //0 成功 非0 失败
                                        "download" : {  //下载结果校验信息
                                             "code" : "0",  //0 成功 非0 失败
                                             "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                                             "localPath" : "remotecomdir/softdir/server1//css"  //下载成功后文件或目录路径
                                        }, 
                                        "msg" : "download success",  //下载成功消息提示
                                        "params" : { 
                                             "app_key" : {  //返回服务器唯一名字
                                             }, 
                                             "dir1" : "/data/www/html1/",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                             "dir2" : "remotecomdir/softdir/server1/",  //download时:本地目录,upload时:远程目录
                                             "http" : {  //是否http下载
                                             }, 
                                             "ipsep" : {  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                                             }, 
                                             "w" : { 
                                             } 
                                        }, 
                                        "server" : "192.168.0.76",  //操作服务器地址
                                        "size" : "32M",  //文件或目录大小
                                        "take" : "20"  //执行命令花费时间
                                   }, 
                                   "remote_confir_dir" : "/data/www/config1/", 
                                   "remotedir" : "/data/www/html1/", 
                                   "start_time" : "1527837563",  //执行发布的开始时间
                                   "take" : "23"  //执行命令花费时间
                              }, 
                              "mainProcess" : "1230"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "confresult" : { 
                                        "code" : "0",  //0 成功 非0 失败
                                        "download" : {  //下载结果校验信息
                                             "code" : "0",  //0 成功 非0 失败
                                             "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                                             "localPath" : "remotecomdir/configuredir/server2//applicationContext.xml"  //下载成功后文件或目录路径
                                        }, 
                                        "msg" : "download success",  //下载成功消息提示
                                        "params" : { 
                                             "app_key" : {  //返回服务器唯一名字
                                             }, 
                                             "dir1" : "/data/www/config2/",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                             "dir2" : "remotecomdir/configuredir/server2/",  //download时:本地目录,upload时:远程目录
                                             "http" : {  //是否http下载
                                             }, 
                                             "ipsep" : {  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                                             }, 
                                             "w" : { 
                                             } 
                                        }, 
                                        "server" : "192.168.0.248",  //操作服务器地址
                                        "size" : "24K",  //文件或目录大小
                                        "take" : "1"  //执行命令花费时间
                                   }, 
                                   "download_all" : "false", 
                                   "end_time" : "1527837587",  //执行发布的结束时间
                                   "local_config_dir" : "remotecomdir/configuredir/server2/", 
                                   "local_config_dir_size" : "21K", 
                                   "localdir" : "remotecomdir/softdir/server2/", 
                                   "localdir_size" : "31M", 
                                   "params" : { 
                                        "config" : { 
                                             "access_log" : "/data/log/server2/catalina.#%Y-%m-%d#.log", 
                                             "app_key" : "server2",  //返回服务器唯一名字
                                             "auto_deloy" : "1", 
                                             "backupdir_same_level" : "0", 
                                             "config_dir" : "/data/www/config2", 
                                             "container_dir" : "/usr/local/tomcat-server2", 
                                             "cpu" : "4核", 
                                             "created_time" : "2018-03-21 10:30:15", 
                                             "depart_name" : "", 
                                             "deploydir_same_level" : "0", 
                                             "disk" : "100G(本地盘)", 
                                             "header" : "Content-Type:application/json", 
                                             "id" : "2", 
                                             "is_deloy_dir" : "2", 
                                             "loadBalancerId" : "lb-7pbxcxiq,lb-7h3nl8kw", 
                                             "local_name" : "server", 
                                             "log_dir" : "/data/log/server2", 
                                             "mask" : { 
                                             }, 
                                             "mem" : "8GB", 
                                             "network_ip" : "192.168.0.248", 
                                             "note" : "", 
                                             "params" : "{"account":"1000636912345","password":"123456"}", 
                                             "pro_dir" : "/data/www/html2", 
                                             "pro_init" : "/etc/init.d/tomcat-server2", 
                                             "pro_key" : "tomcat-server2", 
                                             "pro_port" : "7080", 
                                             "pro_type" : "tomcat", 
                                             "require" : "success", 
                                             "requirecode" : "200", 
                                             "server_name" : "后台服务集群2", 
                                             "status" : "1", 
                                             "system_type" : "centos7.1", 
                                             "updated_time" : "2018-03-21 10:30:15", 
                                             "url" : "http://192.168.235.58:6080/login" 
                                        }, 
                                        "k" : "server2",  //名字
                                        "local_name" : "server2", 
                                        "network_ip" : "192.168.0.248", 
                                        "pro_key_array" : { 
                                             "0" : "server1",  //返回第1个数据
                                        }, 
                                        "relocal_name" : "server", 
                                        "remote_confir_dir" : "/data/www/config2", 
                                        "remotedir" : "/data/www/html2", 
                                        "senv" : { 
                                        }, 
                                        "type" : { 
                                        }, 
                                        "update" : { 
                                        }, 
                                        "usetype" : { 
                                        } 
                                   }, 
                                   "proresult" : { 
                                        "code" : "0",  //0 成功 非0 失败
                                        "download" : {  //下载结果校验信息
                                             "code" : "0",  //0 成功 非0 失败
                                             "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                                             "localPath" : "remotecomdir/softdir/server2//css"  //下载成功后文件或目录路径
                                        }, 
                                        "msg" : "download success",  //下载成功消息提示
                                        "params" : { 
                                             "app_key" : {  //返回服务器唯一名字
                                             }, 
                                             "dir1" : "/data/www/html2/",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                             "dir2" : "remotecomdir/softdir/server2/",  //download时:本地目录,upload时:远程目录
                                             "http" : {  //是否http下载
                                             }, 
                                             "ipsep" : {  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                                             }, 
                                             "w" : { 
                                             } 
                                        }, 
                                        "server" : "192.168.0.248",  //操作服务器地址
                                        "size" : "32M",  //文件或目录大小
                                        "take" : "19"  //执行命令花费时间
                                   }, 
                                   "remote_confir_dir" : "/data/www/config2/", 
                                   "remotedir" : "/data/www/html2/", 
                                   "start_time" : "1527837563",  //执行发布的开始时间
                                   "take" : "24"  //执行命令花费时间
                              }, 
                              "mainProcess" : "1230"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "1230",  //当前命令的进程ID
                    "msg" : "success", 
                    "srcdata" : { 
                         "k" : "server1 server2",  //名字
                         "parallelism" : "10", 
                         "params" : { 
                              "k" : "server",  //名字
                              "senv" : "", 
                              "type" : "", 
                              "update" : "", 
                              "usetype" : "", 
                              "w" : "1" 
                         }, 
                         "take" : "24"  //执行命令花费时间
                    } 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Enter:route:download",  //请求模块
          "k" : "server",  //名字
          "token" : "1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Enter:route:download,--k="server",--token="1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Enter:route:download --k="server" --token="1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "26"  //执行命令花费时间
 }

```