# 基于名字下载http包并合并

**简要描述：**     

- 基于名字下载http包并合并     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Enter:route:downloadCombile   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 此处只能传1个值   |
|url |  是  |    string   |    http下载包的路径   |
|full |  否  |    int   |   合并包时: 1增量合并 2全量合并    |
|dest |  否  |    int   |    1合并到待发布目录中  2合并到updatedir目录中     |
|downdir |  否  |    string   |    自定义下载目录,<br>如果不传时会自动取配置文件中download_dir配置+日期随机目录   |

**详细说明：**     


* 示例1(下载http包到本地)         
下载server应用的http包到本地
```
action=Enter:route:downloadCombile&k='server'&url='http://10.0.0.3/test.tar.gz'
```

* 示例2(下载http包到本地并合并到待发布目录中)        
下载server应用的http包到本地,并合并到待发布目录中
```
action=Enter:route:downloadCombile&k='server'&url='http://10.0.0.3/test.tar.gz'&full='1'&dest='1'
```


**请求示例**     

`action=Enter:route:downloadCombile&k=server&url=http://download.abc.com/1/cm-20180515-142727.tar.gz`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "checkRes" : "1", 
                    "code" : "1", 
                    "downdir" : "download/http/20180601_043628", 
                    "download" : {  //下载结果校验信息
                         "code" : "0",  //0 成功 非0 失败
                         "download" : {  //下载结果校验信息
                              "code" : "0",  //0 成功 非0 失败
                              "download_method" : "http",  //下载方式,通过config/config.yml可定义
                              "localPath" : "download/http/20180601_043628/cm-20180515-142727.tar.gz"  //下载成功后文件或目录路径
                         }, 
                         "msg" : "download success",  //下载成功消息提示
                         "params" : { 
                              "app_key" : {  //返回服务器唯一名字
                              }, 
                              "dir1" : "http://download.abc.com/1/cm-20180515-142727.tar.gz",  //download时:远程文件或者目录,upload时:本地文件或者目录
                              "dir2" : "download/http/20180601_043628",  //download时:本地目录,upload时:远程目录
                              "http" : "1",  //是否http下载
                              "ipsep" : {  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                              }, 
                              "w" : { 
                              } 
                         }, 
                         "server" : "<local>",  //操作服务器地址
                         "take" : "3"  //执行命令花费时间
                    }, 
                    "msg" : "success", 
                    "params" : { 
                         "dest" : "", 
                         "downdir" : "", 
                         "full" : "", 
                         "k" : "server",  //名字
                         "set" : "", 
                         "url" : "http://download.abc.com/1/cm-20180515-142727.tar.gz" 
                    }, 
                    "unzip" : { 
                         "cmd" : "tar -zxf download/http/20180601_043628/cm-20180515-142727.tar.gz -C download/http/20180601_043628",  //执行命令
                         "code" : "1", 
                         "res" : "" 
                    } 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Enter:route:downloadCombile",  //请求模块
          "k" : "server",  //名字
          "token" : "1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr",  //token
          "url" : "http://download.abc.com/1/cm-20180515-142727.tar.gz" 
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Enter:route:downloadCombile,--k="server",--token="1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr",--url="http://download.abc.com/1/cm-20180515-142727.tar.gz",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Enter:route:downloadCombile --k="server" --token="1527837552hvdmkhvhwfrfvlehlfdvxkneoflblnqr" --url="http://download.abc.com/1/cm-20180515-142727.tar.gz" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "6"  //执行命令花费时间
 }

```

