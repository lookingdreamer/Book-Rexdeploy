# 基于名字服务控制

**简要描述：** 

- 校验远程服务器工程、配置以及应用进程是否存在

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527610317gffvfenjcndxnfwjktdiudcpqoafdjom `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    check   |
|k |  是  |    string   |    名字 参见数据库字段的[app_key](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),<br>如果多个app_key,用空格隔开  |

**详细说明：**        
该模块会校验远程服务器的程序/配置/进程等信息        

**请求示例**     

`action=check&k=server1,server2`

**返回示例**     

``` 
{ 
     "code" : "0",  //0 成功 非0 失败 //0调用系统命令执行成功 非0执行失败
     "data" : {      //返回调用数据 //返回数据
          "code" : "0",  //0 成功 非0 失败  //0成功 非0失败
          "data" : {      //返回具体数据 //返回数据
               "0" : {  //返回第1个数据
                    "app_config_dir" : "/data/www/config1",  //配置路径
                    "app_pro_dir" : "/data/www/html1", //工程路径
                    "app_pro_init" : "/etc/init.d/tomcat-server1",  //启动脚本
                    "app_pro_key" : "tomcat-server1", //应用关键词
                    "config_dir" : "1",  //1存在配置目录 非1不存在
                    "pro_dir" : "1",  //1存在工程目录 非1不存在
                    "pro_init" : "1",  //1存在启动脚本 非1不存在
                    "ps_num" : "2" //进程数量
               }, 
               "1" : {  //返回第2个数据
                    "app_config_dir" : "/data/www/config2", 
                    "app_pro_dir" : "/data/www/html2", 
                    "app_pro_init" : "/etc/init.d/tomcat-server2", 
                    "app_pro_key" : "tomcat-server2", 
                    "config_dir" : "1", 
                    "pro_dir" : "1", 
                    "pro_init" : "1", 
                    "ps_num" : "2" 
               }, 
          }, 
          "msg" : "成功"  //返回消息
     }, 
     "param" : { //请求参数 //传参
          "action" : "check", //传参模块 //请求模块
          "k" : "server1 server2",  //传参名字k //名字
          "token" : "1527610317gffvfenjcndxnfwjktdiudcpqoafdjom"   //传参token  //token
     }, 
     "parseparameters" : {  //参数解析 //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败//参数解析 0解析成功 非0失败
          "msg" : "校验和解析参数成功",  //参数解析消息
          "precmd" : "",  //解析参数后前缀命令 //请求参数前缀
          "requestCmd" : "check,--k="server1 server2",--token="1527610317gffvfenjcndxnfwjktdiudcpqoafdjom",--w="1""    //解析参数后后缀命令 //请求参数后缀
     }, 
     "print_stdout" : "0", //是否输出系统调用结果 0不输出 1输出 //是否打印输出结果 0 不打印 1 打印
     "respon" : {   //命令调用返回 //请求命令返回结果信息
          "chdir" : "/data/RexDeployV3",  //命令执行路径 //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  check --k="server1 server2" --token="1527610317gffvfenjcndxnfwjktdiudcpqoafdjom" --w="1"",   //执行命令 //执行命令
          "code" : "1", //执行结果判断 1成功 非1失败
          "msg" : "执行完成",  //执行消息
          "ret" : "0",   //命令返回码 0命令成功 非0命令失败 //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //命令标准错误输出 //最终执行命令标准错误输出
          "stdout" : ""  //命令标准输出 //最终执行命令标准输出
     }, 
     "take" : "4"   //花费时间,单位秒 //执行命令花费时间
 }

```



