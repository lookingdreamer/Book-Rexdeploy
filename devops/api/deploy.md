# 基于名字自动灰度发布

**简要描述：** 

- 基于名字自动灰度发布


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    deploy   |
|k |  是  |    string   |    名字参数    |
|senv |  否  |    string   |    自动取包源环境,该环境在config.yml中配置,你可以自定义其他环境。   |
|url |  否  |    string   |    http下载更新包   |
|full |  否  |    string   |    1增量更新 2全量更新   |


**详细说明：**       
具体灰度发布支持3种方式：      
1.跨环境自动取包更新      
2.自定义下载url更新      
3.手动上传更新包更新       

具体说明: 请参见文档       

具体的超级实践说明,请参考: [高级实践(1) 基于名字服务的灰度发布](https://book.osichina.net/huidufabu.html)

**请求示例**     

`action=deploy&k=server&senv=uat`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "deloy_configdir_after" : "/data/www/config1_20180530_043756",  //配置发布之后的真实路径
                              "deloy_configdir_before" : "/data/www/config1_20180521_043510",  //配置发布之前的真实路径
                              "deloy_prodir_after" : "/data/www/html1_20180530_043756",  //工程发布之后的真实路径
                              "deloy_prodir_before" : "/data/www/html1_20180521_043510",  //工程发布之前的真实路径
                              "deloy_prodir_real_before" : { 
                              }, 
                              "deloy_size" : "32M /data/www/html1_20180530_043756 24K /data/www/config1_20180530_043756",  //发布之后的工程和配置大小
                              "deploy_ip" : "192.168.0.76",  //执行发布的IP地址
                              "deploy_key" : "server1",  //执行发布的唯一名字
                              "deploy_take" : "Total Take:23 || Rsync Time:15 || Start App Time:4",  //执行发布的时间消耗
                              "end_time" : "2018-05-30 04:39:52",  //执行发布的结束时间
                              "processNumber" : "2",  //执行发布后的应用进程数量
                              "randomStr" : "1527665969426659803",  //执行发布的唯一随机字符串
                              "rollRecord" : "0",  //执行发布的回滚记录
                              "rollStatus" : "0",  //执行发布的回滚状态
                              "rollbackNumber" : "0",  //执行发布的回滚次数
                              "rsync_war_time" : "2018-05-30 04:39:44",  //执行发布的开始传输包时间
                              "start_app_time" : "2018-05-30 04:39:48",  //执行发布的启动app时间
                              "start_time" : "2018-05-30 04:39:29"  //执行发布的开始时间
                         }, 
                    }, 
                    "endtime" : "2018-05-30 04:40:17", 
                    "msg" : "server1 have finshed deploy", 
                    "starttime" : "2018-05-30 04:37:56" 
               }, 
               "1" : {  //返回第2个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "deloy_configdir_after" : "/data/www/config2_20180530_043756",  //配置发布之后的真实路径
                              "deloy_configdir_before" : "/data/www/config2_20180521_043510",  //配置发布之前的真实路径
                              "deloy_prodir_after" : "/data/www/html2_20180530_043756",  //工程发布之后的真实路径
                              "deloy_prodir_before" : "/data/www/html2_20180521_043510",  //工程发布之前的真实路径
                              "deloy_prodir_real_before" : { 
                              }, 
                              "deloy_size" : "32M /data/www/html2_20180530_043756 24K /data/www/config2_20180530_043756",  //发布之后的工程和配置大小
                              "deploy_ip" : "192.168.0.248",  //执行发布的IP地址
                              "deploy_key" : "server2",  //执行发布的唯一名字
                              "deploy_take" : "Total Take:23 || Rsync Time:15 || Start App Time:4",  //执行发布的时间消耗
                              "end_time" : "2018-05-30 04:42:20",  //执行发布的结束时间
                              "processNumber" : "2",  //执行发布后的应用进程数量
                              "randomStr" : "1527666117630166853",  //执行发布的唯一随机字符串
                              "rollRecord" : "0",  //执行发布的回滚记录
                              "rollStatus" : "0",  //执行发布的回滚状态
                              "rollbackNumber" : "0",  //执行发布的回滚次数
                              "rsync_war_time" : "2018-05-30 04:42:12",  //执行发布的开始传输包时间
                              "start_app_time" : "2018-05-30 04:42:16",  //执行发布的启动app时间
                              "start_time" : "2018-05-30 04:41:57"  //执行发布的开始时间
                         }, 
                    }, 
                    "endtime" : "2018-05-30 04:42:46", 
                    "msg" : "server2 have finshed deploy", 
                    "starttime" : "2018-05-30 04:40:17" 
               }, 
               "2" : {  //返回第3个数据
                    "code" : "0",  //0 成功 非0 失败
                    "msg" : "server have deploy finshed" 
               }, 
          }, 
          "msg" : "server 全部灰度发布完成" 
     }, 
     "param" : { //请求参数
          "action" : "deploy",  //请求模块
          "k" : "server",  //名字
          "senv" : "uat", 
          "token" : "1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "deploy,--k="server",--senv="uat",--token="1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  deploy --k="server" --senv="uat" --token="1527665686lyoxihoxrulkoxwevyuanwtnkrddgpwr" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "292"  //执行命令花费时间
 }

```

