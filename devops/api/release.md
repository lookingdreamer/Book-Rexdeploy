# 基于名字自动发布

**简要描述：**     

- 基于名字自动发布     

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    release   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |
|senv |  否  |    string   |    自动取包源环境,该环境在config.yml中配置,你可以自定义其他环境。   |
|url |  否  |    string   |    http下载更新包   |
|full |  否  |    string   |    1增量更新 2全量更新   |

**详细说明：**     


**示例1(跨环境自动取包发布)**  
将uat环境的server应用同步发布带当前生产环境
```
action=release&k='server'&senv='uat'
```

**示例2(自动下载更新包发布)**  
将http服务器上test.tar.gz包增量发布到当前环境的server应用
```
action=release&k='server'&url='http://10.0.0.3/test.tar.gz'&full='1'
```

**示例3(手动上传更新包发布)**  
手动将代码和配置上传到当前的程序和配置目录,半自动发布
```
action=release&k='server'
```

自动更新支持名字或者local_name的名字分组的集群更新。   
同时自动发布主要支持3种模式，如上的示例 1 2 3 

- 示例1(跨环境自动取包发布)  
比如你在开发环境(dev)和预生产环境(uat)同时部署了rexdeploy.      同时在config.yml中配置了uat和dev环境的数据([参考配置](https://book.osichina.net/an-zhuang/pei-zhi.html))    
现在要将dev环境的server应用同步发布到uat环境,在uat环境执行`action=release&k='server'&senv='dev'`即可   

- 示例2(自动下载更新包发布)   
如示例2下载更新包直接发布到当前环境,如果是全量full=2   

- 示例3(手动上传更新包发布)   
这个发布相对前2步多了一个步骤,需要把程序和配置包上传到updatedir对应的程序和配置目录.      
比如发布server应用    
(1)执行同步下载程序和配置      
`action=Enter:route:download&k='server'&update='1'`      
该步骤会把正在使用的server应用的程序和配置下载到updatedir/softdir/server/和updatedir/configuredir/server目录     
(2)覆盖程序和修改配置        
上传包在 updatedir/softdir/server/ 上传配置在 updatedir/configuredir/server 
这一步可以结合Common:Use:run和Common:Use:download处理的    
(3)执行发布    
`action=release&k='server'` 

**请求示例**     

`action=release&k=server&senv=uat`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "deloy_configdir_after" : "/data/www/config1_20180624_160902",  //配置发布之后的真实路径
                              "deloy_configdir_before" : "/data/www/config1_20180605_144705",  //配置发布之前的真实路径
                              "deloy_prodir_after" : "/data/www/html1_20180624_160902",  //工程发布之后的真实路径
                              "deloy_prodir_before" : "/data/www/html1_20180605_144705",  //工程发布之前的真实路径
                              "deloy_prodir_real_before" : { 
                              }, 
                              "deloy_size" : "32M /data/www/html1_20180624_160902 24K /data/www/config1_20180624_160902",  //发布之后的工程和配置大小
                              "deploy_ip" : "192.168.0.207",  //执行发布的IP地址
                              "deploy_key" : "server1",  //执行发布的唯一名字
                              "deploy_take" : "Total Take:29 || Rsync Time:14 || Start App Time:5",  //执行发布的时间消耗
                              "end_time" : "2018-06-24 16:10:38",  //执行发布的结束时间
                              "processNumber" : "2",  //执行发布后的应用进程数量
                              "randomStr" : "1529856609597174600",  //执行发布的唯一随机字符串
                              "rollRecord" : "0",  //执行发布的回滚记录
                              "rollStatus" : "0",  //执行发布的回滚状态
                              "rollbackNumber" : "0",  //执行发布的回滚次数
                              "rsync_war_time" : "2018-06-24 16:10:23",  //执行发布的开始传输包时间
                              "start_app_time" : "2018-06-24 16:10:33",  //执行发布的启动app时间
                              "start_time" : "2018-06-24 16:10:09"  //执行发布的开始时间
                         }, 
                         "1" : {  //返回第2个数据
                              "deloy_configdir_after" : "/data/www/config2_20180624_160902",  //配置发布之后的真实路径
                              "deloy_configdir_before" : "/data/www/config2_20180605_144705",  //配置发布之前的真实路径
                              "deloy_prodir_after" : "/data/www/html2_20180624_160902",  //工程发布之后的真实路径
                              "deloy_prodir_before" : "/data/www/html2_20180605_144705",  //工程发布之前的真实路径
                              "deloy_prodir_real_before" : { 
                              }, 
                              "deloy_size" : "32M /data/www/html2_20180624_160902 24K /data/www/config2_20180624_160902",  //发布之后的工程和配置大小
                              "deploy_ip" : "192.168.0.76",  //执行发布的IP地址
                              "deploy_key" : "server2",  //执行发布的唯一名字
                              "deploy_take" : "Total Take:30 || Rsync Time:14 || Start App Time:6",  //执行发布的时间消耗
                              "end_time" : "2018-06-24 16:10:39",  //执行发布的结束时间
                              "processNumber" : "2",  //执行发布后的应用进程数量
                              "randomStr" : "1529856609826720700",  //执行发布的唯一随机字符串
                              "rollRecord" : "0",  //执行发布的回滚记录
                              "rollStatus" : "0",  //执行发布的回滚状态
                              "rollbackNumber" : "0",  //执行发布的回滚次数
                              "rsync_war_time" : "2018-06-24 16:10:23",  //执行发布的开始传输包时间
                              "start_app_time" : "2018-06-24 16:10:33",  //执行发布的启动app时间
                              "start_time" : "2018-06-24 16:10:09"  //执行发布的开始时间
                         }, 
                    }, 
                    "endtime" : "2018-06-24 16:10:44", 
                    "msg" : "server have finshed release", 
                    "starttime" : "2018-06-24 16:09:02" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "release",  //请求模块
          "k" : "server",  //名字
          "senv" : "uat", 
          "token" : "1529853651coqjifffyedvdqfnrghhphcadjgctexi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "release,--k="server",--senv="uat",--token="1529853651coqjifffyedvdqfnrghhphcadjgctexi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  release --k="server" --senv="uat" --token="1529853651coqjifffyedvdqfnrghhphcadjgctexi" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "105"  //执行命令花费时间
 }

```


