# 基于名字自动回滚

**简要描述：**     

- 基于名字自动回滚     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    rollback   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |
|rollstatus |  是  |    int   |    为0时,回滚到上一版本;<br>rollstatus为1 根据数据库字段rollStatus=1回滚到任意版本   |

**详细说明：**      
当rollstatus=1时，该命令会去查询CMDB主机配置表(pre_server_detail)<br> 
当local_name='server' 和 rollStatus=1时的记录回滚   


**请求示例**     

`action=rollback&k=server&rollstatus=0`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "app_key" : "server1",  //返回服务器唯一名字
                                   "data" : {  //返回数据
                                        "0" : {  //返回第1个数据
                                             "config" : { 
                                                  "access_log" : { 
                                                  }, 
                                                  "app_key" : "server1",  //返回服务器唯一名字
                                                  "auto_deloy" : "1", 
                                                  "backupdir_same_level" : "0", 
                                                  "config_dir" : "/data/www/config1", 
                                                  "container_dir" : "/usr/local/tomcat-server1", 
                                                  "cpu" : "4核", 
                                                  "created_time" : "2018-03-21 10:29:50", 
                                                  "depart_name" : "", 
                                                  "deploydir_same_level" : "0", 
                                                  "disk" : "100G(本地盘)", 
                                                  "header" : "Content-Type:application/json", 
                                                  "id" : "1", 
                                                  "is_deloy_dir" : "2", 
                                                  "is_restart_status" : "0", 
                                                  "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                                  "local_name" : "server", 
                                                  "log_dir" : "/data/log/server1", 
                                                  "mask" : { 
                                                  }, 
                                                  "mem" : "8GB", 
                                                  "network_ip" : "192.168.0.207", 
                                                  "note" : "", 
                                                  "params" : "{"cmd":"uptime"}", 
                                                  "pro_dir" : "/data/www/html1", 
                                                  "pro_init" : "/etc/init.d/tomcat-server1", 
                                                  "pro_key" : "tomcat-server1", 
                                                  "pro_port" : "6080", 
                                                  "pro_type" : "tomcat", 
                                                  "require" : "inputStr", 
                                                  "requirecode" : "200", 
                                                  "server_name" : "后台服务集群1", 
                                                  "status" : "1", 
                                                  "system_type" : "centos7.1", 
                                                  "updated_time" : "2018-03-21 10:29:50", 
                                                  "url" : "http://127.0.0.1:6080/packController/runCmd.do?cmd=uptime" 
                                             }, 
                                             "getLastDeloy" : { 
                                                  "deloy_configdir_after" : "/data/www/config1_20180624_160902",  //配置发布之后的真实路径
                                                  "deloy_configdir_before" : "/data/www/config1_20180605_144705",  //配置发布之前的真实路径
                                                  "deloy_prodir_after" : "/data/www/html1_20180624_160902",  //工程发布之后的真实路径
                                                  "deloy_prodir_before" : "/data/www/html1_20180605_144705",  //工程发布之前的真实路径
                                                  "end_time" : "2018-06-24 16:10:38",  //执行发布的结束时间
                                                  "randomStr" : "1529856609597174600"  //执行发布的唯一随机字符串
                                             }, 
                                             "k" : "server1",  //名字
                                             "myAppStatus" : "1529856730777509900" 
                                        }, 
                                   }, 
                                   "k" : "server1",  //名字
                                   "rollstatus" : "0" 
                              }, 
                              "mainProcess" : "20838"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "app_key" : "server2",  //返回服务器唯一名字
                                   "data" : {  //返回数据
                                        "0" : {  //返回第1个数据
                                             "config" : { 
                                                  "access_log" : { 
                                                  }, 
                                                  "app_key" : "server2",  //返回服务器唯一名字
                                                  "auto_deloy" : "1", 
                                                  "backupdir_same_level" : "0", 
                                                  "config_dir" : "/data/www/config2", 
                                                  "container_dir" : "/usr/local/tomcat-server2", 
                                                  "cpu" : "4核", 
                                                  "created_time" : "2018-03-21 10:30:15", 
                                                  "depart_name" : "", 
                                                  "deploydir_same_level" : "0", 
                                                  "disk" : "100G(本地盘)", 
                                                  "header" : "Content-Type:application/json", 
                                                  "id" : "2", 
                                                  "is_deloy_dir" : "2", 
                                                  "is_restart_status" : "0", 
                                                  "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                                  "local_name" : "server", 
                                                  "log_dir" : "/data/log/server2", 
                                                  "mask" : { 
                                                  }, 
                                                  "mem" : "8GB", 
                                                  "network_ip" : "192.168.0.76", 
                                                  "note" : "", 
                                                  "params" : "{"cmd":"uptime"}", 
                                                  "pro_dir" : "/data/www/html2", 
                                                  "pro_init" : "/etc/init.d/tomcat-server2", 
                                                  "pro_key" : "tomcat-server2", 
                                                  "pro_port" : "7080", 
                                                  "pro_type" : "tomcat", 
                                                  "require" : "inputStr", 
                                                  "requirecode" : "200", 
                                                  "server_name" : "后台服务集群2", 
                                                  "status" : "1", 
                                                  "system_type" : "centos7.1", 
                                                  "updated_time" : "2018-03-21 10:30:15", 
                                                  "url" : "http://127.0.0.1:7080/packController/runCmd.do?cmd=uptime" 
                                             }, 
                                             "getLastDeloy" : { 
                                                  "deloy_configdir_after" : "/data/www/config2_20180624_160902",  //配置发布之后的真实路径
                                                  "deloy_configdir_before" : "/data/www/config2_20180605_144705",  //配置发布之前的真实路径
                                                  "deloy_prodir_after" : "/data/www/html2_20180624_160902",  //工程发布之后的真实路径
                                                  "deloy_prodir_before" : "/data/www/html2_20180605_144705",  //工程发布之前的真实路径
                                                  "end_time" : "2018-06-24 16:10:39",  //执行发布的结束时间
                                                  "randomStr" : "1529856609826720700"  //执行发布的唯一随机字符串
                                             }, 
                                             "k" : "server2",  //名字
                                             "myAppStatus" : "1529856731298913400" 
                                        }, 
                                   }, 
                                   "k" : "server2",  //名字
                                   "rollstatus" : "0" 
                              }, 
                              "mainProcess" : "20838"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "20838",  //当前命令的进程ID
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "rollback",  //请求模块
          "k" : "server",  //名字
          "rollstatus" : "0", 
          "token" : "1529853651coqjifffyedvdqfnrghhphcadjgctexi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "rollback,--k="server",--rollstatus="0",--token="1529853651coqjifffyedvdqfnrghhphcadjgctexi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  rollback --k="server" --rollstatus="0" --token="1529853651coqjifffyedvdqfnrghhphcadjgctexi" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "20"  //执行命令花费时间
 }

```

