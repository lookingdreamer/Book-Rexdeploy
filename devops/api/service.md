# 基于名字服务控制

**简要描述：**     

- 基于名字服务控制     

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    service   |
|k |  是  |    string   |    名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),<br>如果多个app_key,用空格隔开   |
|a |  是  |    string   |    start启动 stop关闭 restart重启   |
|f |  否  |    string   |    自定义启动文件,如/usr/local/tomcat-server1/bin/startup.sh    |
|key |  否  |    string   |    自定义应用关键词,如tomcat-server1     |


**详细说明：**    
当不传f时,默认启动文件参考数据库配置pro_init         
当不传key时,默认应用关键词数据库配置pro_key

**请求示例**     

`a=start&action=service&k=server`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "app_key" : "server1",  //返回服务器唯一名字
                                   "before" : { //启动前的信息统计
                                        "processNumber" : "2"  //启动前的应用进程数量
                                   }, 
                                   "config" : { 
                                        "access_log" : { 
                                        }, 
                                        "action" : "start",  //请求模块
                                        "app_key" : "server1",  //返回服务器唯一名字
                                        "auto_deloy" : "1", 
                                        "backupdir_same_level" : "0", 
                                        "config_dir" : "/data/www/config1", 
                                        "container_dir" : "/usr/local/tomcat-server1", 
                                        "cpu" : "4核", 
                                        "created_time" : "2018-03-21 10:29:50", 
                                        "depart_name" : "", 
                                        "deploydir_same_level" : "0", 
                                        "disk" : "100G(本地盘)", 
                                        "header" : "Content-Type:application/json", 
                                        "id" : "1", 
                                        "is_deloy_dir" : "2", 
                                        "is_restart_status" : "0", 
                                        "jsonfile" : "", 
                                        "key" : "", 
                                        "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                        "local_name" : "server", 
                                        "log_dir" : "/data/log/server1", 
                                        "mask" : { 
                                        }, 
                                        "mem" : "8GB", 
                                        "network_ip" : "192.168.0.207", 
                                        "note" : "", 
                                        "params" : "{"cmd":"uptime"}", 
                                        "pro_dir" : "/data/www/html1", 
                                        "pro_init" : "/etc/init.d/tomcat-server1", 
                                        "pro_key" : "tomcat-server1", 
                                        "pro_port" : "6080", 
                                        "pro_type" : "tomcat", 
                                        "require" : "inputStr", 
                                        "requirecode" : "200", 
                                        "server_name" : "后台服务集群1", 
                                        "services_file" : "", 
                                        "status" : "1", 
                                        "system_type" : "centos7.1", 
                                        "updated_time" : "2018-03-21 10:29:50", 
                                        "url" : "http://127.0.0.1:6080/packController/runCmd.do?cmd=uptime" 
                                   }, 
                                   "msg" : "(server1) process is started.",
                                   "service_start_retry" : "5",  //启动或者关闭失败重试参数
                                   "status" : "0" 
                              }, 
                              "mainProcess" : "23508"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "app_key" : "server2",  //返回服务器唯一名字
                                   "before" : {  //启动前的信息统计
                                        "processNumber" : "2"  //启动前的应用进程数量
                                   }, 
                                   "config" : { 
                                        "access_log" : { 
                                        }, 
                                        "action" : "start",  //请求模块
                                        "app_key" : "server2",  //返回服务器唯一名字
                                        "auto_deloy" : "1", 
                                        "backupdir_same_level" : "0", 
                                        "config_dir" : "/data/www/config2", 
                                        "container_dir" : "/usr/local/tomcat-server2", 
                                        "cpu" : "4核", 
                                        "created_time" : "2018-03-21 10:30:15", 
                                        "depart_name" : "", 
                                        "deploydir_same_level" : "0", 
                                        "disk" : "100G(本地盘)", 
                                        "header" : "Content-Type:application/json", 
                                        "id" : "2", 
                                        "is_deloy_dir" : "2", 
                                        "is_restart_status" : "0", 
                                        "jsonfile" : "", 
                                        "key" : "", 
                                        "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                        "local_name" : "server", 
                                        "log_dir" : "/data/log/server2", 
                                        "mask" : { 
                                        }, 
                                        "mem" : "8GB", 
                                        "network_ip" : "192.168.0.76", 
                                        "note" : "", 
                                        "params" : "{"cmd":"uptime"}", 
                                        "pro_dir" : "/data/www/html2", 
                                        "pro_init" : "/etc/init.d/tomcat-server2", 
                                        "pro_key" : "tomcat-server2", 
                                        "pro_port" : "7080", 
                                        "pro_type" : "tomcat", 
                                        "require" : "inputStr", 
                                        "requirecode" : "200", 
                                        "server_name" : "后台服务集群2", 
                                        "services_file" : "", 
                                        "status" : "1", 
                                        "system_type" : "centos7.1", 
                                        "updated_time" : "2018-03-21 10:30:15", 
                                        "url" : "http://127.0.0.1:7080/packController/runCmd.do?cmd=uptime" 
                                   }, 
                                   "msg" : "(server2) process is started.", 
                                   "service_start_retry" : "5", //启动或者关闭失败重试参数
                                   "status" : "0" 
                              }, 
                              "mainProcess" : "23508"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "23508",  //当前命令的进程ID
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "a" : "start", 
          "action" : "service",  //请求模块
          "k" : "server",  //名字
          "token" : "1529853651coqjifffyedvdqfnrghhphcadjgctexi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "service,--a="start",--k="server",--token="1529853651coqjifffyedvdqfnrghhphcadjgctexi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  service --a="start" --k="server" --token="1529853651coqjifffyedvdqfnrghhphcadjgctexi" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "5"  //执行命令花费时间
 }

```



