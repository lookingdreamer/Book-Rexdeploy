# 基于名字校验远程服务器信息


## 基于名字校验远程服务器信息
- check

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字校验远程服务器信息| rex  [module] --k='[k]'| 


**示例1**  
校验名字为server1 server2的应用信息
```
rex check --k='serve1 server2'
```



**参数用法**  
k: 名字 参见数据库字段的app_key(https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）                 
w: 等于1,JSON格式输出结果


**备注**  
该模块会校验远程服务器的程序/配置/进程等信息

**截图**  
![](../assets/check1.gif)


