# 基于名字校验url


## 基于名字校验url
- checkurl

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字校验url| rex  [module] --k='[k]'| 


**示例1**  
校验名字分组为server的应用是否正常
```
rex checkurl --k='server'
```



**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）                  
w: 等于1,JSON格式输出结果


**备注**  
校验应用之前需要录入数据库的数据  
比如校验server1的post接口是否正常    
```
UPDATE pre_server_detail
SET
 url = 'http://127.0.0.1:6080/packController/runCmd.do?cmd=uptime',
 header = 'Content-Type:application/json',
 params = '{"cmd":"uptime"}',
 `require` = 'inputStr',
 requirecode = '200'
WHERE
    app_key = 'server1';
```

 其中当params 不传时为get请求，当params 有参数为post请求。     
 header有多个时用分号;间隔。      
 require的意思是返回内容校验，requirecode 返回状态码校验。           
 如上示例，校验返回内容是否有inputStr 以及返回状态是否为200



**截图**  
示例
![](../assets/checkurl.gif)


