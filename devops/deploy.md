# 基于名字自动灰度发布

## 原理

* 定义
 一般是取出一个或者多个服务器停止服务，执行更新，并重新将其投入使用。周而复始，直到集群中所有的实例都更新成新版本。

![Alt text](https://book.osichina.net/assets/QQ%E6%88%AA%E5%9B%BE20180416142751.png)

具体的超级实践说明,请参考: [高级实践(1) 基于名字服务的灰度发布](https://book.osichina.net/huidufabu.html)

## 基于名字自动灰度发布
- deploy

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字自动灰度发布| rex  [module] --k='[k]'| 


**示例1(跨环境自动取包发布)**  
将uat环境的server应用同步发布带当前生产环境
```
rex deploy --k='server' --senv='uat'
```


**示例2(手动上传更新包发布)**  
手动将代码和配置上传到当前的程序和配置目录,半自动灰度发布
```
rex deploy --k='server'
```


**参数用法**  
k: load_key_sorts(可在配置中自定义)数据表定义的local_name    （必选）        
senv: 自动取包源环境,该环境在config.yml中配置,你可以自定义其他环境。           
w: 等于1,JSON格式输出结果


**备注**  
   
同时自动灰度发布主要支持2种模式，如上的示例 1 2  

- 示例1(跨环境自动取包发布)  
比如你在开发环境(dev)和预生产环境(uat)同时部署了rexdeploy.      同时在config.yml中配置了uat和dev环境的数据([参考配置](https://book.osichina.net/an-zhuang/pei-zhi.html))    
现在要将dev环境的server应用同步发布到uat环境,在uat环境执行`rex deploy --k='server' --senv='dev'`即可   

- 示例2(手动上传更新包发布)   
这个发布相对前2步多了一个步骤,需要把程序和配置包上传到updatedir对应的程序和配置目录.      
比如发布server应用    
(1)执行同步下载程序和配置      
`rex  Enter:route:download  --k='server' --update='1'`      
该步骤会把正在使用的server应用的程序和配置下载到updatedir/softdir/server/和updatedir/configuredir/server目录     
(2)覆盖程序和修改配置        
上传包在 updatedir/softdir/server/ 上传配置在 updatedir/configuredir/server     
(3)执行发布    
`rex  deploy --k='server'`      


**截图**  
示例1
![](../assets/deploy11.gif)

示例2
![](../assets/deploy22.gif)

