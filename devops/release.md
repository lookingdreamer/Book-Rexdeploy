# 基于名字自动发布

## 原理
![](https://book.osichina.net/assets/yuanli.png)

## 基于名字自动发布
- release

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字自动发布| rex  [module] --k='[k]' --senv='uat'| 


**示例1(跨环境自动取包发布)**  
将uat环境的server应用同步发布带当前生产环境
```
rex release --k='server' --senv='uat'
```

**示例2(自动下载更新包发布)**  
将http服务器上server.war包增量发布到当前环境的server应用
```
rex release --k='server' --url='http://10.0.0.3/test.tar.gz' --full='1'
```

**示例3(手动上传更新包发布)**  
手动将代码和配置上传到当前的程序和配置目录,半自动发布
```
rex release --k='server'
```


**参数用法**  
k: 名字(app_key)或者名字分组(local_name) 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
senv: 自动取包源环境,该环境在config.yml中配置,你可以自定义其他环境。   
url: http下载更新包   
full: 1增量更新 2全量更新         
w: 等于1,JSON格式输出结果


**备注**  
自动更新支持名字或者local_name的名字分组的集群更新。   
同时自动发布主要支持3种模式，如上的示例 1 2 3 

- 示例1(跨环境自动取包发布)  
比如你在开发环境(dev)和预生产环境(uat)同时部署了rexdeploy.      同时在config.yml中配置了uat和dev环境的数据([参考配置](https://book.osichina.net/an-zhuang/pei-zhi.html))    
现在要将dev环境的server应用同步发布到uat环境,在uat环境执行`rex release --k='server' --senv='dev'`即可   

- 示例2(自动下载更新包发布)   
如示例2下载更新包直接发布到当前环境,如果是全量full=2   

- 示例3(手动上传更新包发布)   
这个发布相对前2步多了一个步骤,需要把程序和配置包上传到updatedir对应的程序和配置目录.      
比如发布server应用    
(1)执行同步下载程序和配置      
`rex  Enter:route:download  --k='server' --update='1'`      
该步骤会把正在使用的server应用的程序和配置下载到updatedir/softdir/server/和updatedir/configuredir/server目录     
(2)覆盖程序和修改配置        
上传包在 updatedir/softdir/server/ 上传配置在 updatedir/configuredir/server     
(3)执行发布    
`rex  release --k='server'`      


**截图**  
示例1
![](../assets/release1.gif)
示例2
![](../assets/release2.gif)
示例3
![](../assets/release3.gif)

