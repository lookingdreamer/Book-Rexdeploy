# 基于名字自动回滚


## 基于名字自动回滚
- rollback

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字自动回滚| rex  [module] --k='[k]' --rollstatus='0/1'| 


**示例1(回滚到上一版本)**  
将名字分组为server的应用回滚到上一版本
```
rex rollback --k='server' --rollstatus='0'
```

**示例2(回滚到任意版本)**  
将名字分组为server的应用回滚到任意版本
```
rex rollback --k='server' --rollstatus='1'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
rollstatus: 为0时,回滚到上一版本;rollstatus为1 根据数据库字段rollStatus=1回滚到任意版本  （必选）          
w: 等于1,JSON格式输出结果


**备注**  
当rollstatus=1时，该命令会去查询CMDB主机配置表(pre_server_detail)，
当local_name='server' 和 rollStatus=1时的记录回滚   


**截图**  
示例1
![](../assets/rollback.gif)


