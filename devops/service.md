# 基于名字服务控制


## 基于名字服务控制
- service

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字服务控制| rex  [module] --k='[k]' --a='start/stop/restart'| 


**示例1(启动)**  
将名字分组为server的应用启动
```
rex service --k='server' --a='start'
```

**示例2(关闭)**  
将名字分组为server的应用关闭
```
rex service --k='server' --a='stop'
```

**示例3(重启)**  
将名字分组为server的应用重启
```
rex service --k='server' --a='restart'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
a: start启动 stop关闭 restart重启  （必选）      
f: 自定义启动文件,如/usr/local/tomcat-server1/bin/startup.sh                     
key: 自定义应用关键词,如tomcat-server1                      
w: 等于1,JSON格式输出结果    


**备注**  
当不传f时,默认启动文件参考数据库配置pro_init    
当不传key时,默认应用关键词数据库配置pro_key


**截图**  
示例1 && 示例2 && 示例3 
![](../assets/service1.gif)


