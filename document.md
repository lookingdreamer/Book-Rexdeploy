# 模块概览

## 批量执行命令

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| run | [CLI](https://book.osichina.net/cmd/run.html) | [API](https://book.osichina.net/cmd/api/run.html) | 根据名字服务或者名字分组批量执行命令 | 
| Common:Use:run | [CLI](https://book.osichina.net/cmd/Common_Use_run.html) | [API](https://book.osichina.net/cmd/api/Common_Use_run3.html) | 根据多个IP或者IP分组批量执行命令 | 


## 批量文件分发

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| download | [CLI](https://book.osichina.net/piliangwenjianfenfa/download.html) | [API](https://book.osichina.net/piliangwenjianfenfa/api/download.html) | 根据名字服务或者名字分组批量下载文件 | 
| Common:Use:download | [CLI](https://book.osichina.net/piliangwenjianfenfa/piliangwendownload.html) | [API](https://book.osichina.net/piliangwenjianfenfa/api/Common_Use_download2.html) | 根据多个IP或者IP分组批量下载文件 | 
| upload | [CLI](https://book.osichina.net/piliangwenjianfenfa/upload.html) | [API](https://book.osichina.net/piliangwenjianfenfa/api/upload.html) | 根据名字服务或者名字分组批量上传文件 | 
| Common:Use:upload| [CLI](https://book.osichina.net/piliangwenjianfenfa/piliangwenupload.html) | [API](https://book.osichina.net/piliangwenjianfenfa/api/Common_Use_upload2.html) | 根据多个IP或者IP分组批量上传文件 | 


## 应用发布相关

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| check | [CLI](https://book.osichina.net/devops/check.html) | [API](https://book.osichina.net/devops/api/check.html) | 根据名字服务校验远程服务器信息 | 
| deploy | [CLI](https://book.osichina.net/devops/deploy.html) | [API](https://book.osichina.net/devops/api/deploy.html) | 根据名字服务自动灰度发布 | 
| release | [CLI](https://book.osichina.net/devops/release.html) | [API](https://book.osichina.net/devops/api/release.html) | 根据名字服务或者名字分组自动发布 | 
| rollback | [CLI](https://book.osichina.net/devops/rollback.html) | [API](https://book.osichina.net/devops/api/rollback.html) | 根据名字服务或者名字分组自动回滚 | 
| service | [CLI](https://book.osichina.net/devops/service.html) | [API](https://book.osichina.net/devops/api/service.html) | 根据名字服务或者名字分组进行服务控制 | 
| checkurl | [CLI](https://book.osichina.net/devops/checkurl.html) | [API](https://book.osichina.net/devops/api/checkurl.html) | 根据名字服务或者名字分组校验url |
| Enter:route:download | [CLI](https://book.osichina.net/devops/Enter_route_download.html) | [API](https://book.osichina.net/devops/api/Enter_route_download.html) | 根据名字或名字分组下载程序和配置包 | 
| Enter:route:deploy | [CLI](https://book.osichina.net/devops/Enter_route_deploy.html) | [API](https://book.osichina.net/devops/Enter_route_deploy.html) | 根据名字服务自动发布(旧) | 
| Enter:route:rollback | [CLI](https://book.osichina.net/devops/Enter_route_rollback.html) | [API](https://book.osichina.net/book.osichina.net/devops/Enter_route_rollback.html) | 根据名字服务自动回滚(旧) | 
| Enter:route:service | [CLI](https://book.osichina.net/devops/Enter_route_service.html) | [API](https://book.osichina.net/devops/Enter_route_service.html) | 根据名字服务进行服务控制(旧) | 
| Enter:deploy:main | [CLI](https://book.osichina.net/devops/Enter_deploy_main.html) | [API](https://book.osichina.net/devops/api/Enter_deploy_main.html) | 根据名字服务手动灰度发布 | 
| Deploy:Db:initdb | [CLI](https://book.osichina.net/devops/Deploy_Db_initdb.html) | [API](https://book.osichina.net/devops/api/Deploy_Db_initdb.html) | 应用发布初始化 | 
| Deploy:Core:syncpro | [CLI](https://book.osichina.net/devops/Deploy_Core_syncpro.html) | [API](https://book.osichina.net/devops/api/Deploy_Core_syncpro.html) | 根据名字服务进行程序和配置包本地同步 | 
| Deploy:Core:diff | [CLI](https://book.osichina.net/devops/Deploy_Core_diff.html) | [API](https://book.osichina.net/devops/api/Deploy_Core_diff.html) | 根据名字服务校验程序和配置包 | 
| Predeploy:Judge:accesslog | [CLI](https://book.osichina.net/devops/Predeploy_Judge_accesslog.html) | [API](https://book.osichina.net/devops/Predeploy_Judge_accesslog.html) | 根据名字服务校验日志文件md5值 | 
| Enter:route:downloadCombile | [CLI](https://book.osichina.net/devops/Enter_route_downloadCombile.html) | [API](https://book.osichina.net/devops/api/Enter_route_downloadCombile.html) | 根据名字服务下载http包并合并 | 
| Deploy:Configure:config | [CLI](https://book.osichina.net/devops/Deploy_Configure_config.html) | [API](https://book.osichina.net/devops/api/Deploy_Configure_config.html) | 根据名字服务配置文件初始化 | 
## 用户管理相关

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
|  User:main:route | [CLI](https://book.osichina.net/yonghuguanli.html) | [API](https://book.osichina.net/yonghuguanli/api/User_main_route.html) | 用户管理: 查询/增加/删除/锁定/批量操作 | 


## 日志模块相关

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| getLog | [CLI](https://book.osichina.net/rizhi/getLog.html) | [API](https://book.osichina.net/rizhi/api/getLog.html) | 根据名字服务或者名字分组批量下载日志 | 
| logCenter:main:getLog | [CLI](https://book.osichina.net/rizhi/rizhijizhongguanli.html) | [API](https://book.osichina.net/rizhi/api/logCenter_main_getLog3.html) | 根据多个IP或者IP分组批量下载文件 | 
| grepLog | [CLI](https://book.osichina.net/rizhi/grepLog.html) | [API](https://book.osichina.net/rizhi/api/grepLog.html) | 根据名字服务或者名字分组批量过滤日志 | 
| logCenter:main:grepLog | [CLI](https://book.osichina.net/rizhi/rizhijizhongguanli.html) | [API](https://book.osichina.net/rizhi/api/logCenter_main_grepLog3.html) | 根据多个IP或者IP分组批量过滤日志 | 
| lookLog | [CLI](https://book.osichina.net/rizhi/lookLog.html) | [API](https://book.osichina.net/rizhi/api/lookLog.html) | 根据名字服务或者名字分组批量查看日志列表 | 
| logCenter:main:lookLog | [CLI](https://book.osichina.net/rizhi/rizhijizhongguanli.html) | [API](https://book.osichina.net/rizhi/api/logCenter_main_lookLog3.html) | 根据多个IP或者IP分组批量查看日志列表 | 
| logCenter:main:liveLog | [CLI](https://book.osichina.net/rizhi/rizhijizhongguanli.html) | [API](https://book.osichina.net/rizhi/rizhijizhongguanli.html) | 根据多个IP或者IP分组批量实时日志 | 
| logCenter:main:queryList | [CLI](https://book.osichina.net/rizhi/rizhijizhongguanli.html) | [API](https://book.osichina.net/rizhi/api/logCenter_main_lookLog3.html) | 查询当前所有日志列表 | 


## 负载均衡相关(腾讯云)

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| queryk | [CLI](https://book.osichina.net/loadblance/queryk.html) | [API](https://book.osichina.net/loadblance/api/queryk.html) | 根据名字或分组批量查询负载均衡/多进程 | 
| loadService:main:queryk | [CLI](https://book.osichina.net/loadblance/loadService_main_queryk.html) | [API](https://book.osichina.net/loadblance/api/queryk.html) | 根据名字或分组批量查询负载均衡/单进程 | 
| loadService:main:query | [CLI](https://book.osichina.net/loadblance/loadService_main_query.html) | [API](https://book.osichina.net/loadblance/api/loadService_main_query.html) | 根据负载均衡ID查询负载均衡 | 
| updatek  | [CLI](https://book.osichina.net/loadblance/updatek.html) | [API](https://book.osichina.net/loadblance/api/updatek.html) | 根据名字或分组批量更新负载均衡权重/多进程  | 
| loadService:main:update  | [CLI](https://book.osichina.net/loadblance/loadService_main_update.html) | [API](https://book.osichina.net/loadblance/api/updatek.html) | 根据名字或分组批量更新负载均衡权重/单进程  | 
| loadService:main:check  | [CLI](https://book.osichina.net/loadblance/loadService_main_check.html) | [API](https://book.osichina.net/devops/api/checkurl.html) | 根据名字或分组校验url/单进程  |

## 对象存储相关(腾讯云COS)

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| Cos:Upload:txy | [CLI](https://book.osichina.net/devops/loadService_main_check111.html) | [API](https://book.osichina.net/devops/loadService_main_check111.html) | 根据名字服务上传和备份cos | 

## get/post相关

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| Common:Use:get | [CLI](https://book.osichina.net/moni/get.html) | [API](https://book.osichina.net/moni/api/Common_Use_get.html) | 模拟get请求 | 
| Common:Use:post | [CLI](https://book.osichina.net/moni/post.html) | [API](https://book.osichina.net/moni/api/Common_Use_post.html) | 模拟post请求 | 

## 其他

| 模块名 | 命令行文档 | API文档 | 模块说明 |
| :---: | :--- | :--- | :--- | 
| list | [CLI](https://book.osichina.net/list.html) | [API](https://book.osichina.net/list_api.html) | 查询所有已配置的名字服务列表 | 



关于名字分组和IP分组的区别和概念，请参照: [分组概念](fu-lu/group.html)




