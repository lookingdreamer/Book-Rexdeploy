# 二次开发

Rexdeploy的目录结构，可以参见上上章节 [目录结构](https://book.osichina.net/mulujiegou.html)  
其中lib目录是主要的开发目录，Rexfile为对应的入口文件  

二次开发主要有以下分类：

- Docker模式下开发

- vagrant+virtualbox模式下开发

- Mac下开发

- 其他操作系统下开发

## 二次开发注意事项

- Docker模式下开发

将docker目录共享到/data/RexDeployV3,同时将mysql的3306开放出来,方便在本地连接数据库调试 
共享目录之后，然后在本地修改在docker容器进行调试工程即可

我的示例: 
```
 docker run  -it -d -p 3306:3306 -v /Users/huanggaoming/myProject/RexDeployV3:/data/RexDeployV3   rexdeploy:3.0
```

- vagrant+virtualbox模式下开发

也和docker的开发模式类似，默认共享RexDeployV3到/data/RexDeployV3，本地修改工程实时同步在virtualbox中

- Mac下开发 和 其他操作系统下开发

实时修改，直接工程里修改生效。


## 二次开发步骤

举例: 根据关键词替换配置文件 
比如 安装zabbix时，我们需要配置zabbix_agentd.conf中的Hostname,Hostname可以配置IP.或者自定义识别的名字，那么我们就可以使用数据库的app_key字段替换为Hostname的值  

### 步骤(1) rexify初始化模块

- 使用

rexify 主模块名::子模块名 --create-module

- 示例

在Rexfile同级目录执行该操作，会在当前lib目录下形成初始化模块，如下截图   

```
rexify Zabbix::Conf --create-module
```

![](./assets/reify.png)


### 步骤(2) 添加模块至入口文件Rexfile中

添加以下到Rexfile开头部分

```
use Zabbix::Conf ;
```

### 步骤(3) 添加模块显示配置

添加模块配置到主配置文件中，将`Zabbix:Conf:example`添加到主配置文件config.yml中list_task字段中


### 步骤(4) 进入开发调试
执行`rex -T`,就可以看到Zabbix:Conf:example的方法，同时执行`rex Zabbix:Conf:example`就可以调试开发该模块了  

详细开发示例也可以参考：[http://rex.osichina.net/howtos/using_templates.html](http://rex.osichina.net/howtos/using_templates.html)

## 参考
- 少量的perl基础 
[http://rex.osichina.net/howtos/perl.html](http://rex.osichina.net/howtos/perl.html)
- rex指南
[http://rex.osichina.net/howtos/index.html#guides](http://rex.osichina.net/howtos/index.html#guides)



