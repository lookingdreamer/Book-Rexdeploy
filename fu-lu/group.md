# 分组概念
分组概念主要分为名字分组和IP分组

## 名字分组
名字分组集就是基于名字服务的分组，比如案例中的server1,server2,将数据库字段的local_name设置为server,server就是server1,server2的分组名， 那么凡是支持名字分组的模块都是可以通过分组名来调用

## IP分组
IP分组代表的是模块在IP分组的机器中执行，如何定义IP分组可以参考`config/ip_lists.ini`文件


