
# 说明

此自动化的系统是基于原生rex进行二次开发而成,这是第三个大版本了，目前放出的这个版本是`终端版`，web版本已开发完成，预计5月份将会放出。

此前放出过第一个版本，详情请撮：

[http://git.oschina.net/lookingdreamer/RexDeploy\_v1](http://git.oschina.net/lookingdreamer/RexDeploy_v1)

新版本git地址: 

RexDeployV3 码云地址: [https://gitee.com/lookingdreamer/RexDeployV3](https://gitee.com/lookingdreamer/RexDeployV3)  
RexDeployV3 Github地址: [https://github.com/LookingDreamer/RexDeployV3](https://github.com/LookingDreamer/RexDeployV3)  

有任何问题欢迎提issue

rex官方：[http://rexify.org/](http://rexify.org/)  
rex中文: [http://rex.osichina.net](http://rex.osichina.net/)  
rex github: [https://github.com/RexOps/Rex ](https://github.com/RexOps/Rex)

由于国内有时候访问不了rex官方，本人根据[官方git](https://github.com/RexOps/rexify-website)重新拉取了一份中文官方网站部署在搬瓦工上，有需要请自行浏览。

