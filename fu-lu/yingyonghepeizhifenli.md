# 应用和配置分离

我们在实际生产过程当中会遇到这种情况，有时候在开发侧 把配置和应用绑定在一起。 
作为运维侧的我们，每次发布的时候都要处理配置。 在建立配置标准化，我们也是  
强烈要求 配置和应用 隔离出来。  
虽然做了硬性规定，但是有些历史应用配置隔离在开发侧修改比较麻烦。所以我们改变方向，在应用容器层去做修改，而开发不需要修改任何代码。

以 tomcat 为例 

## 修改前 tomcat应用

```
# ls WEB-INF/classes/ -l
-rw-r--r-- 1 root root 4739 Mar 14 22:59 applicationContext.xml
drwxr-xr-x 3 root root 4096 Mar 14 23:33 com
-rw-r--r-- 1 root root  452 Mar 14 23:33 config.properties
-rw-r--r-- 1 root root 2365 Mar 14 22:59 generatorConfig.xml
-rw-r--r-- 1 root root  670 Mar 14 22:59 log4j.properties
```

配置在WEB-INF/classes/下

## 配置隔离修改

- 1.修改tomcat catalina.properties 配置

修改前
```
shared.loader=""
```

修改后
```
shared.loader="/data/www/config1"
```


- 2.删除WEB-INF/classes/ 下涉及的配置文件
- 3.将配置文件移动到 /data/www/config1 目录中


以上就是tomcat实现配置和应用隔离方法，其实是利用了classloader的加载顺序。  

Bootstrap ---> System ---> /WEB-INF/classes ---> /WEB-INF/lib/*.jar ---> Common ---> Server ---> Shared 

Common的配置是通过 catalina.properties中的common.loader设置   
Server的配置是通过 catalina.properties中的Server.loader设置    
Shared的配置是通过 catalina.properties中的Shared.loader设置     
