# 基于名字服务的服务管理

基于名字服务的服务管理，即通过名字就能控制服务的启动和停止。  
首先你需要接入应用，如何接入请参考[接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html),   
最关键的是   启动脚本(pro_init)、进程关键词(pro_key) 这2个配置项一定要配置好。 
当然你进行接入应用的配置时，也是可以自定义使用服务管理的，[自定义服务管理](https://book.osichina.net/fuwuguanli/zidingyifuwuguanli.html)