# 名字服务的服务管理

再次重复一下上章节说的。  
基于名字服务的服务管理，即通过名字就能控制服务的启动和停止。  
首先你需要接入应用，如何接入请参考[接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html),   
最关键的是   启动脚本(pro_init)、进程关键词(pro_key) 这2个配置项一定要配置好。

我们以案例中server1、server2为例 相关应用信息可参考 [https://book.osichina.net/an-zhuang.html](https://book.osichina.net/an-zhuang.html)

## 服务启动
```
rex Enter:route:service  --k='server1 server2'  --a='start'
```
如果服务已经存在不会重复启动 

## 服务停止
```
rex Enter:route:service  --k='server1 server2'  --a='stop'
```

## 服务重启
```
rex Enter:route:service  --k='server1 server2'  --a='restart'
```
