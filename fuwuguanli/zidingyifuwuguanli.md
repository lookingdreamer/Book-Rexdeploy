# 自定义服务管理

如果你想修改启动路径，或者修改进程关键词。可以使用自定义参数进行服务控制

我们仍然以案例中server1为例 相关应用信息可参考 [https://book.osichina.net/an-zhuang.html](https://book.osichina.net/an-zhuang.html)

--f: 启动脚本   
--key: 进程关键词  

## 服务启动 
```
rex Enter:route:service  --k='server1'  --a='start' --f='/usr/local/tomcat-server1/bin/startup.sh' --key='server1'
```


## 服务停止
```
rex Enter:route:service  --k='server1'  --a='stop' --f='/usr/local/tomcat-server1/bin/startup.sh' --key='server1'
```

## 服务重启
```
rex Enter:route:service  --k='server1'  --a='restart' --f='/usr/local/tomcat-server1/bin/startup.sh' --key='server1'
```
