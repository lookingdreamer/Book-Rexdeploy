# 高级应用篇

高级应用主要是利用官方API结合自己的业务情况，定制符合你的业务情况的rex.

rex API中文 [http://rex.osichina.net/api/index.html](http://rex.osichina.net/api/index.html)

rex API英文原版 [https://www.rexify.org/docs/api.html](https://www.rexify.org/docs/api.html)

基本上运维的常态工作API都有提供，如计划任务，防火墙，虚拟化kvm，docker等等，请自行参考。

