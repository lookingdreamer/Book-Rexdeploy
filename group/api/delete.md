# 分组删除

**简要描述：** 

- 分组删除

**请求URL：** 
- ` http://127.0.0.1:3000/rex/groupconfig?token=1531751262jfauoamjlxdguanpyyultntjkvqjpdaa `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|confFile |  否  |    string   |    配置文件config.yml路径,不传时默认fastnotes.json中rexConfig配置   |
|action |  是  |    string   |    delete   |
|codeVal |  是  |    string   |   如code=0,则codeVal为删除分组名,多个以,分隔 <br/>如code=1,则codeVal为删除分组名中IP,格式为 分组名1:IP1,分组名1:IP2 ,多个以,分隔    |
|code |  是  |    int   |    1或者0,见codeVal解释   |

**请求示例** 

`action=delete&codeVal=grouptest:192.168.1.111&code=1`

**返回示例**

``` 
{ 
     "code" : "0", 
     "conf" : "/data/RexdeployV3/config/ip_lists.ini", 
     "data" : { 
          "addIpCount" : "0", 
          "addgroupCount" : "0", 
          "after" : { 
               "group" : { 
                    "0" : "192.168.0.207", 
                    "1" : "192.168.0.76", 
               }, 
               "group1" : { 
                    "0" : "192.168.0.1", 
                    "1" : "192.168.0.76", 
               }, 
               "group3" : { 
                    "0" : "192.168.1.101", 
               }, 
               "grouptest" : { 
               } 
          }, 
          "before" : { 
               "group" : { 
                    "0" : "192.168.0.207", 
                    "1" : "192.168.0.76", 
               }, 
               "group1" : { 
                    "0" : "192.168.0.1", 
                    "1" : "192.168.0.76", 
               }, 
               "group3" : { 
                    "0" : "192.168.1.101", 
               }, 
               "grouptest" : { 
                    "0" : "192.168.1.111", 
               } 
          }, 
          "code" : "0", 
          "deleteIpCount" : "1", 
          "deletegroupCount" : "0", 
          "params" : { 
               "action" : "delete", 
               "delete" : { 
                    "0" : "grouptest:192.168.1.111", 
               }, 
               "deletecode" : "1", 
               "file" : "/data/RexdeployV3/config/ip_lists.ini" 
          } 
     }, 
     "msg" : "成功", 
     "param" : { 
          "action" : "delete", 
          "code" : "1", 
          "codeVal" : "grouptest:192.168.1.111", 
          "confFile" : "", 
          "group" : { 
          } 
     }, 
     "start_time" : "2018/07/16 15:04:42", 
     "take" : "0" 
 }

```

