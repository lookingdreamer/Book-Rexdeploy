# 分组查询

**简要描述：** 

- 分组查询


**请求URL：** 
- ` http://127.0.0.1:3000/rex/groupconfig?token=1531751262jfauoamjlxdguanpyyultntjkvqjpdaa `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|confFile |  否  |    string   |    配置文件config.yml路径,不传时默认fastnotes.json中rexConfig配置   |
|group |  否  |    string   |    IP分组名称,如果不传默认查询所有的分组   |

**请求示例** 

`group=group`

**返回示例**

``` 
{ 
     "code" : "0", 
     "conf" : "/data/RexdeployV3/config/ip_lists.ini", 
     "data" : { 
          "group" : { 
               "0" : "192.168.0.207", 
               "1" : "192.168.0.76", 
          } 
     }, 
     "msg" : "成功", 
     "param" : { 
          "action" : { 
          }, 
          "code" : { 
          }, 
          "codeVal" : { 
          }, 
          "confFile" : "", 
          "group" : "group" 
     }, 
     "start_time" : "2018/07/16 14:38:28", 
     "take" : "0" 
 }

```

