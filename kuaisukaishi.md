# 快速开始

部署完成之后，首先进入到RexdeployV3目录，执行  `rex -T` 。
> 必须进入到RexdeployV3执行，因为需要读取该目录下的初始化文件Rexfile.
> -T参数: 输出已有模块和简要的帮助文档，相当于命令行的--help

![](./assets/kuaisutiyan.gif)

比如你要查询[4台模拟客户端](https://book.osichina.net/an-zhuang.html)的启动时间

![](./assets/uptime.gif)