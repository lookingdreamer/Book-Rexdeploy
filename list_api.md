# 查询名字列表

**简要描述：** 

- 查询名字列表

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    无   |

**请求示例**     

`action=list`

**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "0", 
          "data" : { 
               "0" : "flow server", 
               "1" : "server1 server2 flow1 flow2 4", 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { 
          "action" : "list", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "list,--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  list --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "2" 
 }

```

