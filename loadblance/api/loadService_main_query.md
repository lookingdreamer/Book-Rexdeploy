# 根据负载均衡ID查询负载均衡

**简要描述：**     

- 根据负载均衡ID查询负载均衡  

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    loadService:main:query   |
|loadBalancerId |  是  |    string   |    负载均衡ID   |


**请求示例**     

`action=loadService:main:query&loadBalancerId=lb-0101010101`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "0", 
          "data" : { 
               "0" : { 
                    "backendSet" : { 
                         "0" : { 
                              "instanceId" : "qcvm00000000000000000000000000000001", 
                              "instanceName" : "[负载1]-后台服务集群1", 
                              "instanceStatus" : "2", 
                              "lanIp" : "192.168.0.207", 
                              "unInstanceId" : "ins-0001", 
                              "wanIpSet" : { 
                                   "0" : "8.8.8.149", 
                              }, 
                              "weight" : "10" 
                         }, 
                         "1" : { 
                              "instanceId" : "qcvm00000000000000000000000000000002", 
                              "instanceName" : "[负载2]-后台服务集群2", 
                              "instanceStatus" : "2", 
                              "lanIp" : "192.168.0.76", 
                              "unInstanceId" : "ins-0002", 
                              "wanIpSet" : { 
                                   "0" : "8.8.8.58", 
                              }, 
                              "weight" : "10" 
                         }, 
                         "2" : { 
                              "instanceId" : "qcvm00000000000000000000000000000003", 
                              "instanceName" : "[负载3]-后台服务集群3", 
                              "instanceStatus" : "2", 
                              "lanIp" : "192.168.0.248", 
                              "unInstanceId" : "ins-0003", 
                              "wanIpSet" : { 
                                   "0" : "8.8.8.206", 
                              }, 
                              "weight" : "10" 
                         }, 
                    }, 
                    "code" : "0", 
                    "codeDesc" : "Success", 
                    "message" : "", 
                    "totalCount" : "3" 
               }, 
          }, 
          "msg" : "æå" 
     }, 
     "param" : { 
          "action" : "loadService:main:query", 
          "loadBalancerId" : "lb-0101010101", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "loadService:main:query,--loadBalancerId="lb-0101010101",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  loadService:main:query --loadBalancerId="lb-0101010101" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "Wide character in print at /loader/0x2a81950/Common/Use.pm line 1028.
", 
          "stdout" : "" 
     }, 
     "take" : "2" 
 }

```

