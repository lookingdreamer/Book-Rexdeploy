# 基于名字的负载均衡查询

**简要描述：**     

- 基于名字的负载均衡查询    


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    queryk   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |

**详细说明：**      

关于负载均衡的配置参考[https://book.osichina.net/an-zhuang/shengji.html](https://book.osichina.net/an-zhuang/shengji.html)        
主要是配置loadBalancerId(负载均衡id1)


**请求示例**     

`action=queryk&k=server`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "app_key" : "server1",  //返回服务器唯一名字
                                   "code" : "0",  //0 成功 非0 失败
                                   "data" : {  //返回数据
                                        "0" : {  //返回第1个数据
                                             "k" : "server1",  //名字
                                             "loadBalance" : { 
                                                  "0" : {  //返回第1个数据
                                                       "backend" : { 
                                                       }, 
                                                       "backendSetLen" : "3", 
                                                       "loadBalance" : "lb-0101010101", 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : {  //返回第1个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : {  //返回第2个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : {  //返回第3个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0",  //0 成功 非0 失败
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       } 
                                                  }, 
                                                  "1" : {  //返回第2个数据
                                                       "backend" : { 
                                                       }, 
                                                       "backendSetLen" : "3", 
                                                       "loadBalance" : "lb-0202020202", 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : {  //返回第1个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : {  //返回第2个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : {  //返回第3个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0",  //0 成功 非0 失败
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       } 
                                                  }, 
                                             }, 
                                             "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                             "loadBalancerIdLength" : "2", 
                                             "network_ip" : "192.168.0.207" 
                                        }, 
                                   }, 
                                   "msg" : "success", 
                                   "params" : { 
                                        "file" : "", 
                                        "k" : "server1"  //名字
                                   } 
                              }, 
                              "mainProcess" : "13274"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "app_key" : "server2",  //返回服务器唯一名字
                                   "code" : "0",  //0 成功 非0 失败
                                   "data" : {  //返回数据
                                        "0" : {  //返回第1个数据
                                             "k" : "server2",  //名字
                                             "loadBalance" : { 
                                                  "0" : {  //返回第1个数据
                                                       "backend" : { 
                                                       }, 
                                                       "backendSetLen" : "3", 
                                                       "loadBalance" : "lb-0101010101", 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : {  //返回第1个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : {  //返回第2个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : {  //返回第3个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0",  //0 成功 非0 失败
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       } 
                                                  }, 
                                                  "1" : {  //返回第2个数据
                                                       "backend" : { 
                                                       }, 
                                                       "backendSetLen" : "3", 
                                                       "loadBalance" : "lb-0202020202", 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : {  //返回第1个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : {  //返回第2个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : {  //返回第3个数据
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206",  //返回第1个数据
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0",  //0 成功 非0 失败
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       } 
                                                  }, 
                                             }, 
                                             "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                             "loadBalancerIdLength" : "2", 
                                             "network_ip" : "192.168.0.76" 
                                        }, 
                                   }, 
                                   "msg" : "success", 
                                   "params" : { 
                                        "file" : "", 
                                        "k" : "server2"  //名字
                                   } 
                              }, 
                              "mainProcess" : "13274"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "13274",  //当前命令的进程ID
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "æå" 
     }, 
     "param" : { //请求参数
          "action" : "queryk",  //请求模块
          "k" : "server",  //名字
          "token" : "1529853651coqjifffyedvdqfnrghhphcadjgctexi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "queryk,--k="server",--token="1529853651coqjifffyedvdqfnrghhphcadjgctexi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  queryk --k="server" --token="1529853651coqjifffyedvdqfnrghhphcadjgctexi" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "Wide character in print at /loader/0x11c87a8/Common/Use.pm line 1028. //最终执行命令标准错误输出
", 
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "4"  //执行命令花费时间
 }

```

