# 基于名字的负载均衡修改

**简要描述：**     

- 基于名字的负载均衡修改    

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    updatek   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |
|weight |  是  |    int   |    要修改的权重   |

**请求示例**     

`action=updatek&k=server&weight=10`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "0", 
          "data" : { 
               "0" : { 
                    "code" : "0", 
                    "count" : "2", 
                    "data" : { 
                         "0" : { 
                              "data" : { 
                                   "app_key" : "server1", 
                                   "code" : "0", 
                                   "data" : { 
                                        "0" : { 
                                             "config" : { 
                                                  "access_log" : { 
                                                  }, 
                                                  "app_key" : "server1", 
                                                  "auto_deloy" : "1", 
                                                  "backupdir_same_level" : "0", 
                                                  "config_dir" : "/data/www/config1", 
                                                  "container_dir" : "/usr/local/tomcat-server1", 
                                                  "cpu" : "4æ ¸", 
                                                  "created_time" : "2018-03-21 10:29:50", 
                                                  "depart_name" : "", 
                                                  "deploydir_same_level" : "0", 
                                                  "disk" : "100G(æ¬å°ç)", 
                                                  "header" : "Content-Type:application/json", 
                                                  "id" : "1", 
                                                  "is_deloy_dir" : "2", 
                                                  "is_restart_status" : "0", 
                                                  "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                                  "local_name" : "server", 
                                                  "log_dir" : "/data/log/server1", 
                                                  "mask" : { 
                                                  }, 
                                                  "mem" : "8GB", 
                                                  "network_ip" : "192.168.0.207", 
                                                  "note" : "", 
                                                  "params" : "{"cmd":"uptime"}", 
                                                  "pro_dir" : "/data/www/html1", 
                                                  "pro_init" : "/etc/init.d/tomcat-server1", 
                                                  "pro_key" : "tomcat-server1", 
                                                  "pro_port" : "6080", 
                                                  "pro_type" : "tomcat", 
                                                  "require" : "inputStr", 
                                                  "requirecode" : "200", 
                                                  "server_name" : "åå°æå¡éç¾¤1", 
                                                  "status" : "1", 
                                                  "system_type" : "centos7.1", 
                                                  "updated_time" : "2018-03-21 10:29:50", 
                                                  "url" : "http://127.0.0.1:6080/packController/runCmd.do?cmd=uptime" 
                                             }, 
                                             "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                             "loadBalancerIdLength" : "2", 
                                             "network_ip" : "192.168.0.207", 
                                             "secondArray" : { 
                                                  "0" : { 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0", 
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       }, 
                                                       "updateArray" : { 
                                                            "0" : { 
                                                                 "ediRes" : { 
                                                                      "code" : "0", 
                                                                      "codeDesc" : "Success", 
                                                                      "message" : "", 
                                                                      "requestId" : "5242831" 
                                                                 }, 
                                                                 "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                 "lanIp" : "192.168.0.207", 
                                                                 "loadBalance" : "lb-0101010101", 
                                                                 "nowWeight" : "10" 
                                                            }, 
                                                       } 
                                                  }, 
                                                  "1" : { 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0", 
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       }, 
                                                       "updateArray" : { 
                                                            "0" : { 
                                                                 "ediRes" : { 
                                                                      "code" : "0", 
                                                                      "codeDesc" : "Success", 
                                                                      "message" : "", 
                                                                      "requestId" : "5242831" 
                                                                 }, 
                                                                 "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                 "lanIp" : "192.168.0.207", 
                                                                 "loadBalance" : "lb-0202020202", 
                                                                 "nowWeight" : "10" 
                                                            }, 
                                                       } 
                                                  }, 
                                             } 
                                        }, 
                                   }, 
                                   "msg" : "success", 
                                   "params" : { 
                                        "k" : "server1", 
                                        "w" : "10" 
                                   } 
                              }, 
                              "mainProcess" : "11476" 
                         }, 
                         "1" : { 
                              "data" : { 
                                   "app_key" : "server2", 
                                   "code" : "0", 
                                   "data" : { 
                                        "0" : { 
                                             "config" : { 
                                                  "access_log" : { 
                                                  }, 
                                                  "app_key" : "server2", 
                                                  "auto_deloy" : "1", 
                                                  "backupdir_same_level" : "0", 
                                                  "config_dir" : "/data/www/config2", 
                                                  "container_dir" : "/usr/local/tomcat-server2", 
                                                  "cpu" : "4æ ¸", 
                                                  "created_time" : "2018-03-21 10:30:15", 
                                                  "depart_name" : "", 
                                                  "deploydir_same_level" : "0", 
                                                  "disk" : "100G(æ¬å°ç)", 
                                                  "header" : "Content-Type:application/json", 
                                                  "id" : "2", 
                                                  "is_deloy_dir" : "2", 
                                                  "is_restart_status" : "0", 
                                                  "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                                  "local_name" : "server", 
                                                  "log_dir" : "/data/log/server2", 
                                                  "mask" : { 
                                                  }, 
                                                  "mem" : "8GB", 
                                                  "network_ip" : "192.168.0.76", 
                                                  "note" : "", 
                                                  "params" : "{"cmd":"uptime"}", 
                                                  "pro_dir" : "/data/www/html2", 
                                                  "pro_init" : "/etc/init.d/tomcat-server2", 
                                                  "pro_key" : "tomcat-server2", 
                                                  "pro_port" : "7080", 
                                                  "pro_type" : "tomcat", 
                                                  "require" : "inputStr", 
                                                  "requirecode" : "200", 
                                                  "server_name" : "åå°æå¡éç¾¤2", 
                                                  "status" : "1", 
                                                  "system_type" : "centos7.1", 
                                                  "updated_time" : "2018-03-21 10:30:15", 
                                                  "url" : "http://127.0.0.1:7080/packController/runCmd.do?cmd=uptime" 
                                             }, 
                                             "loadBalancerId" : "lb-0101010101,lb-0202020202", 
                                             "loadBalancerIdLength" : "2", 
                                             "network_ip" : "192.168.0.76", 
                                             "secondArray" : { 
                                                  "0" : { 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0", 
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       }, 
                                                       "updateArray" : { 
                                                            "0" : { 
                                                                 "ediRes" : { 
                                                                      "code" : "0", 
                                                                      "codeDesc" : "Success", 
                                                                      "message" : "", 
                                                                      "requestId" : "5242831" 
                                                                 }, 
                                                                 "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                 "lanIp" : "192.168.0.76", 
                                                                 "loadBalance" : "lb-0101010101", 
                                                                 "nowWeight" : "10" 
                                                            }, 
                                                       } 
                                                  }, 
                                                  "1" : { 
                                                       "loadInfo" : { 
                                                            "backendSet" : { 
                                                                 "0" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000001", 
                                                                      "instanceName" : "[负载1]-后台服务集群1", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.207", 
                                                                      "unInstanceId" : "ins-0001", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.149", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "1" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                      "instanceName" : "[负载2]-后台服务集群2", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.76", 
                                                                      "unInstanceId" : "ins-0002", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.58", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                                 "2" : { 
                                                                      "instanceId" : "qcvm00000000000000000000000000000003", 
                                                                      "instanceName" : "[负载3]-后台服务集群3", 
                                                                      "instanceStatus" : "2", 
                                                                      "lanIp" : "192.168.0.248", 
                                                                      "unInstanceId" : "ins-0003", 
                                                                      "wanIpSet" : { 
                                                                           "0" : "8.8.8.206", 
                                                                      }, 
                                                                      "weight" : "10" 
                                                                 }, 
                                                            }, 
                                                            "code" : "0", 
                                                            "codeDesc" : "Success", 
                                                            "message" : "", 
                                                            "totalCount" : "3" 
                                                       }, 
                                                       "updateArray" : { 
                                                            "0" : { 
                                                                 "ediRes" : { 
                                                                      "code" : "0", 
                                                                      "codeDesc" : "Success", 
                                                                      "message" : "", 
                                                                      "requestId" : "5242831" 
                                                                 }, 
                                                                 "instanceId" : "qcvm00000000000000000000000000000002", 
                                                                 "lanIp" : "192.168.0.76", 
                                                                 "loadBalance" : "lb-0202020202", 
                                                                 "nowWeight" : "10" 
                                                            }, 
                                                       } 
                                                  }, 
                                             } 
                                        }, 
                                   }, 
                                   "msg" : "success", 
                                   "params" : { 
                                        "k" : "server2", 
                                        "w" : "10" 
                                   } 
                              }, 
                              "mainProcess" : "11476" 
                         }, 
                    }, 
                    "mainProcess" : "11476", 
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "æå" 
     }, 
     "param" : { 
          "action" : "updatek", 
          "k" : "server", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb", 
          "weight" : "10" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "updatek,--k="server",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--weight="10",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  updatek --k="server" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --weight="10" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "Wide character in print at /loader/0x253d788/Common/Use.pm line 1028.
", 
          "stdout" : "" 
     }, 
     "take" : "5" 
 }

```

