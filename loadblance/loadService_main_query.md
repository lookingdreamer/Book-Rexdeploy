# 根据负载均衡ID查询负载均衡

和基于名字的负载均衡查询的区别就是需要传负载均衡的ID来查询      

## 根据负载均衡ID查询负载均衡
- loadService:main:query

| 描述 | 命令 | 
| :---: | :--- | 
|通过负载均衡ID查询负载均衡| rex  [module] --loadBalancerId=''| 


**示例1**  
查询负载均衡ID为testabc的负载均衡
```
rex loadService:main:query --loadBalancerId='testabc' 
```


**参数用法**  
loadBalancerId:负载均衡ID     （必选）                  
w: 等于1,JSON格式输出结果


**备注**  


**截图**  
示例1
![](../assets/loadService_main_query1.gif)


