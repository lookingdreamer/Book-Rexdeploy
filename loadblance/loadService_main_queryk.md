# 基于名字的负载均衡查询(单进程)

和基于名字的负载均衡查询(多进程)             
唯一的区别: queryk是多进程运行,而loadService:main:queryk是单进程运行      
目前由于腾讯云的API不支持多线程调用,所以多进程同时并发有可能某些会失败      

## 基于名字的负载均衡查询(单进程)
- loadService:main:queryk

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字的负载均衡查询(单进程)| rex  [module] --k='[k]'| 


**示例1**  
查询server(名字分组)应用的负载均衡
```
rex loadService:main:queryk --k='server' 
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）                  
w: 等于1,JSON格式输出结果


**备注**  
关于负载均衡的配置参考[https://book.osichina.net/an-zhuang/shengji.html](https://book.osichina.net/an-zhuang/shengji.html)        
主要是配置loadBalancerId(负载均衡id1)


**截图**  
示例1
![](../assets/loadService_main_queryk1.gif)


