# 基于名字的批量负载均衡修改(单进程)

和基于名字的负载均衡修改(多进程)             
唯一的区别: updatek是多进程运行,而loadService:main:update是单进程运行      
目前由于腾讯云的API不支持多线程调用,所以多进程同时并发有可能某些会失败      

## 基于名字的批量负载均衡修改(单进程)
- loadService:main:update

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字的负载均衡修改(单进程)| rex  [module] --k='[k]'  --weight=''| 


**示例1**  
修改server(名字分组)应用的负载均衡的权重为10
```
rex loadService:main:update --k='server'  --w='10'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）      
weight: 要修改的权重值0-100                      
w: 等于1,JSON格式输出结果


**备注**  
关于负载均衡的配置参考[https://book.osichina.net/an-zhuang/shengji.html](https://book.osichina.net/an-zhuang/shengji.html)        
主要是配置loadBalancerId(负载均衡id1)


**截图**  
示例1
![](../assets/loadService_main_update1.gif)


