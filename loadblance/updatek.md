# 基于名字的批量负载均衡修改(多进程)


## 基于名字的批量负载均衡修改(多进程)
- updatek

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字的负载均衡修改(多进程)| rex  [module] --k='[k]' --weight=''| 


**示例1**  
修改server(名字分组)应用的负载均衡的权重为0
```
rex updatek --k='server' --weight='0'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
weight: 要修改的权重值0-100                      
w: 等于1,JSON格式输出结果         


**备注**  
关于负载均衡的配置参考[https://book.osichina.net/an-zhuang/shengji.html](https://book.osichina.net/an-zhuang/shengji.html)        
主要是配置loadBalancerId(负载均衡id1)


**截图**  
示例1
![](../assets/updatek1.gif)



