# 登陆获取Token

**简要描述：** 

- 通过API接口登陆获取API所需要的Token

**请求URL：** 
- ` http://127.0.0.1:3000/login `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|username |  是  |    string   |    用户名   |
|password |  是  |    string   |    密码   |

**详细说明：** 
账号/密码/有效期 可以通过`web/fastnotes.json`配置      

**请求示例** 

`username=admin&password=admin20180529#@!`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败//0 成功
     "expire" : "3600",   //token有效期 单位s
     "msg" : "获取成功",  //消息
     "params" : {   //传参
          "password" : "admin20180529#@!",  //用户名
          "username" : "admin"  //密码
     }, 
     "saveToken" : {   //token保存信息
          "code" : "0",  //0 成功 非0 失败 //0 保存成功
          "msg" : "保存token成功"  //保存消息提示
     }, 
     "take" : "0",  //花费时间 单位s //执行命令花费时间
     "time" : {  //时间信息
          "datetime" : "2018-05-29 16:10:26",  //当前时间
          "timestamp" : "1527610226"  //当前时间戳
     }, 
     "token" : "1527610226ecqpaxwqsyhbptqteuocqsqvmwgaccpp"  //返回token //token
 }

```

