# 验证token

**简要描述：** 

- 验证token是否可用

**请求URL：** 
- ` http://127.0.0.1:3000/token `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|token |  是  |    string   |    token字符串   |

**详细说明：** 
   
**请求示例** 

`token=1527610317gffvfenjcndxnfwjktdiudcpqoafdjom`

**返回示例**

``` 
{ 
     "expire" : "3600",  //token有效期 单位s
     "getToken" : {   //获取token消息
          "code" : "0",  //0 成功 非0 失败 // 0成功
          "msg" : "token校验正常", //获取校验消息 
          "query" : {  //从数据库查询查询消息
               "expire" : "3600", //从数据库查询 token有效期 单位s
               "timestamp" : "1527610317", //从数据库查询 token 时间戳 单位s
               "token" : "1527610317gffvfenjcndxnfwjktdiudcpqoafdjom" //从数据库查询 token //token
          } 
     }, 
     "timestamp" : "1527610324", //当前时间戳 
     "token" : "1527610317gffvfenjcndxnfwjktdiudcpqoafdjom"  //token  //token
 }

```

