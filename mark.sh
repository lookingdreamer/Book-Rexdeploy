#!/bin/bash
# 批量备注

mark(){
	echo ""
	echo "当前操作的文件: $fileName"
	start=`cat $fileName |grep -n '"param" :' |awk -F':' '{print $1}'`
	end=`cat $fileName |grep -n ' "parseparameters" :' |awk -F':' '{print $1}'`
	resturn=`cat $fileName |grep -n '**返回示例**' |awk -F':' '{print $1}'`

	echo "start:$start end:$end"
	if [[ $start == "" ]]; then
		echo "当前操作的文件: $fileName 获取起始number为空"
		exit
	fi
	if [[ $end == "" ]]; then
		echo "当前操作的文件: $fileName 获取结束number为空"
		exit
	fi
	start_number=`echo "$start" |wc -l`
	if [[ $start_number -eq 1 ]]; then

		last=`expr $end - 1 `
		echo "sed -n ${start},${last}p $fileName"
		sedContent=`sed -n "${start},${last}p" $fileName`
		echo "${sedContent}"
		resparam=`echo "${sedContent}" |grep -v '"token" :' |grep -v '"param" :' |awk -F',' '{print $1}' |grep -v '}' |sed 's/"//g' |sed 's/ : /=/g'|sed 's/ //g'  |tr "\n" "&" |sed "s/&$//" |sed 's/127.0.0.110.0.2.15/127.0.0.1 10.0.2.15/g' |sed "s/server1server2/server1,server2/g"`
		echo "resparam ------------> $resparam"
		lastres=`expr $resturn - 1`
		# echo "sed -i \"\\n${lastres} i**请求示例**     \\n\\n\`${resparam}\`\\n\" $fileName "
		sed -i "${lastres} i\n**请求示例**     \n\n\`${resparam}\`\n" $fileName 

	elif [[ $start_number -eq 2 ]]; then

		start=`echo "$start" |head -n 1`
		end=`echo "$end" |head -n 1`
		last=`expr $end - 1 `
		echo "sed -n ${start},${last}p $fileName"
		sedContent=`sed -n "${start},${last}p" $fileName`
		echo "${sedContent}"
		resparam=`echo "${sedContent}" |grep -v '"token" :' |grep -v '"param" :' |awk -F',' '{print $1}' |grep -v '}' |sed 's/"//g' |sed 's/ : /=/g'|sed 's/ //g'  |tr "\n" "&" |sed "s/&$//" |sed 's/127.0.0.110.0.2.15/127.0.0.1 10.0.2.15/g' |sed "s/server1server2/server1,server2/g"`
		echo "resparam ------------> $resparam"
		lastres=`echo "$resturn" |head -n 1`
		lastres=`expr $lastres - 1`
		sed -i "${lastres} i\n**请求示例**     \n\n\`${resparam}\`\n" $fileName

		resturn=`cat $fileName |grep -n '**返回示例**' |awk -F':' '{print $1}'`
		start=`cat $fileName |grep -n '"param" :' |awk -F':' '{print $1}'`
		end=`cat $fileName |grep -n ' "parseparameters" :' |awk -F':' '{print $1}'`
		echo ""
		echo "==============================="
		start=`echo "$start" |tail -n 1`
		end=`echo "$end" |tail -n 1`
		last=`expr $end - 1 `
		echo "sed -n ${start},${last}p $fileName"
		sedContent=`sed -n "${start},${last}p" $fileName`
		echo "${sedContent}"
		resparam=`echo "${sedContent}" |grep -v '"token" :' |grep -v '"param" :' |awk -F',' '{print $1}' |grep -v '}' |sed 's/"//g' |sed 's/ : /=/g'|sed 's/ //g'  |tr "\n" "&" |sed "s/&$//" |sed 's/127.0.0.110.0.2.15/127.0.0.1 10.0.2.15/g' |sed "s/server1server2/server1,server2/g"`
		echo "resparam ------------> $resparam"
		lastres=`echo "$resturn" |tail -n 1`
		lastres=`expr $lastres - 1`
		sed -i "${lastres} i\n**请求示例**     \n\n\`${resparam}\`\n" $fileName

 	else
 		echo "过滤数量异常:$start_number"
 		exit
	fi
	# echo "**请求示例**     " >>  $fileName
	# echo "" >>  $fileName
	# echo "\`${resparam}\`" >>  $fileName
	# echo "" >>  $fileName
}

# for i in `find /Users/huanggaoming/myProject/RexDeploy-api-book -name "*md" -type f `
for i in `find /data/RexdeployV3/RexDeploy-api-book/moni/api -name "**md" -type f`
do

fileName=$i
if [[  -f $fileName ]];then
	mark
else
	echo "$fileName 不存在"
	exit
fi 

done

