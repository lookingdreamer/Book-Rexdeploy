# 模拟POST请求

**简要描述：** 

- 模拟POST请求

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527661143jounhnkfhpsfynvkphcryehonhhlnjhm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    Common:Use:post   |
|url |  是  |    string   |    请求url   |
|header |  是  |    string   |    请求header   |
|param |  是  |    string   |    JSON格式   |


**详细说明：** 
header格式: Content-Type:application/json;Accept:application/json  
param: 参数如果为表单提交,直接拼接在url中即可.

**请求示例**     

`action=Common:Use:post&header=&url=http://api.okayapi.com/?s=Ext.Pinyin.Name&text=拼音`

n**请求示例**     

`url=http://api.okayapi.com/?s=Ext.Pinyin.Name&text=拼音`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败  //0成功 非0失败
          "data" : {  //返回数据
               "0" : "200", //请求返回码 //返回第1个数据
               "1" : "{"ret":200,"data":{"err_code":0,"err_msg":"","pinyin":"pin yin"},"msg":"","info":{"s":"Ext.Pinyin.Name"}}", //返回数据 //返回第2个数据
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "Common:Use:post",  //请求模块
          "header" : "", 
          "param" : "", 
          "token" : "1527661143jounhnkfhpsfynvkphcryehonhhlnjhm",  //token
          "url" : "http://api.okayapi.com/?s=Ext.Pinyin.Name&text=拼音" 
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Common:Use:post,--header="",--param="",--token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm",--url="http://api.okayapi.com/?s=Ext.Pinyin.Name&text=拼音",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Common:Use:post --header="" --param="" --token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm" --url="http://api.okayapi.com/?s=Ext.Pinyin.Name&text=拼音" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "2"  //执行命令花费时间
 }

```

