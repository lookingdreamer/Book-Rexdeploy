# 模拟get请求


## 模拟get请求
- Common:Use:get

| 描述 | 命令 | 
| :---: | :--- | 
|模拟get请求| rex  [module] --url='[url]' --header='[header参数]'  [--print='1']| 


**示例1**  
查询IP:113.119.57.178的地址信息
```
rex Common:Use:get --url='http://ip.taobao.com/service/getIpInfo.php?ip=113.119.57.17' --header='Accept:application/json' --print='1'
```



**参数用法**  
url: url    （必选）                    
header: header参数,header有多个时用分号;间隔。    （必选）                    
print: 1打印输出结果 0不打印 不传不打印                         
w: 等于1,JSON格式输出结果


**备注**  




**截图**  
示例1
![](../assets/get1.gif)


