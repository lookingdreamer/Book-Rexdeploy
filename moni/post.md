# 模拟post请求


## 模拟get请求
- Common:Use:post

| 描述 | 命令 | 
| :---: | :--- | 
|模拟get请求| rex Common:Use:post --url='[url]' --header='[header参数]' --param='[参数]' [--print='1']| 


**示例1**  
查询拼音转换
```
rex Common:Use:post --url='http://api.okayapi.com/?s=Ext.Pinyin.Name&text=拼音' --header='Accept:application/json' --param='{}' --print='1'   
```



**参数用法**  
url: url    （必选）                    
header: header参数,header有多个时用分号;间隔。    （必选）                    
param: json格式    （必选）                    
print: 1打印输出结果 0不打印 不传不打印                              
w: 等于1,JSON格式输出结果


**备注**  
示例1
![](../assets/post1.gif)



**截图**  



