# 目录结构

```
# tree -L 2
├── config           (配置文件目录)
│   ├── config.yml   (主配置文件)
│   └── ip_lists.ini (IP分组列表配置)
├── softdir          (发布前的工程目录)
├── configuredir     (发布前的配置目录)
├── remotecomdir     (从远程服务器下载后的目录)
├── updatedir        (发布的更新目录)
|-- docker            (docker安装目录)
|   ├── Dockerfile      (docker入口文件)
|   ├── client           (模拟客户端docker入口目录)
|   ├── client.sh        (模拟客户端初始化脚本)
|   ├── mysql            (数据库启动脚本)
|   ├── start.sh         (docker启动脚本)
|   ├── tomcat-flow1     (模拟客户端1启动脚本) 
|   ├── tomcat-flow2     (模拟客户端2启动脚本) 
|   ├── tomcat-server1   (模拟客户端3启动脚本) 
|   └── tomcat-server2   (模拟客户端4启动脚本) 
├── install          (安装目录)
│   ├── autotask.sql    (安装sql)
│   ├── initStall.sh    (安装和初始化脚本)
│   ├── dockerinit.sh   (docker版本安装和初始化脚本)
│   ├── packages        (本地安装源包)
│   └── rex.repo        (yum仓库for rex)
├── keys             (秘钥目录)
│   ├── autotask        (autotask私钥)
│   └── autotask.pub    (autotask公钥)
├── lib              (模块目录)
│   ├── Common          (公共模块)
│   ├── Deploy          (发布模块)
│   ├── Enter           (路由模块)
│   ├── User            (用户模块)
│   ├── logCenter       (日志模块)
│   ├── loadService     (负载模块)
│   └── Rex             (自有模块)
├── logs             (日志目录)
│   └── rex.log         (日志文件)
├── src              (Rex源码目录)
│   └── Rex             (源码模块)
├── scripts     (脚本目录)
├── temp        (集群发布临时目录)
├── README.md   (说明文件)
├── Rexfile     (rex主程序入口)
├── LICENSE     (LICENSE文件)
├── download    (下载目录)
├── upload    (上传目录)
└── web       (API目录)
    ├── MYMETA.json
    ├── MYMETA.yml
    ├── Makefile
    ├── Makefile.PL
    ├── README
    ├── README.md
    ├── app.psgi
    ├── cpanfile
    ├── cpanfile.snapshot
    ├── fastnotes.json (API配置文件)
    ├── lib   
    ├── public
    ├── qq.pl
    ├── run.pl
    ├── script  (API启动目录)
    ├── storage
    ├── t

26 directories, 21 files
```


