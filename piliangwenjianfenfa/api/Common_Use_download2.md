# 基于IP或分组批量文件下载

**简要描述：** 

- 基于IP或者多个IP或者分组名批量下载



**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527661143jounhnkfhpsfynvkphcryehonhhlnjhm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:download   |
|dir1 |  是  |    string   |    下载目录或者文件   |
|dir2 |  是  |    string   |    本地保存目录   |
|H |  是  |    string   |    指定主机多个空格间隔   |
|G/g |  是  |    string   |    分组名,H或者G参数任何一个   |
|ipsep |  否  |    int   |    为1时,下载目录以IP为分割   |
|random |  是  |    string   |    随机数   |

**详细说明：**       
上述H参数可以替换成G参数,G参数指定分组,比如定义192.168.1.1到192.168.1.100为group分组,则使用时直接使用G分组的group名即可;H参数和G参数只能选择其中1个            
dir1是目录且以/结尾时,会直接下载下层目录到目标目录,比如dir1=/test/,dir2=/tmp,则会把对应test目录下的文件下载到tmp目录中,不包含test目录自己.<br>dir1是目录且不以/结尾时, 则会把test目录直接下载到tmp目录
<br>ipsep不存在时,比如下载远程主机(192.168.1.100)的test.txt文件到/tmp目录.<br>当ipsep=1时,就会把test.txt下载到/tmp/192.168.1.100/目录下.<br> 

**请求示例**     

`G=group&action=Common:Use:download&dir1=/data/www/html1&dir2=/tmp/download&ipsep=1&random=randomdownload`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
     }, 
     "param" : { //请求参数
          "G" : "group",  //执行主机分组,通过rex -T查询,通过config/iplists.ini定义
          "action" : "Common:Use:download",  //请求模块
          "dir1" : "/data/www/html1",  //download时:远程文件或者目录,upload时:本地文件或者目录
          "dir2" : "/tmp/download",  //download时:本地目录,upload时:远程目录
          "ipsep" : "1",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
          "random" : "randomdownload",  //随机数
          "token" : "1527661143jounhnkfhpsfynvkphcryehonhhlnjhm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "-G "group"",  //请求参数前缀
          "requestCmd" : "Common:Use:download,--dir1="/data/www/html1",--dir2="/tmp/download",--ipsep="1",--random="randomdownload",--token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF -G "group" Common:Use:download --dir1="/data/www/html1" --dir2="/tmp/download" --ipsep="1" --random="randomdownload" --token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "8"  //执行命令花费时间
 }

```

 **备注**       
 通过Common:Use:getJson获取返回结果,见下小节
