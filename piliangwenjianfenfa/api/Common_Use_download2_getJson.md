# 获取基于IP或分组批量文件下载结果

**简要描述：** 

- 获取基于IP或分组批量文件下载结果


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527661143jounhnkfhpsfynvkphcryehonhhlnjhm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:getJson   |
|random |  是  |    string   |    随机数,批量执行的随机数   |
|delete |  是  |    int   |    获取结果之后是否删除内存中保存的数据,1时删除   |

**请求示例**     

`action=Common:Use:getJson&delete=1&random=randomdownload`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "1", 
          "count" : "2",  //返回总数量
          "data" : {  //返回数据
               "0" : {   //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {    //返回数据 
                         "0" : {  //返回第1个数据
                              "code" : "0",  //0 成功 非0 失败 
                              "download" : {    //下载结果校验信息
                                   "code" : "0",  //0 成功 非0 失败
                                   "download_method" : "sftp", //下载方式,通过config/config.yml可定义
                                   "localPath" : "/tmp/download/10.0.2.15/html1"  //下载成功后文件或目录路径
                              }, 
                              "msg" : "download success",  //下载成功消息提示
                              "params" : { 
                                   "app_key" : {  //返回服务器唯一名字
                                   }, 
                                   "dir1" : "/data/www/html1",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                   "dir2" : "/tmp/download",  //download时:本地目录,upload时:远程目录
                                   "http" : {  //是否http下载
                                   }, 
                                   "ipsep" : "1",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};
                                   "w" : "1" 
                              }, 
                              "server" : "10.0.2.15", //下载的目标主机 //操作服务器地址
                              "size" : "0",  //文件或目录大小
                              "take" : "4"   //下载花费时间,单位秒 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
               "1" : {     //第2个节点数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "code" : "0",  //0 成功 非0 失败
                              "download" : {  //下载结果校验信息
                                   "code" : "0",  //0 成功 非0 失败
                                   "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                                   "localPath" : "/tmp/download/127.0.0.1/html1"  //下载成功后文件或目录路径
                              }, 
                              "msg" : "download success",  //下载成功消息提示
                              "params" : { 
                                   "app_key" : {  //返回服务器唯一名字
                                   }, 
                                   "dir1" : "/data/www/html1",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                   "dir2" : "/tmp/download",  //download时:本地目录,upload时:远程目录
                                   "http" : {  //是否http下载
                                   }, 
                                   "ipsep" : "1",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                                   "w" : "1" 
                              }, 
                              "server" : "127.0.0.1",  //操作服务器地址
                              "size" : "0",  //文件或目录大小
                              "take" : "4"  //执行命令花费时间
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { //请求参数
          "action" : "Common:Use:getJson",  //请求模块
          "delete" : "1",  //Common:Use:getJson获取结果之后是否删除内存中保存的数据,为1时删除
          "random" : "randomdownload",  //随机数
          "token" : "1527661143jounhnkfhpsfynvkphcryehonhhlnjhm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "Common:Use:getJson,--delete="1",--random="randomdownload",--token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  Common:Use:getJson --delete="1" --random="randomdownload" --token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "2"  //执行命令花费时间
 }

```
