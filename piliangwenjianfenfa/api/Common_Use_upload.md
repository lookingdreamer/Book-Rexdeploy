# 基于单个IP文件上传

**简要描述：** 

- 基于单个IP文件上传


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527661143jounhnkfhpsfynvkphcryehonhhlnjhm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:upload    |
|dir1 |  是  |    string   |    本地文件或者目录   |
|dir2 |  是  |    string   |    远程目录   |
|ipsep |  是  |    int   |    当该参数不传时，会上传到/tmp目录中;<br>等于1时上传到{dir2}/{ip地址};   |
|H |  是  |    string   |    指定IP主机   |

**详细说明：**       
dir1是目录且以/结尾时,会直接上传下层目录到目标目录,比如dir1=/test/,dir2=/tmp,则会把对应test目录下的文件上传到tmp目录中,不包含test目录自己.<br>dir1是目录且不以/结尾时, 则会把test目录直接上传到tmp目录
<br>ipsep不存在时,比如上传远程主机(192.168.1.100)的test.txt文件到/tmp目录.<br>当ipsep=1时,就会把test.txt上传到/tmp/192.168.1.100/目录下.<br>            

**请求示例**     

`H=127.0.0.1&action=Common:Use:upload&dir1=/data/www/html1&dir2=/tmp&ipsep=1`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败  //0 上传成功
                    "msg" : "upload success",  //上传成功消息提示
                    "params" : { 
                         "app_key" : {  //返回服务器唯一名字
                         }, 
                         "dir1" : "/data/www/html1",  //download时:远程文件或者目录,upload时:本地文件或者目录
                         "dir2" : "/tmp",  //download时:本地目录,upload时:远程目录
                         "ipsep" : "1",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                         "w" : "1" 
                    }, 
                    "server" : "127.0.0.1",  //操作主机 //操作服务器地址
                    "take" : "2",  //花费时间 //执行命令花费时间
                    "upload" : {  //上传后文件校验信息
                         "code" : "0",  //0 成功 非0 失败   //校验上传文件 0校验成功
                         "size" : "0",   //文件大小 //文件或目录大小
                         "upload_method" : "sftp"  //上传方式 //上传方式,通过config/config.yml可定义
                    } 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "H" : "127.0.0.1",  //执行主机,多个以空格间隔
          "action" : "Common:Use:upload",  //请求模块
          "dir1" : "/data/www/html1",  //download时:远程文件或者目录,upload时:本地文件或者目录
          "dir2" : "/tmp",  //download时:本地目录,upload时:远程目录
          "ipsep" : "1",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
          "token" : "1527661143jounhnkfhpsfynvkphcryehonhhlnjhm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "-H "127.0.0.1"",  //请求参数前缀
          "requestCmd" : "Common:Use:upload,--dir1="/data/www/html1",--dir2="/tmp",--ipsep="1",--token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF -H "127.0.0.1" Common:Use:upload --dir1="/data/www/html1" --dir2="/tmp" --ipsep="1" --token="1527661143jounhnkfhpsfynvkphcryehonhhlnjhm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "6"  //执行命令花费时间
 }

```