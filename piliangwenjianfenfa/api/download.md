# 基于名字批量文件下载

**简要描述：** 

- 基于名字批量文件下载

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527669337mxchcptwhwevbwgyiahtilfadcycyfxm `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    download   |
|dir1 |  是  |    string   |    远程文件或者目录   |
|dir2 |  是  |    string   |    本地目录   |
|k |  是  |    string   |    名字或者名字分组<br> 参见数据库字段的app_key或local_name,<br>如果多个app_key,用空格隔开   |
|ipsep |  否  |    int   |    当该参数不传时，会下载到/tmp目录中;<br>等于1时下载到{dir2}/{ip地址};<br> 等于2时下载到{dir2}/{名字}   |

**详细说明：**       
dir1是目录且以/结尾时,会直接下载下层目录到目标目录,比如dir1=/test/,dir2=/tmp,则会把对应test目录下的文件下载到tmp目录中,不包含test目录自己.<br>dir1是目录且不以/结尾时, 则会把test目录直接下载到tmp目录
<br>ipsep不存在时,比如下载远程主机(192.168.1.100)的test.txt文件到/tmp目录.<br>当ipsep=1时,就会把test.txt下载到/tmp/192.168.1.100/目录下.<br> 当ipsep=2时,就会把test.txt下载到/tmp/{名字名称}/目录下

**请求示例**     

`action=download&dir1=/data/www/html1/ws.html&dir2=/tmp&ipsep=2&k=server`

**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "app_key" : "server1",  //返回服务器唯一名字
                                   "code" : "0",  //0 成功 非0 失败
                                   "download" : {  //下载结果校验信息
                                        "code" : "0",  //0 成功 非0 失败
                                        "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                                        "localPath" : "/tmp/server1/ws.html"  //下载成功后文件或目录路径
                                   }, 
                                   "msg" : "download success",  //下载成功消息提示
                                   "params" : { 
                                        "app_key" : "server1",  //返回服务器唯一名字
                                        "dir1" : "/data/www/html1/ws.html",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                        "dir2" : "/tmp",  //download时:本地目录,upload时:远程目录
                                        "http" : {  //是否http下载
                                        }, 
                                        "ipsep" : "2",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                                        "w" : { 
                                        } 
                                   }, 
                                   "server" : "192.168.0.76",  //操作服务器地址
                                   "size" : "4.0K",  //文件或目录大小
                                   "take" : "0"  //执行命令花费时间
                              }, 
                              "mainProcess" : "30454"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "app_key" : "server2",  //返回服务器唯一名字
                                   "code" : "0",  //0 成功 非0 失败
                                   "download" : {  //下载结果校验信息
                                        "code" : "0",  //0 成功 非0 失败
                                        "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                                        "localPath" : "/tmp/server2/ws.html"  //下载成功后文件或目录路径
                                   }, 
                                   "msg" : "download success",  //下载成功消息提示
                                   "params" : { 
                                        "app_key" : "server2",  //返回服务器唯一名字
                                        "dir1" : "/data/www/html1/ws.html",  //download时:远程文件或者目录,upload时:本地文件或者目录
                                        "dir2" : "/tmp",  //download时:本地目录,upload时:远程目录
                                        "http" : {  //是否http下载
                                        }, 
                                        "ipsep" : "2",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                                        "w" : { 
                                        } 
                                   }, 
                                   "server" : "192.168.0.248",  //操作服务器地址
                                   "size" : "4.0K",  //文件或目录大小
                                   "take" : "0"  //执行命令花费时间
                              }, 
                              "mainProcess" : "30454"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "30454",  //当前命令的进程ID
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "download",  //请求模块
          "dir1" : "/data/www/html1/ws.html",  //download时:远程文件或者目录,upload时:本地文件或者目录
          "dir2" : "/tmp",  //download时:本地目录,upload时:远程目录
          "ipsep" : "2",  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
          "k" : "server",  //名字
          "token" : "1527669337mxchcptwhwevbwgyiahtilfadcycyfxm"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "download,--dir1="/data/www/html1/ws.html",--dir2="/tmp",--ipsep="2",--k="server",--token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  download --dir1="/data/www/html1/ws.html" --dir2="/tmp" --ipsep="2" --k="server" --token="1527669337mxchcptwhwevbwgyiahtilfadcycyfxm" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "4"  //执行命令花费时间
 }

```