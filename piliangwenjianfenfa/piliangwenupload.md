# 基于IP或分组批量文件上传

批量文件上传的原理是通过rsync或者sftp(可配置)远程取并发执行

## 基于IP或分组批量文件上传
- Common:Use:upload

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名批量上传| rex -G '[group]' [module] --dir1='[dir1]' --dir2='[dir2>]' --ipsep=1| 
|通过多个IP批量上传| rex -H '[IP1 IP2...]' [module] --dir1='[dir1]' --dir2='[dir2]' --ipsep=1| 

**示例**  
将分组名group的机器的/data/www/html1/ws.html文件上传本机/tmp/{ip}目录中
```
rex -G group Common:Use:upload --dir1='/data/www/html1/ws.html' --dir2='/tmp' --ipsep=1
```


**参数用法**  
分组名可以通过`rex -T`命令查询,同时在ip_lists.ini文件中设置。   
dir1: 远程文件或者目录  （必选）  
dir2: 本地目录  （必选）    
ipsep: 当该参数不传时，如上示例，会上传到/tmp目录中;等于1时上传到{dir2}/{ip地址}


**备注**  
dir1以/结尾时,会直接上传下层目录到目标目录,比如dir1=/test/,dir2=/tmp,则会把对应test目录下的文件上传到tmp目录中,不包含test目录自己.当不以/结尾时,
则会把test目录直接上传到tmp目录  
ipsep不存在时,比如上传远程主机(192.168.1.100)的test.txt文件到/tmp目录,当ipsep=1时,就会把test.txt上传到/tmp/192.168.1.100/目录下

**截图**  
示例
![](../assets/piliangwenupload.gif)


