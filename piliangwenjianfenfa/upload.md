# 基于名字服务批量文件上传

批量文件上传的原理是通过rsync或者sftp(可配置)远程取并发执行

## 基于名字服务批量文件上传
- upload

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字批量上传| rex  [module] --k='[k]' --dir1='[dir1]' --dir2='[dir2>]' --ipsep=1| 


**示例1**  
将名字分组为server的机器的/data/www/html1/ws.html文件上传本机/tmp/{ip}目录中
```
rex upload --k='server' --dir1='/data/www/html1/ws.html' --dir2='/tmp' --ipsep=1
```

**示例2**  
将名字分组为server的机器的/data/www/html1/ws.html文件上传本机/tmp/{名字}目录中
```
rex upload --k='server' --dir1='/data/www/html1/ws.html' --dir2='/tmp' --ipsep=2
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开   （必选）        
dir1: 远程文件或者目录  （必选）  
dir2: 本地目录  （必选）   
ipsep: 当该参数不传时，如上示例，会上传到/tmp目录中;等于1时上传到{dir2}/{ip地址};等于2时上传到{dir2}/{名字}         
w: 等于1,JSON格式输出结果


**备注**  
dir1以/结尾时,会直接上传下层目录到目标目录,比如dir1=/test/,dir2=/tmp,则会把对应test目录下的文件上传到tmp目录中,不包含test目录自己.当不以/结尾时,
则会把test目录直接上传到tmp目录  
ipsep不存在时,比如上传远程主机(192.168.1.100)的test.txt文件到/tmp目录,当ipsep=1时,就会把test.txt上传到/tmp/192.168.1.100/目录下,当ipsep=2时,就会把test.txt上传到/tmp/{名字名称}/目录下


**截图** 
示例1 && 示例2  
![](../assets/upload1.gif)


