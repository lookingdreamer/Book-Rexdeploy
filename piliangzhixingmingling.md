# 批量命令执行

批量命令命令主要分为2类，1类是基于名字服务的批量命令执行，1类是基于分组和IP列表的批量命令执行

## 基于名字服务的批量命令执行

首先你需要接入应用，如何接入请参考[接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)

<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

- run

| 命令 | 参数 | 
| :---: | :--- | 
| rex [module] --cmd='[cmd]' --k='[app_key1 app_key2 ...]'| cmd:执行命令 app_key:唯一名字|
| rex [module] --cmd='[cmd]' --k='[local_name ...]'| cmd:执行命令 local_name:名字分组|

| 示例 | 解释 | 
| :---: | :--- | 
| rex run --cmd='uptime' --k='server'|根据名字分组查询server1/server2的启动时间|
| rex run --cmd='uptime' --k='server1 server2'|查询server1/server2的启动时间([演示](../assets/cmd.png))|

![演示](../assets/cmd.png)

该模块默认支持并发控制，可通过主配置文件(parallelism)配置并发个数。  
命令输出格式默认为: [IP地址]-[根据IP查询到服务器名/如果没有则不显示] 命令内容

## 基于分组和IP列表的批量命令执行

这种执行方式，不要接入数据库配置即可使用  

- Common:Use:run

| 命令 | 参数 | 
| :---: | :--- | 
| rex -G '[group]' [module] --cmd='[cmd]' | cmd:执行命令 group:分组名|
| rex -H '[IP1 IP2...]' [module] --cmd='[cmd]' | cmd:执行命令 IP以空格间隔|

| 示例 | 解释 | 
| :---: | :--- | 
| rex -G group  Common:Use:run --cmd='uptime' |查询分组名为group的启动时间([演示](../assets/cmd1.png))|

![演示](../assets/cmd1.png))

- 并发参数

该Common:Use:run也可以通过设置并发参数执行，-t参数设置并发执行个数   
比如并发10个线程执行查询服务器内存情况 

```
rex -G group -t 10  Common:Use:run --cmd='free -m'
```

命令输出格式默认为: [IP地址]-[根据IP查询到服务器名/如果没有则不显示] 命令内容

- group分组

如何定义分组可以参考`config/ip_lists.ini`文件

