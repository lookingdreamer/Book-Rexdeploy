# 基于名字的日志批量过滤

**简要描述：**     

- 基于名字的日志批量过滤     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    grepLog   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |
|grep |  是  |    string   |    过滤关键词   |

**详细说明：**  
默认过滤当天的日志      

**请求示例**     

`action=grepLog&grep=Serverstartup&k=server`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "app_key" : "server1",  //返回服务器唯一名字
                                   "log_grep_line" : "2",  //过滤后的行数
                                   "logsize" : "44K ", //日志大小
                                   "max_grep_row" : "500",  //最大显示行数
                                   "output_grep" : "01-Jun-2018 04:40:52.417 INFO [main] org.apache.catalina.startup.Catalina.start [01;31m[KServer startup[m[K in 7339 ms
01-Jun-2018 04:41:01.306 INFO [main] org.apache.catalina.startup.Catalina.start [01;31m[KServer startup[m[K in 16232 ms", //过滤后的内容
                                   "params" : { 
                                        "debug" : { 
                                        }, 
                                        "grep" : "Server startup", 
                                        "k" : "server1",  //名字
                                        "log" : { 
                                        }, 
                                        "search" : { 
                                        }, 
                                        "w" : { 
                                        } 
                                   }, 
                                   "server_info" : {  //服务器信息
                                        "external_ip" : "127.0.0.1",  //第2IP
                                        "log" : "/data/log/server1/catalina.2018-06-01", //日志路径
                                        "names" : "后台服务集群1", //服务器名称
                                        "network_ip" : "192.168.0.76" //第21IP
                                   } 
                              }, 
                              "mainProcess" : "7713"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "app_key" : "server2",  //返回服务器唯一名字
                                   "log_grep_line" : "1", 
                                   "logsize" : "8.0K    ", 
                                   "max_grep_row" : "500", 
                                   "output_grep" : "01-Jun-2018 04:40:52.417 INFO [main] org.apache.catalina.startup.Catalina.start [01;31m[KServer startup[m[K in 7339 ms", 
                                   "params" : { 
                                        "debug" : { 
                                        }, 
                                        "grep" : "Server startup", 
                                        "k" : "server2",  //名字
                                        "log" : { 
                                        }, 
                                        "search" : { 
                                        }, 
                                        "w" : { 
                                        } 
                                   }, 
                                   "server_info" : { 
                                        "external_ip" : "127.0.0.1", 
                                        "log" : "/data/log/server2/catalina.2018-06-01.log", 
                                        "names" : "后台服务集群2", 
                                        "network_ip" : "192.168.0.248" 
                                   } 
                              }, 
                              "mainProcess" : "7713"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "7713",  //当前命令的进程ID
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "grepLog",  //请求模块
          "grep" : "Server startup", 
          "k" : "server",  //名字
          "token" : "1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "grepLog,--grep="Server startup",--k="server",--token="1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  grepLog --grep="Server startup" --k="server" --token="1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "6"  //执行命令花费时间
 }

```

