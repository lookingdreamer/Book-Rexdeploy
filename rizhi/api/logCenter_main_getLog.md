# 根据搜索词下载日志

**简要描述：**     

- 根据搜索词下载日志     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    logCenter:main:getLog   |
|search |  是  |    string   |    支持关键词,服务器名称,内网IP,外网IP模糊搜索   |
|download_local |  是  |    int   |    日志本地下载或者远程rsync传输:<br> 等于1时下载日志到download/{ip地址}目录下;<br>等于0时上传日志到rsync服务端   |

**详细说明：**    
默认下载当天的日志      
download_local=1 下载到download/{ip地址}目录,其中download目录可以参考config.yml         
download_local=0 上传到rsync服务端,其中rsync服务端的配置可以参考config.yml


**请求示例**     

`action=logCenter:main:getLog&download_local=1&search=后台服务集群1`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "download" : {  //下载结果校验信息
                         "code" : "0",  //0 成功 非0 失败
                         "download" : {  //下载结果校验信息
                              "code" : "0",  //0 成功 非0 失败
                              "download_method" : "sftp",  //下载方式,通过config/config.yml可定义
                              "localPath" : "download/192.168.0.76//catalina.2018-06-01"  //下载成功后文件或目录路径
                         }, 
                         "msg" : "download success",  //下载成功消息提示
                         "params" : { 
                              "app_key" : {  //返回服务器唯一名字
                              }, 
                              "dir1" : "/data/log/server1/catalina.2018-06-01",  //download时:远程文件或者目录,upload时:本地文件或者目录
                              "dir2" : "download/192.168.0.76/",  //download时:本地目录,upload时:远程目录
                              "http" : {  //是否http下载
                              }, 
                              "ipsep" : {  //当该参数不传时，会下载到/tmp目录中;等于1时下载到{dir2}/{ip地址};等于2时下载到{dir2}/{名字}
                              }, 
                              "w" : { 
                              } 
                         }, 
                         "server" : "192.168.0.76",  //操作服务器地址
                         "size" : "44K",  //文件或目录大小
                         "take" : "0"  //执行命令花费时间
                    }, 
                    "log" : "/data/log/server1/catalina.2018-06-01", 
                    "params" : { 
                         "download_local" : "1", 
                         "k" : {  //名字
                         }, 
                         "log" : { 
                         }, 
                         "search" : "后台服务集群1" 
                    } 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "logCenter:main:getLog",  //请求模块
          "download_local" : "1", 
          "search" : "后台服务集群1", 
          "token" : "1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "logCenter:main:getLog,--download_local="1",--search="后台服务集群1",--token="1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  logCenter:main:getLog --download_local="1" --search="后台服务集群1" --token="1527841191sfkomtntcpfwomvlvdfavpvraqlnmlbf" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "3"  //执行命令花费时间
 }

```

