# 基于IP或分组下载指定日志

## 基于IP或分组下载指定日志       

**简要描述：**        

- 基于IP或分组下载指定日志   
   

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527923837smjjwstsbyjsqfrqwvkfbprpvfwfrlcq `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |     logCenter:main:getLog   |
|download_local |  是  |    int   |    日志本地下载或者远程rsync传输:<br> 等于1时下载日志到download/{ip地址}目录下;<br>等于0时上传日志到rsync服务端   |
|H |  是  |    string   |    指定主机多个空格间隔   |
|G/g |  是  |    string   |    分组名,H或者G参数任何一个   |
|log |  是  |    string   |    指定日志路径   |
|random |  是  |    string   |    随机数   |

**详细说明：**    


**请求示例**     

`G=group&action=logCenter:main:getLog&download_local=1&log=/data/log/server1/catalina.2018-07-03&random=getLog`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
     }, 
     "param" : { 
          "G" : "group", 
          "action" : "logCenter:main:getLog", 
          "download_local" : "1", 
          "log" : "/data/log/server1/catalina.2018-07-03", 
          "random" : "getLog", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "-G "group"", 
          "requestCmd" : "logCenter:main:getLog,--download_local="1",--log="/data/log/server1/catalina.2018-07-03",--random="getLog",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF -G "group" logCenter:main:getLog --download_local="1" --log="/data/log/server1/catalina.2018-07-03" --random="getLog" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "5" 
 }

```
   

## 获取基于IP或分组下载指定日志


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:getJson   |
|random |  是  |    string   |    随机数   |
|delete |  是  |    int   |    获取结果之后是否删除内存中保存的数据,1时删除   |

n
**请求示例**     

`action=Common:Use:getJson&delete=1&random=getLog`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "1", 
          "count" : "2", 
          "data" : { 
               "0" : { 
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "download" : { 
                                   "code" : "0", 
                                   "download" : { 
                                        "code" : "0", 
                                        "download_method" : "rsync", 
                                        "localPath" : "download/192.168.0.207//catalina.2018-07-03" 
                                   }, 
                                   "msg" : "download success", 
                                   "params" : { 
                                        "app_key" : { 
                                        }, 
                                        "dir1" : "/data/log/server1/catalina.2018-07-03", 
                                        "dir2" : "download/192.168.0.207/", 
                                        "http" : { 
                                        }, 
                                        "ipsep" : { 
                                        }, 
                                        "w" : { 
                                        } 
                                   }, 
                                   "server" : "192.168.0.207", 
                                   "size" : "24K", 
                                   "take" : "0" 
                              }, 
                              "log" : "/data/log/server1/catalina.2018-07-03", 
                              "params" : { 
                                   "download_local" : "1", 
                                   "k" : { 
                                   }, 
                                   "log" : "/data/log/server1/catalina.2018-07-03", 
                                   "search" : { 
                                   } 
                              }, 
                              "size" : "24K  /data/log/server1/catalina.2018-07-03" 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
               "1" : { 
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "download" : { 
                                   "code" : "0", 
                                   "download" : { 
                                        "code" : "0", 
                                        "download_method" : "rsync", 
                                        "localPath" : "download/192.168.0.76//catalina.2018-07-03" 
                                   }, 
                                   "msg" : "download success", 
                                   "params" : { 
                                        "app_key" : { 
                                        }, 
                                        "dir1" : "/data/log/server1/catalina.2018-07-03", 
                                        "dir2" : "download/192.168.0.76/", 
                                        "http" : { 
                                        }, 
                                        "ipsep" : { 
                                        }, 
                                        "w" : { 
                                        } 
                                   }, 
                                   "server" : "192.168.0.76", 
                                   "size" : "24K", 
                                   "take" : "0" 
                              }, 
                              "log" : "/data/log/server1/catalina.2018-07-03", 
                              "params" : { 
                                   "download_local" : "1", 
                                   "k" : { 
                                   }, 
                                   "log" : "/data/log/server1/catalina.2018-07-03", 
                                   "search" : { 
                                   } 
                              }, 
                              "size" : "24K  /data/log/server1/catalina.2018-07-03" 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { 
          "action" : "Common:Use:getJson", 
          "delete" : "1", 
          "random" : "getLog", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "Common:Use:getJson,--delete="1",--random="getLog",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  Common:Use:getJson --delete="1" --random="getLog" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "2" 
 }

```


