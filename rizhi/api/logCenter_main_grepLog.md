# 根据搜索词过滤日志

**简要描述：**     

- 根据搜索词过滤日志  


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1527923837smjjwstsbyjsqfrqwvkfbprpvfwfrlcq `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    logCenter:main:grepLog   |
|search |  是  |    string   |    支持关键词,服务器名称,内网IP,外网IP模糊搜索   |
|grep |  是  |    string   |    过滤关键词   |


**请求示例**     

`action=logCenter:main:grepLog&grep=Serverstartup&search=后台服务集群1`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "0", 
          "data" : { 
               "0" : { 
                    "log_grep_line" : "1", 
                    "logsize" : "8.0K   ", 
                    "max_grep_row" : "500", 
                    "output_grep" : "03-Jul-2018 13:41:34.329 INFO [main] org.apache.catalina.startup.Catalina.start [01;31m[KServer startup[m[K in 10699 ms", 
                    "params" : { 
                         "debug" : { 
                         }, 
                         "grep" : "Server startup", 
                         "k" : { 
                         }, 
                         "log" : { 
                         }, 
                         "search" : "后台服务集群1", 
                         "w" : "1" 
                    }, 
                    "server_info" : { 
                         "external_ip" : "127.0.0.1", 
                         "log" : "/data/log/server1/catalina.2018-07-03.log", 
                         "names" : "后台服务集群1", 
                         "network_ip" : "192.168.0.207" 
                    } 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { 
          "action" : "logCenter:main:grepLog", 
          "grep" : "Server startup", 
          "search" : "后台服务集群1", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "logCenter:main:grepLog,--grep="Server startup",--search="后台服务集群1",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  logCenter:main:grepLog --grep="Server startup" --search="后台服务集群1" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "5" 
 }

```