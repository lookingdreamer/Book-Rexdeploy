# 基于IP或分组过滤指定日志

## 基于IP或分组过滤指定日志       

**简要描述：**        

- 基于IP或分组过滤指定日志  


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    logCenter:main:grepLog   |
|H |  是  |    string   |    指定主机多个空格间隔   |
|G/g |  是  |    string   |    分组名,H或者G参数任何一个   |
|log |  是  |    string   |    指定日志路径   |
|random |  是  |    string   |    随机数   |
|grep |  是  |    string   |    过滤关键词   |


**请求示例**     

`G=group&action=logCenter:main:grepLog&download_local=1&grep=Serverstartup&log=/data/log/server1/catalina.2018-07-03&random=grepLog`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
     }, 
     "param" : { 
          "G" : "group", 
          "action" : "logCenter:main:grepLog", 
          "download_local" : "1", 
          "grep" : "Server startup", 
          "log" : "/data/log/server1/catalina.2018-07-03", 
          "random" : "grepLog", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "-G "group"", 
          "requestCmd" : "logCenter:main:grepLog,--grep="Server startup",--log="/data/log/server1/catalina.2018-07-03",--random="grepLog",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF -G "group" logCenter:main:grepLog  --grep="Server startup" --log="/data/log/server1/catalina.2018-07-03" --random="grepLog" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "4" 
 }

```

## 获取基于IP或分组过滤指定日志结果   


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:getJson   |
|random |  是  |    string   |    随机数   |
|delete |  是  |    int   |    获取结果之后是否删除内存中保存的数据,1时删除   |

n
**请求示例**     

`action=Common:Use:getJson&delete=1&random=grepLog`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "1", 
          "count" : "2", 
          "data" : { 
               "0" : { 
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "log_grep_line" : "1", 
                              "logsize" : "24K    ", 
                              "max_grep_row" : "500", 
                              "output_grep" : "03-Jul-2018 13:41:34.329 INFO [main] org.apache.catalina.startup.Catalina.start [01;31m[KServer startup[m[K in 10699 ms", 
                              "params" : { 
                                   "debug" : { 
                                   }, 
                                   "grep" : "Server startup", 
                                   "k" : { 
                                   }, 
                                   "log" : "/data/log/server1/catalina.2018-07-03", 
                                   "search" : { 
                                   }, 
                                   "w" : "1" 
                              }, 
                              "server_info" : { 
                                   "log" : "/data/log/server1/catalina.2018-07-03", 
                                   "names" : "后台服务集群2", 
                                   "server" : "192.168.0.76" 
                              } 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
               "1" : { 
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "log_grep_line" : "1", 
                              "logsize" : "24K    ", 
                              "max_grep_row" : "500", 
                              "output_grep" : "03-Jul-2018 13:41:34.329 INFO [main] org.apache.catalina.startup.Catalina.start [01;31m[KServer startup[m[K in 10699 ms", 
                              "params" : { 
                                   "debug" : { 
                                   }, 
                                   "grep" : "Server startup", 
                                   "k" : { 
                                   }, 
                                   "log" : "/data/log/server1/catalina.2018-07-03", 
                                   "search" : { 
                                   }, 
                                   "w" : "1" 
                              }, 
                              "server_info" : { 
                                   "log" : "/data/log/server1/catalina.2018-07-03", 
                                   "names" : "后台服务集群1", 
                                   "server" : "192.168.0.207" 
                              } 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { 
          "action" : "Common:Use:getJson", 
          "delete" : "1", 
          "random" : "grepLog", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "Common:Use:getJson,--delete="1",--random="grepLog",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  Common:Use:getJson --delete="1" --random="grepLog" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "2" 
 }

```

