# 根据搜索词查看日志

**简要描述：**     

- 根据搜索词查看日志   

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    logCenter:main:lookLog   |
|search |  是  |    string   |    支持关键词,服务器名称,内网IP,外网IP模糊搜索   |


**请求示例**     

`action=logCenter:main:lookLog&search=server1`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "fileres" : { 
                         "0" : "444K",  //返回第1个数据
                         "1" : "catalina.2018-06-18",  //返回第2个数据
                         "2" : "catalina.2018-06-18.log",  //返回第3个数据
                         "3" : "localhost.2018-06-18.log", 
                         "4" : "host-manager.2018-06-18.log", 
                         "5" : "manager.2018-06-18.log", 
                         "6" : "catalina.2018-06-05", 
                         "7" : "catalina.2018-06-05.log", 
                         "8" : "localhost.2018-06-05.log", 
                         "9" : "host-manager.2018-06-05.log", 
                         "10" : "manager.2018-06-05.log", 
                         "11" : "catalina.2018-06-04", 
                         "12" : "catalina.2018-06-04.log", 
                         "13" : "localhost.2018-06-04.log", 
                         "14" : "host-manager.2018-06-04.log", 
                         "15" : "manager.2018-06-04.log", 
                    }, 
                    "logdir" : "/data/log/server1", 
                    "names" : "后台服务集群1", 
                    "params" : { 
                         "k" : {  //名字
                         }, 
                         "logdir" : { 
                         }, 
                         "more" : { 
                         }, 
                         "search" : "server1", 
                         "w" : "1" 
                    }, 
                    "server" : "192.168.0.207"  //操作服务器地址
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "logCenter:main:lookLog",  //请求模块
          "search" : "server1", 
          "token" : "1529853651coqjifffyedvdqfnrghhphcadjgctexi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "logCenter:main:lookLog,--search="server1",--token="1529853651coqjifffyedvdqfnrghhphcadjgctexi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  logCenter:main:lookLog --search="server1" --token="1529853651coqjifffyedvdqfnrghhphcadjgctexi" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "3"  //执行命令花费时间
 }

```

