# 基于IP或分组查看日志

## 基于IP或分组查看日志       

**简要描述：**        

- 基于IP或分组查看日志  

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    logCenter:main:lookLog   |
|H |  是  |    string   |    指定主机多个空格间隔   |
|G/g |  是  |    string   |    分组名,H或者G参数任何一个   |
|logdir |  是  |    string   |    日志目录   |
|random |  是  |    string   |    随机数   |


**请求示例**     

`G=group&action=logCenter:main:lookLog&logdir=/data/log/server1/&random=lookLog`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
     }, 
     "param" : { 
          "G" : "group", 
          "action" : "logCenter:main:lookLog", 
          "logdir" : "/data/log/server1/", 
          "random" : "lookLog", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "-G "group"", 
          "requestCmd" : "logCenter:main:lookLog,--logdir="/data/log/server1/",--random="lookLog",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF -G "group" logCenter:main:lookLog --logdir="/data/log/server1/" --random="lookLog" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "4" 
 }

```

## 获取基于IP或分组查看日志结果  


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:getJson   |
|random |  是  |    string   |    随机数   |
|delete |  是  |    int   |    获取结果之后是否删除内存中保存的数据,1时删除   |

n
**请求示例**     

`action=Common:Use:getJson&delete=1&random=lookLog`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "1", 
          "count" : "2", 
          "data" : { 
               "0" : { 
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "code" : "1", 
                              "fileres" : { 
                                   "0" : "540K", 
                                   "1" : "catalina.2018-07-03", 
                                   "2" : "catalina.2018-07-03.log", 
                                   "3" : "localhost.2018-07-03.log", 
                                   "4" : "host-manager.2018-07-03.log", 
                                   "5" : "manager.2018-07-03.log", 
                                   "6" : "catalina.2018-06-24", 
                                   "7" : "catalina.2018-06-24.log", 
                                   "8" : "localhost.2018-06-24.log", 
                                   "9" : "host-manager.2018-06-24.log", 
                                   "10" : "manager.2018-06-24.log", 
                                   "11" : "catalina.2018-06-18", 
                                   "12" : "catalina.2018-06-18.log", 
                                   "13" : "localhost.2018-06-18.log", 
                                   "14" : "host-manager.2018-06-18.log", 
                                   "15" : "manager.2018-06-18.log", 
                                   "16" : "catalina.2018-06-05", 
                                   "17" : "catalina.2018-06-05.log", 
                                   "18" : "localhost.2018-06-05.log", 
                                   "19" : "host-manager.2018-06-05.log", 
                                   "20" : "manager.2018-06-05.log", 
                                   "21" : "catalina.2018-06-04", 
                                   "22" : "catalina.2018-06-04.log", 
                                   "23" : "localhost.2018-06-04.log", 
                                   "24" : "host-manager.2018-06-04.log", 
                                   "25" : "manager.2018-06-04.log", 
                              }, 
                              "logdir" : "/data/log/server1/", 
                              "msg" : "success", 
                              "myFiles" : "22K   2018-07-03  13:41:34  catalina.2018-07-03
4.2K  2018-07-03  13:41:34  catalina.2018-07-03.log
466   2018-07-03  13:41:33  localhost.2018-07-03.log
0     2018-07-03  13:41:20  host-manager.2018-07-03.log
0     2018-07-03  13:41:20  manager.2018-07-03.log
44K   2018-06-24  16:12:34  catalina.2018-06-24
8.3K  2018-06-24  16:12:34  catalina.2018-06-24.log
932   2018-06-24  16:12:33  localhost.2018-06-24.log
0     2018-06-24  16:10:37  host-manager.2018-06-24.log
0     2018-06-24  16:10:37  manager.2018-06-24.log
86K   2018-06-18  13:49:01  catalina.2018-06-18
17K   2018-06-18  13:49:01  catalina.2018-06-18.log
1.9K  2018-06-18  13:49:01  localhost.2018-06-18.log
0     2018-06-18  13:38:09  host-manager.2018-06-18.log
0     2018-06-18  13:38:09  manager.2018-06-18.log
87K   2018-06-05  14:49:15  catalina.2018-06-05
17K   2018-06-05  14:49:15  catalina.2018-06-05.log
1.9K  2018-06-05  14:49:14  localhost.2018-06-05.log
0     2018-06-05  14:29:30  host-manager.2018-06-05.log
0     2018-06-05  14:29:30  manager.2018-06-05.log
170K  2018-06-04  16:36:01  catalina.2018-06-04
34K   2018-06-04  16:36:01  catalina.2018-06-04.log
3.7K  2018-06-04  16:36:00  localhost.2018-06-04.log
0     2018-06-04  16:08:38  host-manager.2018-06-04.log
0     2018-06-04  16:08:38  manager.2018-06-04.log", 
                              "names" : "后台服务集群2", 
                              "params" : { 
                                   "k" : { 
                                   }, 
                                   "logdir" : "/data/log/server1/", 
                                   "more" : { 
                                   }, 
                                   "search" : { 
                                   }, 
                                   "w" : "1" 
                              }, 
                              "server" : "192.168.0.76" 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
               "1" : { 
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "code" : "1", 
                              "fileres" : { 
                                   "0" : "540K", 
                                   "1" : "catalina.2018-07-03", 
                                   "2" : "catalina.2018-07-03.log", 
                                   "3" : "localhost.2018-07-03.log", 
                                   "4" : "host-manager.2018-07-03.log", 
                                   "5" : "manager.2018-07-03.log", 
                                   "6" : "catalina.2018-06-24", 
                                   "7" : "catalina.2018-06-24.log", 
                                   "8" : "localhost.2018-06-24.log", 
                                   "9" : "host-manager.2018-06-24.log", 
                                   "10" : "manager.2018-06-24.log", 
                                   "11" : "catalina.2018-06-18", 
                                   "12" : "catalina.2018-06-18.log", 
                                   "13" : "localhost.2018-06-18.log", 
                                   "14" : "host-manager.2018-06-18.log", 
                                   "15" : "manager.2018-06-18.log", 
                                   "16" : "catalina.2018-06-05", 
                                   "17" : "catalina.2018-06-05.log", 
                                   "18" : "localhost.2018-06-05.log", 
                                   "19" : "host-manager.2018-06-05.log", 
                                   "20" : "manager.2018-06-05.log", 
                                   "21" : "catalina.2018-06-04", 
                                   "22" : "catalina.2018-06-04.log", 
                                   "23" : "localhost.2018-06-04.log", 
                                   "24" : "host-manager.2018-06-04.log", 
                                   "25" : "manager.2018-06-04.log", 
                              }, 
                              "logdir" : "/data/log/server1/", 
                              "msg" : "success", 
                              "myFiles" : "22K   2018-07-03  13:41:34  catalina.2018-07-03
4.2K  2018-07-03  13:41:34  catalina.2018-07-03.log
466   2018-07-03  13:41:33  localhost.2018-07-03.log
0     2018-07-03  13:41:20  host-manager.2018-07-03.log
0     2018-07-03  13:41:20  manager.2018-07-03.log
44K   2018-06-24  16:12:34  catalina.2018-06-24
8.3K  2018-06-24  16:12:34  catalina.2018-06-24.log
932   2018-06-24  16:12:33  localhost.2018-06-24.log
0     2018-06-24  16:10:37  host-manager.2018-06-24.log
0     2018-06-24  16:10:37  manager.2018-06-24.log
86K   2018-06-18  13:49:01  catalina.2018-06-18
17K   2018-06-18  13:49:01  catalina.2018-06-18.log
1.9K  2018-06-18  13:49:01  localhost.2018-06-18.log
0     2018-06-18  13:38:09  host-manager.2018-06-18.log
0     2018-06-18  13:38:09  manager.2018-06-18.log
87K   2018-06-05  14:49:15  catalina.2018-06-05
17K   2018-06-05  14:49:15  catalina.2018-06-05.log
1.9K  2018-06-05  14:49:14  localhost.2018-06-05.log
0     2018-06-05  14:29:30  host-manager.2018-06-05.log
0     2018-06-05  14:29:30  manager.2018-06-05.log
170K  2018-06-04  16:36:01  catalina.2018-06-04
34K   2018-06-04  16:36:01  catalina.2018-06-04.log
3.7K  2018-06-04  16:36:00  localhost.2018-06-04.log
0     2018-06-04  16:08:38  host-manager.2018-06-04.log
0     2018-06-04  16:08:38  manager.2018-06-04.log", 
                              "names" : "后台服务集群1", 
                              "params" : { 
                                   "k" : { 
                                   }, 
                                   "logdir" : "/data/log/server1/", 
                                   "more" : { 
                                   }, 
                                   "search" : { 
                                   }, 
                                   "w" : "1" 
                              }, 
                              "server" : "192.168.0.207" 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { 
          "action" : "Common:Use:getJson", 
          "delete" : "1", 
          "random" : "lookLog", 
          "token" : "1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "Common:Use:getJson,--delete="1",--random="lookLog",--token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  Common:Use:getJson --delete="1" --random="lookLog" --token="1530628106gcfmnmiaouaqygtxaqevrhhjjpjkdgrb" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "2" 
 }

```


