# 基于名字的日志批量查看

**简要描述：**     

- 基于名字的日志批量查看    

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    lookLog   |
|k |  是  |    string   |    名字(app_key)或者名字分组(local_name)<br> 参见数据库字段的app_key或local_name<br> 如果多个app_key,用空格隔开   |



**请求示例**     

`action=lookLog&k=server`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "count" : "2",  //返回总数量
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "data" : {  //返回数据
                                   "app_key" : "server1",  //返回服务器唯一名字
                                   "fileres" : {  //当前日志目录下日志列表
                                        "0" : "444K",  //大小
                                        "1" : "catalina.2018-06-18", 
                                        "2" : "catalina.2018-06-18.log",  
                                        "3" : "localhost.2018-06-18.log", 
                                        "4" : "host-manager.2018-06-18.log", 
                                        "5" : "manager.2018-06-18.log", 
                                        "6" : "catalina.2018-06-05", 
                                        "7" : "catalina.2018-06-05.log", 
                                        "8" : "localhost.2018-06-05.log", 
                                        "9" : "host-manager.2018-06-05.log", 
                                        "10" : "manager.2018-06-05.log", 
                                        "11" : "catalina.2018-06-04", 
                                        "12" : "catalina.2018-06-04.log", 
                                        "13" : "localhost.2018-06-04.log", 
                                        "14" : "host-manager.2018-06-04.log", 
                                        "15" : "manager.2018-06-04.log", 
                                   }, 
                                   "logdir" : "/data/log/server1",  //日志目录路径
                                   "names" : "后台服务集群1", //服务器名称
                                   "params" : { 
                                        "k" : "server1",  //名字
                                        "logdir" : { 
                                        }, 
                                        "more" : { 
                                        }, 
                                        "search" : { 
                                        }, 
                                        "w" : { 
                                        } 
                                   }, 
                                   "server" : "192.168.0.207"  //操作服务器地址
                              }, 
                              "mainProcess" : "13012"  //当前命令的进程ID
                         }, 
                         "1" : {  //返回第2个数据
                              "data" : {  //返回数据
                                   "app_key" : "server2",  //返回服务器唯一名字
                                   "fileres" : { 
                                        "0" : "444K",  //大小
                                        "1" : "catalina.2018-06-18", 
                                        "2" : "catalina.2018-06-18.log", 
                                        "3" : "localhost.2018-06-18.log", 
                                        "4" : "host-manager.2018-06-18.log", 
                                        "5" : "manager.2018-06-18.log", 
                                        "6" : "catalina.2018-06-05", 
                                        "7" : "catalina.2018-06-05.log", 
                                        "8" : "localhost.2018-06-05.log", 
                                        "9" : "host-manager.2018-06-05.log", 
                                        "10" : "manager.2018-06-05.log", 
                                        "11" : "catalina.2018-06-04", 
                                        "12" : "catalina.2018-06-04.log", 
                                        "13" : "localhost.2018-06-04.log", 
                                        "14" : "host-manager.2018-06-04.log", 
                                        "15" : "manager.2018-06-04.log", 
                                   }, 
                                   "logdir" : "/data/log/server2", 
                                   "names" : "后台服务集群2", 
                                   "params" : { 
                                        "k" : "server2",  //名字
                                        "logdir" : { 
                                        }, 
                                        "more" : { 
                                        }, 
                                        "search" : { 
                                        }, 
                                        "w" : { 
                                        } 
                                   }, 
                                   "server" : "192.168.0.76"  //操作服务器地址
                              }, 
                              "mainProcess" : "13012"  //当前命令的进程ID
                         }, 
                    }, 
                    "mainProcess" : "13012",  //当前命令的进程ID
                    "msg" : "success" 
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "lookLog",  //请求模块
          "k" : "server",  //名字
          "token" : "1529853651coqjifffyedvdqfnrghhphcadjgctexi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "lookLog,--k="server",--token="1529853651coqjifffyedvdqfnrghhphcadjgctexi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  lookLog --k="server" --token="1529853651coqjifffyedvdqfnrghhphcadjgctexi" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "4"  //执行命令花费时间
 }

```

