# 基于名字的日志批量下载


## 基于名字的日志批量下载
- getLog

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字的日志批量下载| rex  [module] --k='[k]' | 


**示例**  
下载server(名字分组)应用的今天的日志     
```
rex getLog --k='server' --download_local='1'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
download_local: 日志本地下载或者远程rsync传输;      等于1时下载日志到download/{ip地址}目录下;等于0时上传日志到rsync服务端             
w: 等于1,JSON格式输出结果


**备注**  
download_local=1 下载到download/{ip地址}目录,其中download目录可以参考config.yml             
download_local=0 上传到rsync服务端,其中rsync服务端的配置可以参考config.yml      


**截图**  
示例
![](../assets/getLog1.gif)


