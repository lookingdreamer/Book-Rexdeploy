# 基于名字的日志批量过滤


## 基于名字的日志批量过滤
- grepLog

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字的日志批量过滤| rex  [module] --k='[k]' --grep='' | 


**示例**  
过滤server(名字分组)应用的今天日志含Server startup关键词的地方        
```
rex grepLog --k='server' --grep='Server startup'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）        
grep: 日志过滤关键词（必选）                     
w: 等于1,JSON格式输出结果


**备注**  
   


**截图**  
示例
![](../assets/grepLog1.gif)


