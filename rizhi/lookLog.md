# 基于名字的日志批量查看


## 基于名字的日志批量查看
- lookLog

| 描述 | 命令 | 
| :---: | :--- | 
|通过名字的日志批量查看| rex  [module] --k='[k]' | 


**示例**  
查看server(名字分组)应用的日志列表    
```
rex lookLog --k='server'
```


**参数用法**  
k: 名字或者名字分组 参见数据库字段的app_key或[local_name](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html),如果多个app_key,用空格隔开    （必选）                   
w: 等于1,JSON格式输出结果


**备注**  
 


**截图**  
示例
![](../assets/lookLog1.gif)


