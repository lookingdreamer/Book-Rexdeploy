# 名字服务的日志集中管理系统

首先你需要接入应用，如何接入请参考[接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html),
最关键的是 日志路径(log_dir)  日志文件(logfile) 配置好。当你不配置也是可以在命令传参也是可以的  

我们以案例中server1为例 相关应用信息可参考 https://book.osichina.net/an-zhuang.html

数据库配置解释:  log_dir是指日志文件的目录，logfile是当天的日志文件  
如果你的logfile是含日期，比如catalina.2018-03-28.log，你可以将logfile  
配置为catalina.#%Y-%m-%d.log，程序会自动解析正确的格式

## 五大模块

<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

| 模块 | 描述 |
| :---: | :--- | 
| logCenter:main:liveLog | 查看实时日志  |
| logCenter:main:lookLog | 日志列表查询 |
| logCenter:main:getLog | 日志文件下载 |
| logCenter:main:grepLog | 日志文件过滤 |
| logCenter:main:queryList | 查询当前列表 |

### 查看实时日志
- logCenter:main:liveLog

> (1).实时查看某台服务器当天的日志

| 命令 | 参数 | 
| :---: | :--- | 
| rex [module] --search='[search]' | search(搜索参数): 支持关键词,服务器名称,内网IP,外网IP模糊搜索|

| 示例 | 示例解释 | 
| :---: | :--- | 
| rex logCenter:main:liveLog --search='server1' | 实时查看唯一名字包含server1的应用当天日志([演示](../assets/livelog1.png))|
| rex logCenter:main:liveLog --search='后台服务集群1' | 实时查看服务器名字包含后台服务集群1的应用当天日志|

> (2).实时查看指定服务器的指定日志

| 命令 | 参数 | 
| :---: | :--- | 
| rex -H '[IP]' [module] --log='[logfile]' |   IP:IP地址  logfile:日志的绝对路径|

| 示例 | 示例解释 | 
| :---: | :--- | 
| rex -H '127.0.0.1' logCenter:main:liveLog --log='/data/log/server1/catalina.out' | 实时查看IP:127.0.0.1,日志:<br/>/data/log/server1/catalina.out|

提示1:  search关键词可以通过 logCenter:main:queryList模块来查询
```
rex logCenter:main:queryList
```

提示2:  终止命令请按Ctrl + C

> (3).高级用法

| 描述 | 命令 | 
| :---: | :--- | 
|实时查看并且实时过滤关键词后10行| rex -H '[IP]' [module] --log='[logfile]' &#124; grep -A 10 '[关键词]'| 

### 日志列表查询
-  logCenter:main:lookLog

> (1).查看某台服务器的日志列表

| 命令 | 参数 | 
| :---: | :--- | 
| rex [module] --search='[search]' | search(搜索参数): 支持关键词,服务器名称,内网IP,外网IP模糊搜索|

| 示例 | 示例解释 | 
| :---: | :--- | 
| rex logCenter:main:lookLog --search='server1' | 查看唯一名字包含server1的应用日志列表|
| rex logCenter:main:lookLog --search='后台服务集群1' | 查看服务器名字包含后台服务集群1的应用日志列表|

> (2).查看指定服务器的日志列表

| 命令 | 参数 | 
| :---: | :--- | 
| rex -H '[IP]' [module] --logdir='[logdir]' |   IP:IP地址  logdir:日志目录|

| 示例 | 示例解释 | 
| :---: | :--- | 
| rex -H '127.0.0.1' logCenter:main:lookLog --logdir='/data/log/server1/' | 查看IP:127.0.0.1,/data/log/server1/下的<br/>日志列表([演示](../assets/looklog1.png))|

> (3).高级用法

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名查看日志列表| rex -G '[group]' [module] --logdir='[logdir]'| 
|通过多个IP查看日志列表| rex -H '[IP1 IP2...]' [module] --logdir='[logdir]'| 

分组名可以通过`rex -T`命令查询,同时在ip_lists.ini文件中设置。  
高级命令的用法可以使用户同时查询、过滤并对比和统计集群的中日志  
的关键信息。比如关键词在集群中合计出现次数，不同集群中关键词出现  
的频率等等，完全不需要用户一台台登陆服务器提取再处理。其他模块:    
查看日志列表、下载日志模块、日志文件过滤 也同样支持的分组处理，  
用户可以同时查看/过滤/下载多台服务器的日志

### 日志文件下载

-  logCenter:main:getLog

> (1).下载某台服务器的当天日志

| 命令 | 参数 | 
| :---: | :--- | 
| rex [module] --search='[search]' | search(搜索参数): 支持关键词,服务器名称,内网IP,外网IP模糊搜索|

| 示例 | 示例解释 | 
| :---: | :--- | 
| rex logCenter:main:getLog --search='server1' | 下载唯一名字包含server1的当天应用日志([演示](../assets/getlog1.png))|
| rex logCenter:main:getLog --search='后台服务集群1' | 下载服务器名字包含后台服务集群1的当天应用日志|

> (2).下载指定服务器的指定日志

| 命令 | 参数 | 
| :---: | :--- | 
| rex -H '[IP]' [module] --log='[log]' |   IP:IP地址  log:日志文件|

| 示例 | 示例解释 | 
| :---: | :--- | 
| rex -H '127.0.0.1' logCenter:main:getLog --log='/data/log/server1/catalina.out' | 下载IP:127.0.0.1,日志:<br/>/data/log/server1/catalina.out|

> (3).高级用法

| 描述 | 命令 | 
| :---: | :--- | 
| rex [module] --search='[search]' --download_local='[1]' | 下载当前download路径下,主配置download_dir配置<br/>search(搜索参数): 支持关键词,服务器名称,内网IP,<br/>外网IP模糊搜索|
| rex -H '[IP]' [module] --log='[log]' --download_local='[1]' | 下载当前download路径下,主配置download_dir配置,<br/>IP:IP地址  log:日志文件|
|通过分组名下载日志| rex -G '[group]' [module] --log='[log]'| 
|通过多个IP下载日志| rex -H '[IP1 IP2...]' [module] --log='[log]'| 

- download_local参数

日志文件下载涉及的所有模块都支持download_local参数(0和1)，该参数默认为0。    
当download_local=0，即通过rsync传输日志文件到的rsync服务端，rsync的服务端 
可以通过主配置文件配置(log_rsync_server,log_rsync_user,log_rsync_pass,log_rsync_module),通过nginx代理指向该下载目录，也可以提供web远程下载。  
当download_local=1, 下载日志文件到rex的download目录，该目录也可以通过  
主配置文件的download_dir配置。配置该参数，无需rsync服务端部署。

- 日志文件下载规则

日志文件的下载规则主要是 下载目录/[IP地址]/日志文件  
所以如果是同1个IP,同1个日志文件会出现覆盖的情况，当然你可以通过配置download_local参数，  
来避免这种情况，但是最多2个相同ip的相同名称的日志文件。

- 下载中心地址

如果download_local=0，也就是rsync同步，同时也会提供web下载，docker版本和virtualbox版本，开启了web日志下载，默认的下载地址为  http://127.0.0.1/ +对应后缀  。你也可以自行部署和配置，使用nginx也可以，然后开启auth认证，添加日志下载用户控制，这样开发也可以在web界面自行下载日志，不需要运维人员单独处理。  

### 日志文件过滤

-  logCenter:main:grepLog

> (1).过滤某台服务器的当天日志

| 命令 | 参数 | 
| :---: | :--- | 
| rex [module] --search='[search]' --grep='[grep>]' | search(搜索参数): 支持关键词,服务器名称,内网IP,<br/>外网IP模糊搜索<br/> grep:过滤关键词|

| 示例 | 示例解释 | 
| :---: | :--- | 
| rex logCenter:main:grepLog --search='server1' --grep='转换JSON数据' | 过滤唯一名字含server1且日志内容<br/>含'转换JSON数据'的当天应用日志|
| rex logCenter:main:grepLog --search='后台服务集群1' --grep='转换JSON数据' | 过滤服务器名字含后台服务集群1且<br/>日志内容含'转换JSON数据'的当天<br/>应用日志|

> (2).过滤指定服务器的指定日志

| 命令 | 参数 | 
| :---: | :--- | 
| rex -H '[IP]' [module] --log='[log]' --grep='[grep>]'|   IP:IP地址  log:日志文件 grep:过滤关键词|

| 示例 | 解释 | 
| :---: | :--- | 
| rex -H '127.0.0.1' logCenter:main:grepLog --log='/data/log/server1/catalina.out' --grep='转换JSON数据' | ...|

> (3).高级用法

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名查询过滤日志| rex -G '[group]' [module] --log='[log]' --grep='[grep>]'| 
|通过多个IP查询过滤日志| rex -H '[IP1 IP2...]' [module] --log='[log]' --grep='[grep>]'| 

### 查询当前列表

-  logCenter:main:queryList

> 查询环境所有服务器的名称、内网地址、外网地址

| 命令 | 参数 | 
| :---: | :--- | 
| rex [module] --search='[search]' | 查询环境所有服务器的名称、内网地址、外网地址 |


