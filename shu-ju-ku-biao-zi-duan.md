# 数据库表约束和解释

数据库表主要包含以下7个表

| 表名 | 说明 |
| :--- | :--- |
| [pre\_auto\_configure](https://book.osichina.net/shu-ju-ku-biao-zi-duan/pre_auto_configure.html) | 配置模板关联表 |
| [pre\_auto\_template\_vars](https://book.osichina.net/shu-ju-ku-biao-zi-duan/pre_auto_template_vars.html) | 配置模板表 |
| [pre\_bwzzbmonitor](https://book.osichina.net/shu-ju-ku-biao-zi-duan/pre_bwzzbmonitor.html) | 监控信息表 |
| [pre\_deploy\_manage](https://book.osichina.net/shu-ju-ku-biao-zi-duan/pre_deploy_manage.html) | 自动发布信息记录表 |
| [pre\_deploy\_status](https://book.osichina.net/shu-ju-ku-biao-zi-duan/pre_deploy_status.html) | 自动发布状态表 |
| [pre\_external\_server\_detail](https://book.osichina.net/shu-ju-ku-biao-zi-duan/pre_external_server_detail.html) | CMDB主机配置外网表 |
| [pre\_server\_detail](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html) | CMDB主机配置表 |



这7个表中最关键的是CMDB主机配置表，所有名字服务关联的自动发布等都从这个表中取关键信息。

