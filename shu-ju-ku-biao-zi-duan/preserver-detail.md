# pre_server_detail 

CMDB主机配置表(重要)，包含应用配置文件，应用程序位置，应用关键词等等

## 数据表结构
```
CREATE TABLE `pre_server_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `yewu_name` varchar(255) DEFAULT NULL COMMENT '业务管理',
  `depart_name` varchar(64) DEFAULT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称',
  `external_ip` varchar(15) DEFAULT NULL COMMENT '公网IP',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `server_id` int(11) DEFAULT NULL COMMENT '服务器ID',
  `mirr_id` int(11) DEFAULT NULL COMMENT '镜像ID',
  `pro_type` varchar(164) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(255) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(255) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(164) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(164) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(164) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `entrance_server` varchar(64) DEFAULT NULL COMMENT '所属主机',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '最后更新记录时间',
  `groupby` varchar(128) DEFAULT NULL COMMENT '分组名称',
  `local_name` varchar(200) DEFAULT NULL COMMENT '识别名称',
  `app_key` varchar(200) DEFAULT NULL COMMENT '应用唯一关键词',
  `is_deloy_dir` varchar(64) DEFAULT NULL COMMENT '发布目录判断',
  `container_dir` varchar(100) DEFAULT NULL COMMENT '容器目录',
  `java_versoin` varchar(64) DEFAULT NULL COMMENT 'java版本',
  `java_home` varchar(100) DEFAULT NULL COMMENT 'java的Home目录',
  `java_confirm` varchar(64) DEFAULT NULL COMMENT 'java版本确认',
  `auto_deloy` varchar(64) DEFAULT '0' COMMENT '是否加入自动发布',
  `backupdir_same_level` int(11) DEFAULT NULL COMMENT '备份目录是否和pro_dir同级',
  `deploydir_same_level` int(11) DEFAULT NULL COMMENT '发布目录是否和pro_dir同级',
  `huanjin_name` varchar(255) DEFAULT NULL COMMENT '环境管理',
  `jifang_name` varchar(255) DEFAULT NULL COMMENT '机房管理',
  `env` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `configure_file_list` text COMMENT '同步远程配置文件到待发表目录的配置组',
  `configure_file_status` int(11) DEFAULT NULL COMMENT '配置文件状态0为拷贝整个目录，1为读取configure_file_list的列表文件',
  `checkurl_status` int(11) DEFAULT '1' COMMENT '校验url',
  `differcount` varchar(255) DEFAULT NULL COMMENT '变化校验结果',
  `weight` varchar(255) DEFAULT NULL COMMENT '权重',
  `loadBalancerId` text COMMENT '负载均衡ID',
  `logfile` varchar(255) DEFAULT NULL COMMENT '日志文件',
  `url` varchar(255) DEFAULT NULL COMMENT 'url',
  `header` text COMMENT 'header',
  `params` text COMMENT 'post参数',
  `require` varchar(255) DEFAULT NULL COMMENT '返回校验',
  `requirecode` varchar(255) DEFAULT NULL COMMENT '期望状态码',
  `checkdir` text COMMENT '校验更新文件',
  `predir` text COMMENT 'http包合并下层路径',
  `local_pro_cmd` text COMMENT '待发布包之前的工程初始化操作',
  `local_conf_cmd` text COMMENT '待发布包之前的配置初始化操作',
  `is_restart_status` int(11) DEFAULT '0' COMMENT '不需要启动和关闭应用'
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;
```

## 功能概述
如上所述，pre_server_detail 主要存放跟主机相关的信息表。

针对pre_server_detail，作重点解释。
重点字段如下:
```
"id","app_key","server_name","network_ip","pro_type","config_dir","pro_dir","pro_key","pro_init","local_name","is_deloy_dir"
```

- app_key: ```应用发布的唯一关键词,不能有重复,不能为空,如果为空,则不会加入到自动发布的系统里面.```
- pro_key: ```进程关键字最好选择的唯一的关键词,在关闭应用失败的时候,会通过应用关键词去KILL应用```
- pro_init: ```启动脚本```
- is_deloy_dir: ```发布目录判断,2代表工程路径和配置路径是隔离开来的,比如:server1的工程路径为: /data/www/html1 配置路径为: /data/www/config1;1代表 工程路径和配置路径是合在一起的,比如flow1,它的工程路径为/data/www/flow1,配置路径为: /data/www/flow1/WEB-INF/classes/```
- local_name: ```应用发布初始目录的名字,比如 server1系统设置的local_name为server,且is_deloy_dir为2,那么发布的初始目录为: 工程路径:$softdir/server 配置路径为: $configure/server/server1 ```

## 举例

