# 上传文件

**简要描述：** 

- 上传文件

**请求URL：** 
- ` http://127.0.0.1:3000/upload?token=1529853651coqjifffyedvdqfnrghhphcadjgctexi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|upload |  是  |    string   |    上传的文件   |
|unzip |  是  |    string   |    是否解压 1解压 0不解压 仅解压tar/zip  |


**请求示例**  
 
必须是上传文件类型    
`upload=压缩包&unzip=1`

**返回示例**

``` 
{
    "code": 0, //0上传成功  
    "download": "/data/RexdeployV3/download/web/1531928243", //保存路径
    "downloadDir": "/data/RexdeployV3/download/web",
    "filename": "Thumbs.db.zip",
    "move_to": "/data/RexdeployV3/download/web/1531928243/Thumbs.db.zip",
    "msg": "上传成功", 
    "name": "upload",
    "size": 72721,  //文件大小 字节
    "start_time": "2018/07/18 15:37:23", //请求时间
    "take": 0, //花费时间
    "unzipRes": { //解压结果
        "cmd": "unzip /data/RexdeployV3/download/web/1531928243/Thumbs.db.zip -d /data/RexdeployV3/download/web/1531928243",
        "code": 1, //1解压成功
        "res": {
            "chdir": "/tmp",
            "cmd": "unzip /data/RexdeployV3/download/web/1531928243/Thumbs.db.zip -d /data/RexdeployV3/download/web/1531928243",
            "code": 1,
            "msg": "执行完成",
            "ret": 0,
            "stderr": "",
            "stdout": "Archive:  /data/RexdeployV3/download/web/1531928243/Thumbs.db.zip\n  inflating: /data/RexdeployV3/download/web/1531928243/Thumbs.db  \n"
        }
    }
}

```

