# 普通用户创建

## 批量主机普通用户创建

**简要描述：**     

- 普通用户创建     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    User:main:route   |
|function |  是  |    string   |    create   |
|H |  是  |    string   |    指定主机多个空格间隔   |
|G/g |  是  |    string   |    分组名,H或者G参数任何一个   |
|random |  是  |    string   |    随机数   |
|user |  是  |    string   |    用户名   |
|pass |  否  |    string   |    如果pass参数不传时，默认创建的用户的密码和账号一致   |
|sudo |  否  |    int   |    如果sudo参数不传时，<br>默认没有sudo权限，<br>该操作是把用户添加至wheel组   |
|allow |  否  |    int   |    如果pass参数不传时，<br>该操作是将用户添加至sshd_config配置文件中的AllowUsers中，<br>如果该配置未设置，该参数不起作用   |

**请求示例**         
 
`G=group&action=User:main:route&allow=1&function=create&pass=testuser&random=create&sudo=1&user=testuser`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
     }, 
     "param" : { //请求参数
          "G" : "group",  //执行主机分组,通过rex -T查询,通过config/iplists.ini定义
          "action" : "User:main:route",  //请求模块
          "allow" : "1", 
          "function" : "create", 
          "pass" : "testuser", 
          "random" : "create",  //随机数
          "sudo" : "1", 
          "token" : "1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi",  //token
          "user" : "testuser" 
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "-G "group"",  //请求参数前缀
          "requestCmd" : "User:main:route,--allow="1",--function="create",--pass="testuser",--random="create",--sudo="1",--token="1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi",--user="testuser",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF -G "group" User:main:route --allow="1" --function="create" --pass="testuser" --random="create" --sudo="1" --token="1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi" --user="testuser" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "5"  //执行命令花费时间
 }

```

## 获取批量主机普通用户创建结果

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:getJson   |
|random |  是  |    string   |    随机数,批量执行的随机数   |
|delete |  是  |    int   |    获取结果之后是否删除内存中保存的数据,1时删除   |
**请求示例**     
`action=json&delete=1&random=create`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "1", 
          "count" : "2",  //返回总数量
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "createUser" : { 
                                   "0" : {  //返回第1个数据
                                        "append_allow" : "1", 
                                        "code" : "1",  //1请求创建成功
                                        "create" : "1", //1创建成功 非1创建失败
                                        "createUserParams" : { //创建时参数 
                                             "allow" : "1", 
                                             "dir" : "0", 
                                             "level" : "0", 
                                             "pass" : "testuser", 
                                             "query" : "0", 
                                             "sudo" : "1", 
                                             "user" : "testuser" 
                                        }, 
                                        "create_sudo" : "1", //添加sudo 1成功
                                        "msg" : "success" 
                                   }, 
                              }, 
                              "params" : { 
                                   "action" : "create",  //请求模块
                                   "allow" : "1", 
                                   "batch" : { 
                                   }, 
                                   "dir" : { 
                                   }, 
                                   "level" : { 
                                   }, 
                                   "pass" : "testuser", 
                                   "sudo" : "1", 
                                   "user" : "testuser", 
                                   "w" : "1" 
                              }, 
                              "query" : { 
                                   "0" : "0",  //返回第1个数据
                              }, 
                              "server" : "172.17.0.2"  //操作服务器地址
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
               "1" : {  //返回第2个数据
                    "code" : "0",  //0 成功 非0 失败
                    "data" : {  //返回数据
                         "0" : {  //返回第1个数据
                              "createUser" : { 
                                   "0" : {  //返回第1个数据
                                        "append_allow" : "1", 
                                        "code" : "1", 
                                        "create" : "1", 
                                        "createUserParams" : { 
                                             "allow" : "1", 
                                             "dir" : "0", 
                                             "level" : "0", 
                                             "pass" : "testuser", 
                                             "query" : "0", 
                                             "sudo" : "1", 
                                             "user" : "testuser" 
                                        }, 
                                        "create_sudo" : "1", 
                                        "msg" : "success" 
                                   }, 
                              }, 
                              "params" : { 
                                   "action" : "create",  //请求模块
                                   "allow" : "1", 
                                   "batch" : { 
                                   }, 
                                   "dir" : { 
                                   }, 
                                   "level" : { 
                                   }, 
                                   "pass" : "testuser", 
                                   "sudo" : "1", 
                                   "user" : "testuser", 
                                   "w" : "1" 
                              }, 
                              "query" : { 
                                   "0" : "0",  //返回第1个数据
                              }, 
                              "server" : "172.17.0.3"  //操作服务器地址
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { //请求参数
          "action" : "json",  //请求模块
          "delete" : "1",  //Common:Use:getJson获取结果之后是否删除内存中保存的数据,为1时删除
          "random" : "create",  //随机数
          "token" : "1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi"  //token
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "json,--delete="1",--random="create",--token="1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  json --delete="1" --random="create" --token="1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "2"  //执行命令花费时间
 }

```

