# 创建用户秘钥

**简要描述：**     

- 创建用户秘钥     


**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |     User:main:route    |
|function |  是  |    string   |    createkey   |
|user |  是  |    string   |    秘钥用户名   |
|pass |  是  |    string   |    秘钥用户密码   |

**请求示例**       

`action=User:main:route&function=createkey&pass=testabc&user=testabc`


**返回示例**

``` 
{ 
     "code" : "0",  //0 成功 非0 失败
     "data" : {  //返回数据
          "code" : "0",  //0 成功 非0 失败
          "data" : {  //返回数据
               "0" : {  //返回第1个数据
                    "general_key" : { 
                         "0" : "/tmp/ukmgyvoagh",  //返回第1个数据,用户秘钥保存路径
                    }, 
                    "params" : { 
                         "action" : "createkey",  //请求模块
                         "allow" : { 
                         }, 
                         "batch" : { 
                         }, 
                         "dir" : { 
                         }, 
                         "level" : { 
                         }, 
                         "pass" : "testabc", 
                         "sudo" : { 
                         }, 
                         "user" : "testabc", 
                         "w" : "1" 
                    }, 
                    "server" : "<local>"  //操作服务器地址
               }, 
          }, 
          "msg" : "成功" 
     }, 
     "param" : { //请求参数
          "action" : "User:main:route",  //请求模块
          "function" : "createkey", 
          "pass" : "testabc", 
          "token" : "1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi",  //token
          "user" : "testabc" 
     }, 
     "parseparameters" : {  //解析,组合以及校验参数结果
          "code" : "0",  //0 成功 非0 失败
          "msg" : "校验和解析参数成功", 
          "precmd" : "",  //请求参数前缀
          "requestCmd" : "User:main:route,--function="createkey",--pass="testabc",--token="1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi",--user="testabc",--w="1""  //请求参数后缀
     }, 
     "print_stdout" : "0",  //是否打印输出结果 0 不打印 1 打印
     "respon" : {  //请求命令返回结果信息
          "chdir" : "/data/RexdeployV3",  //命令执行路径
          "cmd" : "/usr/bin/rex  -qF  User:main:route --function="createkey" --pass="testabc" --token="1529932344hohgjdqbjqmfjsdtyeriomkrduckgjhi" --user="testabc" --w="1"",  //执行命令
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0",  //最终执行命令返回值 0 成功 非0失败
          "stderr" : "",  //最终执行命令标准错误输出
          "stdout" : ""  //最终执行命令标准输出
     }, 
     "take" : "2"  //执行命令花费时间
 }

```

