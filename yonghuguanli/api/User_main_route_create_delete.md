# 用户删除

## 批量主机用户删除

**简要描述：**     

- 批量主机用户删除      



**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    User:main:route    |
|function |  是  |    string   |    delete   |
|random |  是  |    string   |    随机数   |
|user |  是  |    string   |    删除的用户名   |
|H |  是  |    string   |    指定主机多个空格间隔   |
|G/g |  是  |    string   |    分组名,H或者G参数任何一个   |

**请求示例**        

`G=group&action=User:main:route&function=delete&random=deleteUser&user=test`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
     }, 
     "param" : { 
          "G" : "group", 
          "action" : "User:main:route", 
          "function" : "delete", 
          "random" : "deleteUser", 
          "token" : "1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu", 
          "user" : "test" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "-G "group"", 
          "requestCmd" : "User:main:route,--function="delete",--random="deleteUser",--token="1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu",--user="test",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF -G "group" User:main:route --function="delete" --random="deleteUser" --token="1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu" --user="test" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "4" 
 }

```

## 获取批量主机用户删除结果

**请求URL：** 
- ` http://127.0.0.1:3000/rex?token=1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu `
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|action |  是  |    string   |    模块名:必须为Common:Use:getJson   |
|random |  是  |    string   |    随机数   |
|delete |  是  |    int   |    获取结果之后是否删除内存中保存的数据,1时删除   |

**请求示例**     
`action=Common:Use:getJson&delete=1&random=deleteUser`


**返回示例**

``` 
{ 
     "code" : "0", 
     "data" : { 
          "code" : "1", 
          "count" : "2", 
          "data" : { 
               "0" : {  //第1个参数
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "deleteUser" : { 
                                   "0" : "test", //返回删除的用户则证明删除成功
                              }, 
                              "params" : { 
                                   "action" : "delete", 
                                   "allow" : { 
                                   }, 
                                   "batch" : { 
                                   }, 
                                   "dir" : { 
                                   }, 
                                   "level" : { 
                                   }, 
                                   "pass" : { 
                                   }, 
                                   "sudo" : { 
                                   }, 
                                   "user" : "test", 
                                   "w" : "1" 
                              }, 
                              "server" : "172.17.0.3" 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
               "1" : {  //第2个参数
                    "code" : "0", 
                    "data" : { 
                         "0" : { 
                              "deleteUser" : { 
                                   "0" : "test", //返回删除的用户则证明删除成功
                              }, 
                              "params" : { 
                                   "action" : "delete", 
                                   "allow" : { 
                                   }, 
                                   "batch" : { 
                                   }, 
                                   "dir" : { 
                                   }, 
                                   "level" : { 
                                   }, 
                                   "pass" : { 
                                   }, 
                                   "sudo" : { 
                                   }, 
                                   "user" : "test", 
                                   "w" : "1" 
                              }, 
                              "server" : "172.17.0.2" 
                         }, 
                    }, 
                    "msg" : "成功" 
               }, 
          }, 
          "msg" : "success" 
     }, 
     "param" : { 
          "action" : "Common:Use:getJson", 
          "delete" : "1", 
          "random" : "deleteUser", 
          "token" : "1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu" 
     }, 
     "parseparameters" : { 
          "code" : "0", 
          "msg" : "校验和解析参数成功", 
          "precmd" : "", 
          "requestCmd" : "Common:Use:getJson,--delete="1",--random="deleteUser",--token="1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu",--w="1"" 
     }, 
     "print_stdout" : "0", 
     "respon" : { 
          "chdir" : "/data/RexdeployV3", 
          "cmd" : "/usr/bin/rex  -qF  Common:Use:getJson --delete="1" --random="deleteUser" --token="1530457754qosawkxlslmbdbbkkoqmwdjdgorufdgu" --w="1"", 
          "code" : "1", 
          "msg" : "执行完成", 
          "ret" : "0", 
          "stderr" : "", 
          "stdout" : "" 
     }, 
     "take" : "2" 
 }

```

