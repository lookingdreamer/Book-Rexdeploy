# 用户创建

用户创建分为2类，服务器普通用户以及秘钥用户。  

## User:main:route

### 创建普通用户

<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

- 用法

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名创建普通用户| rex -G '[group]' [module] --action='create' --user='[用户名]' <br/>[--pass='密码']  [--sudo=1]  [--allow=1] | 
|通过多个IP创建普通用户|rex -H '[IP1 IP2...]' [module] --action='create' --user='[用户名]‘ <br/>[--pass='密码']  [--sudo=1]  [--allow=1] | 

- 参数

action: 必须，必须为create  
user: 必须，用户名  
passwd: 非必须，如果pass参数不传时，默认创建的用户的密码和账号一致。  
sudo: 非必须，如果sudo参数不传时，默认没有sudo权限，该操作是把用户添加至wheel组。  
allow: 非必须，如果pass参数不传时，该操作是将用户添加至sshd_config配置文件中的AllowUsers中，如果该配置未设置，该参数不起作用。  

- 示例

IP: 127.0.0.1 创建test普通用户，同时设置密码为testabc，同时添加至wheel组中
```
rex -H "127.0.0.1" User:main:route --action='create' --user='test' --pass='testabc' --sudo=1
```

- 并发参数

该User:main:route也可以通过设置并发参数执行，-t参数设置并发执行个数 
比如并发10个线程 创建用户
```
rex -H "127.0.0.1" -t 10  User:main:route --action='create' --user='test' --pass='testabc' --sudo=1
```


### 创建秘钥用户

以下的模块都支持并发参数-t，-t参数设置并发执行个数 

#### 生成秘钥
- 用法

| 描述 | 命令 | 
| :---: | :--- | 
|生成用户秘钥| rex [module] --action='createkey' --user='[用户名]' --pass='[ 秘钥密码]' | 

- 参数

action: 必须，必须为createkey   
user: 必须，用户名  
passwd: 非必须，如果pass参数不传时，默认创建的秘钥的密码为空，意思就是不需要秘钥密码，秘钥默认是rsa加密 

- 示例

创建test用户的秘钥，同时设置秘钥的密码为'testabc123456'
```
[root@9ad2c46f179f RexDeployV3]# rex User:main:route --action='createkey' --user='test' --pass='testabc123456'
[2018-04-04 15:36:35] INFO - Running task User:main:route on <local>
[2018-04-04 15:36:35] INFO - user参数: test action参数: createkey
[2018-04-04 15:36:35] INFO - level参数: 0 sudo参数: 0 pass参数:testabc123456 allow参数: 0 dir参数:0 batch参数:0
[2018-04-04 15:36:35] INFO - 服务器: <local>
[2018-04-04 15:36:35] INFO - __SUB__:开始生成秘钥文件
[2018-04-04 15:36:35] INFO - 秘钥密码:testabc123456
[2018-04-04 15:36:35] INFO - 开始生成秘钥
[2018-04-04 15:36:35] INFO - cmd:mkdir -p /tmp/ratwvktlxy ; ssh-keygen -t rsa -f /tmp/ratwvktlxy/test  -P 'testabc123456' &&  result=$? ;echo status=$result
[2018-04-04 15:36:35] INFO - 秘钥路径: /tmp/ratwvktlxy
[2018-04-04 15:36:35] INFO - 生成秘钥成功
[2018-04-04 15:36:36] INFO - All tasks successful on all hosts
[root@9ad2c46f179f RexDeployV3]#
```


#### 创建秘钥用户(固定秘钥, 有密码)

秘钥用户，sshd_config中AuthorizedKeysFile设置为.ssh/authorized_keys   
以下秘钥用户的模块操作都是基于以上AuthorizedKeysFile的路径

- 用法

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名创建秘钥用户| rex -G '[group]' [module] --action='create' --user='[用户名]' <br/>--dir='[秘钥目录]' --level='1' [--sudo=1]  [--allow=1] | 
|通过多个IP创建秘钥用户|rex -H '[IP1 IP2...]' [module] --action='create' --user='[用户名]‘ <br/>--dir='[秘钥目录]' --level='1' [--sudo=1]  [--allow=1] | 

- 参数

action: 必须，必须为create  
user: 必须，用户名  
dir: 必须，秘钥目录，生成秘钥目录，参照上面段落   
level: 必须，必须为1      
sudo: 非必须，如果sudo参数不传时，默认没有sudo权限，该操作是把用户添加至wheel组。  
allow: 非必须，如果pass参数不传时，该操作是将用户添加至sshd_config配置文件中的AllowUsers中，如果该配置未设置，该参数不起作用。  

- 示例

IP: 127.0.0.1 创建test秘钥用户，同时设置密码为testabc123456，同时添加至wheel组，同时加入allow用户中
```
rex -H '127.0.0.1' User:main:route --action='create' --user='test' --level='1' --sudo=1 --allow=1 --dir='/tmp/ratwvktlxy'
```


#### 创建秘钥用户(随机秘钥,无密码)

秘钥用户，sshd_config中AuthorizedKeysFile设置为.ssh/authorized_keys   
以下秘钥用户的模块操作都是基于以上AuthorizedKeysFile的路径  
该模块如果对分组和多个IP操作，每台机器生成的秘钥都是不一样，在实际的生产环境，参考意义不大  

- 用法

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名创建秘钥用户| rex -G '[group]' [module] --action='create' --user='[用户名]' <br/> --level='2' [--sudo=1]  [--allow=1] | 
|通过多个IP创建秘钥用户|rex -H '[IP1 IP2...]' [module] --action='create' --user='[用户名]‘ <br/> --level='2' [--sudo=1]  [--allow=1] | 

- 参数

action: 必须，必须为create  
user: 必须，用户名  
level: 必须，必须为2       
sudo: 非必须，如果sudo参数不传时，默认没有sudo权限，该操作是把用户添加至wheel组。  
allow: 非必须，如果pass参数不传时，该操作是将用户添加至sshd_config配置文件中的AllowUsers中，如果该配置未设置，该参数不起作用。  

- 示例

IP: 127.0.0.1 创建test秘钥用户，同时设置秘钥密码为空，同时添加至wheel组，同时加入allow用户中
```
rex -H '127.0.0.1' User:main:route --action='create' --user='test' --level='2' --sudo=1 --allow=1 
```



#### 创建秘钥用户(随机秘钥,有密码)

秘钥用户，sshd_config中AuthorizedKeysFile设置为.ssh/authorized_keys   
以下秘钥用户的模块操作都是基于以上AuthorizedKeysFile的路径  
该模块如果对分组和多个IP操作，每台机器生成的秘钥都是不一样，在实际的生产环境，参考意义不大  

- 用法

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名创建秘钥用户| rex -G '[group]' [module] --action='create' --user='[用户名]' <br/> --level='2' [--sudo=1]  [--allow=1] | 
|通过多个IP创建秘钥用户|rex -H '[IP1 IP2...]' [module] --action='create' --user='[用户名]‘ <br/> --level='2' [--sudo=1]  [--allow=1] | 

- 参数

action: 必须，必须为create  
user: 必须，用户名  
level: 必须，必须为2    
pass: 必须，秘钥密码 。     
sudo: 非必须，如果sudo参数不传时，默认没有sudo权限，该操作是把用户添加至wheel组。  
allow: 非必须，如果pass参数不传时，该操作是将用户添加至sshd_config配置文件中的AllowUsers中，如果该配置未设置，该参数不起作用。  

- 示例

IP: 127.0.0.1 创建test秘钥用户，同时设置秘钥密码为123456，同时添加至wheel组，同时加入allow用户中
```
rex -H '127.0.0.1' User:main:route --action='create' --user='test' --level='2' --sudo=1 --allow=1 --pass='123456'
```
