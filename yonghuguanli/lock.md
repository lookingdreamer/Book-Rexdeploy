# 锁定用户

该模块是锁定服务器系统用户  

## User:main:route

### 锁定用户

<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

- 用法

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名锁定用户| rex -G '[group]' [module] --action='lock' --user='[用户名]'  | 
|通过多个IP锁定用户|rex -H '[IP1 IP2...]' [module] --action='lock' --user='[用户名]‘  | 

- 参数

action: 必须，必须为lock  
user: 必须，用户名  

- 示例

IP: 127.0.0.1 锁定test用户
```
rex -H "127.0.0.1" User:main:route --action='lock' --user='test'
```

- 并发参数

该User:main:route也可以通过设置并发参数执行，-t参数设置并发执行个数 
比如并发10个线程 锁定用户
```
rex -H "127.0.0.1" -t 10  User:main:route --action='lock' --user='test'
```

