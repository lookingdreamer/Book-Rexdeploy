# 用户查询

## User:main:route
- 查询可登录的用户列表

<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名查询用户列表| rex -G '[group]' [module] --action='list'| 
|通过多个IP查询用户列表|rex -H '[IP1 IP2...]' [module] --action='list'| 


- 查询用户是否存在

| 描述 | 命令 | 
| :---: | :--- | 
|通过分组名查询用户 | rex -G '[group]' [module] --action='query' --user='[用户名]'| 
|通过多个IP查询用户 | rex -H '[IP1 IP2...]' [module] --action='query' --user='[用户名]'| 


- 并发参数

该User:main:route也可以通过设置并发参数执行，-t参数设置并发执行个数   
比如并发10个线程 查询服务用户

```
rex -t 10  -G group User:main:route --action='list'
```