# 基于名字服务的自动发布

基于名字服务的自动发布原理，可以参照我的这张图: [https://book.osichina.net/assets/yuanli.png](https://book.osichina.net/assets/yuanli.png)

- 自动发布原理解释
> 通过唯一名字关键词从数据库匹配到服务器基本信息，将本地工程路径对应的程序和配置同步到远程发布，然后进行一系列的修改软链接，重启应用等操作。


