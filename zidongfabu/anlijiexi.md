# 案例解析

如上上章节，[从案例开始](https://book.osichina.net/zidongfabu/conganlikaishi.html)
一共4个步骤，现在对每个步骤的进行详细解析，便于理解。


## 1. 同步集群应用、配置到本地
```
rex Enter:route:download --k='server1 server2'
```
> - 说明: 该步骤是将远程server1 server2对应的程序和配置同步到本地remotecomdir目录中
> - 目的: 保障待发布的应用和配置一致(比如我此次更新只有class文件，那么我需要将环境中正在使用的程序包和配置包下载下来，然后覆盖发布)
[同步下载截图](https://book.osichina.net/assets/download.png)

从执行过程当中我们就知道，这个步骤首先通过关键词查询该这2个应用关联的服务器信息，比如工程路径，配置路径，启动脚本，进程关键词。然后根据
主配置文件的认证信息，连接到远程服务器去下载对应的程序和配置。下载主要是通过rsync的ssh通道进行下载。同时也具有并发功能，通过主配置文件
可以配置并发的个数。


## 2. 同步应用、配置到待发布目录
```
rex  Deploy:Core:syncpro --k='server1 server2'
```
> - 说明: 该步骤是将本地remotecomdir中server1 server2的程序和配置同步到待发布目录softdir和configuredir目录中
> - 目的: 保障待发布的应用和配置一致，以及在做特殊配置处理([后面章节会讲到](https://book.osichina.net/zidongfabu/jieruxinyingyong.html))
[同步移动截图](https://book.osichina.net/assets/syncdir.png)

这个过程其实就是把remotecomdir下载下来的代码和配置，再一次的移动到待发布目录softdir和configuredir目录。也许有人问，为什么不直接把代码下载待发布目录中。因为在这个过程，我们可能要处理另外一种情况。有一些应用，它的代码和配置并没有实现分离。比如JAVA应用有1个配置文件在class下面，那么这时候，我在
数据库配置中配置好，只需要移动其中1个配置文件即可。(如果没有理解，等下会在[接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)给大家演示)

## 3. 上传修改代码
将要修改的文件覆盖到 softdir/server/目录中
> * 为什么是 softdir/server 目录？
> 因为我在数据库(pre_server_detail)中定义了server1 server2的local_name都为server，那么则证明这2个应用是同一个应用，
> 使用同一个工程包，同时它的待发布的工程包的路径一定是在softdir/{local_name}路径下。具体详细细节，参照 [接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)

在这步骤，比如softdir/server/ 是server1 server2 的工程路径，configuredir/server/{server1,server2}为对应配置路径
为什么是这样的规则？答：目前configuredir/softdir是通过主配置文件定义，softdir/{local_name}/ ,configuredir/{local_name}/{app_key(1),app_key(2)},后面主要是通过数据库配置字段local_name以及app_key定义的

## 4. 执行发布
```
rex  Enter:route:deploy --k='server1 server2'
```
> - 说明: 该步骤是将工程包和配置，并发的形式上传到对应远程服务器中，同时修改软链接重启应用等操作。
[自动发布截图](https://book.osichina.net/assets/deploy.png)

执行发布的步骤，就是一个传包，修改软链接，重启应用的操作。