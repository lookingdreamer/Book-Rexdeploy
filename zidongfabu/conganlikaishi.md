# 从案例开始

## 案例说明
比如我们有1个后台服务集群的应用(server)需要发布，server应用是一个集群,该集群有2个节点。

- 集群的基本信息如下:
<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

| 名字 | 服务名称描述 | 服务器 | 配置文件路径 | 工程文件路径 | 类型 |
| :---: | :--- | :--- | :--- | :--- | :--- | :--- |
| server1 | 后台服务集群1 | 127.0.0.1  | /data/www/config1 | /data/www/html1 | tomcat |
| server2 | 后台服务集群2 | 127.0.0.1  | /data/www/config2 | /data/www/html2 | tomcat |

## 自动发布步骤

1. 同步集群应用、配置到本地
```
rex Enter:route:download --k='server1 server2'
```
> - 说明: 该步骤是将远程server1 server2对应的程序和配置同步到本地remotecomdir目录中
> - 目的: 保障待发布的应用和配置一致(比如我此次更新只有class文件，那么我需要将环境中正在使用的程序包和配置包下载下来，然后覆盖发布)
[同步下载截图](https://book.osichina.net/assets/download.png)

2. 同步应用、配置到待发布目录
```
rex  Deploy:Core:syncpro --k='server1 server2'
```
> - 说明: 该步骤是将本地remotecomdir中server1 server2的程序和配置同步到待发布目录softdir和configuredir目录中
> - 目的: 保障待发布的应用和配置一致，以及在做特殊配置处理([后面章节会讲到](https://book.osichina.net/zidongfabu/jieruxinyingyong.html))
[同步移动截图](https://book.osichina.net/assets/syncdir.png)

3. 上传修改代码
将要修改的文件覆盖到 softdir/server/目录中
> * 为什么是 softdir/server 目录？
> 因为我在数据库(pre_server_detail)中定义了server1 server2的local_name都为server，那么则证明这2个应用是同一个应用，
> 使用同一个工程包，同时它的待发布的工程包的路径一定是在softdir/{local_name}路径下。具体详细细节，参照 [接入新应用](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)

4. 执行发布
```
rex  Enter:route:deploy --k='server1 server2'
```
> - 说明: 该步骤是将工程包和配置，并发的形式上传到对应远程服务器中，同时修改软链接重启应用等操作。
[自动发布截图](https://book.osichina.net/assets/deploy.png)