# 自动回滚 

我们以[从案例开始](https://book.osichina.net/zidongfabu/conganlikaishi.html)的案例举例。  

- Enter:route:rollback

<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

| 命令 | 参数 | 
| :---: | :--- | 
| rex [module] --k='[app_key1 app_key2 ...]' --rollstatus='[0/1]' | app_key:唯一名字,空格间隔 <br/>rollstatus:回滚状态|

rollstatus为0 回滚到上一版本   
rollstatus为1 根据数据库字段rollStatus=1回滚到任意版本

- 举例1 回滚到上一版本

```
rex  Enter:route:rollback --k='server1 server2' --rollstatus='0'
```

- 举例2 回滚到任意版本

```
rex  Enter:route:rollback --k='server1 server2' --rollstatus='1'
```

当rollstatus=1时，该命令会去查询CMDB主机配置表(pre_server_detail)，  
当app_key='server1' 和 rollStatus=1时的记录回滚
