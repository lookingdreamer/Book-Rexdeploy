# 发布目录介绍

关于目录的结构介绍请撮，[https://book.osichina.net/mulujiegou.html](https://book.osichina.net/mulujiegou.html)

那么对于使用者而言，我们只需要关注`softdir`、`configuredir` 目录
soft为存放程序目录，configuredir为存放配置目录。如果配置在应用程序
中的话，发布的过程中会自动把配置合并到程序目录当中。
但是我觉得还是有必要针对应用发布涉及到目录简要的介绍一下

## 发布目录原理介绍
如 接入新应用章节中，[类型一: 配置和应用分开部署](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)

<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

| 名字 | 服务名称描述 | 服务器 | 配置文件路径 | 工程文件路径 | 类型 |
| :---: | :--- | :--- | :--- | :--- | :--- | :--- |
| newserver | 新增测试服务 | 127.0.0.1  | /data/www/newconfig | /data/www/newhtml | tomcat |

| 进程关键词 | 启动脚本 | 识别名称 | 程序配置是否分离 | 开启发布 | 
| :---: | :--- | :--- | :--- | :--- | :--- | 
| tomcat-newserver | /etc/init.d/tomcat-newserver | newserver  | 是 | 是 | 
 
| 备份目录是否同级 | 发布目录是否同级 | 是否开启配置组 | 配置组|  
| :---: | :--- | :--- | :--- | :--- |
| 否 | 否 | 否  | 无 |

发布涉及四部曲
<1>. 同步集群应用、配置到本地
```
rex Enter:route:download --k='newserver'
```
>这个步骤会将远程服务器(127.0.0.1)上的程序和配置目录下载remotecomdir目录中
>
>程序下载路径: remotecomdir/softdir/newserver  
>配置下载路径: remotecomdir/configuredir/newserver  
>
>如果对应的是集群，比如[从案例开始](https://book.osichina.net/zidongfabu/conganlikaishi.html)中的server1,server2  
>server1,server2程序下载路径: remotecomdir/softdir/server1,remotecomdir/softdir/server2  
>server1,server2配置下载路径: remotecomdir/configuredir/server1,remotecomdir/configuredir/server2
>
>其实下载的规则就是根据数据库配置字段{app_key},  
>以及主配置文件字段的{local_prodir}和{local_confdir}  
>程序下载路径:{local_prodir}/{app_key}  
>配置下载路径:{local_confdir}/{app_key}  

<2>. 同步应用、配置到待发布目录
```
rex  Deploy:Core:syncpro --k='newserver'
```
>该步骤是将remotecomdir中newserver的程序和配置  
>同步到待发布目录softdir和configuredir目录中    
>这个步骤如果 程序配置是否分离 设置为是(is_deloy_dir=2)，  
>那么会直接将程序和配置移动  
>remotecomdir/softdir/newserver ---> softdir/newserver      
>remotecomdir/configuredir/newserver   ---> configuredir/newserver     
>
>如果是接入的应用类型为 [类型二: 配置和应用集成部署](https://book.osichina.net/zidongfabu/jieruxinyingyong.html)
>比如接入应用为案例中的combine
>这个时候程序配置是否分离 设置为否(is_deloy_dir=1)，  
>同时开启了配置组，configure_file_status=1，configure_file_list='......'   
>那么执行 将程序和配置移动和上面会有所区别  
>remotecomdir/softdir/combine ---> softdir/combine      
>remotecomdir/configuredir/combine/{applicationContext.xml,config.properties,generatorConfig.xml,log4j.properties}   ---> configuredir/combine  
>不同的是，这个步骤只会移动configure_file_list中配置的文件到待发布的配置目录

<3>. 上传修改代码或者修改配置文件   
在对应的程序配置目录上传和修改配置 
比如newserver应用：  
将要修改的文件覆盖到 softdir/newserver/目录中，修改configuredir/newserver下的配置文件   
如果是集群，它的配置目录为: configuredir/{local_name}/{app_key}  

<4>. 执行发布
```
rex  Enter:route:deploy --k='server1 server2'
```

这个过程当中，将程序包和配置包自动发布到远程服务器并执行一系列重启等操作。  
如果是 配置和应用集成部署 ，在这个过程当中会把configuredir目录下的配置合并  
到工程目录softdir目录中，然后在发布
