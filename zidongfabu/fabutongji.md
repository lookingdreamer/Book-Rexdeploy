# 发布统计

上一章节的案例，从执行的日志当中我们可以看到，在发布前、中、后会记录一些相关的发布信息。
发布相关的记录保存在pre_deploy_manage表中。
```
select * from pre_deploy_manage
```

发布的程序包大小,启动的时间，花费的时间扽等等，通过这些数据我们可以对每天/每周/每月，作出一份详细的发布统计。
每个月版本迭代次数等统计和分析。

![](https://book.osichina.net/assets/history.png)

