# 接入新应用
如果你也想把你的应用接入到自动发布当中来，那么你需要经过如下2个步骤。
- 一. 确认并修改你认证信息
- 二. 插入数据库记录

## 一. 确认并修改你认证信息
如果需要接入新的应用，那么你首先要做的就是，确认你的应用所在服务器是如何登陆认证。
认证的配置主要是通过主配置文件(config.yml)修改，在主配置文件修改的认证方式，是对全局生效的，也就是说
你执行任何操作，比如执行命令等，都是默认读取config.yml文件里面配置的认证信息。当然你也可以通过传参数或者
在分组中自定义认证信息，或者自定义认证信息(自定义这种情况很少用，在[批量执行命令](https://book.osichina.net/piliangzhixingmingling.html)中，我会详细介绍这种方式)。正常的情况下，
同一个环境，认证方式都是一致的。所以本章节主要针对的这种情况。同一个环境，认证方式一致的情况。
如果不同的环境，认证不一致，可以拷贝该程序目录，重新配置即可。

- 认证方式: 密码认证
```
 env:
   key: "dev"
 dev:
   key_auth: "false"
   pass_auth: "true"  
   user: "root"
   password: "autotask2015"
   private_key: ""
   public_key: ""
   global_sudo: "off"
   sudo_password: "" 
```
如果不是root用户，那就只有普通用户的权限。如果需要配置sudo权限，只需要将global_sudo设置为“on”
目前sudo_password不支持，后续有需要可以加上。也就是说目前如果是要开启sudo权限，请确保sudoers配置有
这个配置`普通用户	ALL=(ALL)	NOPASSWD: ALL`或者将这个用户加入wheel组，同时`%wheel	ALL=(ALL)	NOPASSWD: ALL`.
其实也就是一个要求，开启sudo的这个用户免密码登陆。

- 认证方式: 秘钥认证
```
 env:
   key: "dev"
 dev:
   key_auth: "true"
   pass_auth: "false"  
   user: "root"
   password: ""
   private_key: "keys/autotask"
   public_key: "keys/autotask.pub"
   global_sudo: "off"
   sudo_password: "" 
```
如果是普通用户需要配置sudo权限，也是同上的要求，开启sudo的这个用户免密码登陆。

## 二. 插入数据库记录
那么接下里就是为你的应用插入数据库记录，该步骤你也可以自行开发后台网页，可视化操作数据库，我这里主要讲数据层面。
应用的自动发布，主要依靠的是取数据库的记录，也即CMDB主机配置表(pre_server_detail)，具体字段的解释，请参考
[https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html](https://book.osichina.net/shu-ju-ku-biao-zi-duan/preserver-detail.html)

在常规的应用发布当中，按照配置部署类型方向，我们一般将应用分为2类。1类为配置和应用分开部署，1类为配置集成在应用当中。(还有1类原始类,[请原谅我
这么称呼]，参见类型三)当然我们强烈建议，应用和配置分开部署，这样的话，可以避免每次发版本配置覆盖或者混乱的问题，当然Rexdeploy也支持配置集成在应用当中
的情况。(如果你的是tomcat应用，你的配置集成在应用，你也想把配置和应用分开，你可以参考我的这篇文档，无需修改任何代码，通过tomcat配置，
[实现应用和配置分离](https://book.osichina.net/fu-lu/yingyonghepeizhifenli.md))

- **类型一: 配置和应用分开部署**

>新应用接入信息
<style type="text/css">
        table th  
        {  
            white-space: nowrap;  
        }  
        table td  
        {  
            white-space: nowrap;  
        }        
</style>

| 名字 | 服务名称描述 | 服务器 | 配置文件路径 | 工程文件路径 | 类型 |
| :---: | :--- | :--- | :--- | :--- | :--- | :--- |
| newserver | 新增测试服务 | 127.0.0.1  | /data/www/newconfig | /data/www/newhtml | tomcat |

| 进程关键词 | 启动脚本 | 识别名称 | 程序配置是否分离 | 开启发布 | 
| :---: | :--- | :--- | :--- | :--- | :--- | 
| tomcat-newserver | /etc/init.d/tomcat-newserver | newserver  | 是 | 是 | 
 
| 备份目录是否同级 | 发布目录是否同级 | 是否开启配置组 | 配置组|  
| :---: | :--- | :--- | :--- | :--- |
| 否 | 否 | 否  | 无 |


(1).上SQL
```
INSERT INTO `pre_server_detail` 
(`app_key`, `server_name`, `network_ip`, `pro_type`, `config_dir`, `pro_dir`, `pro_key`, `pro_init`, `local_name`, `is_deloy_dir`, `auto_deloy`, `backupdir_same_level`, `deploydir_same_level`, `configure_file_status`, `configure_file_list`) VALUES 
('newserver', '新增测试服务', '127.0.0.1', 'tomcat', '/data/www/newconfig', '/data/www/newhtml', 'tomcat-newserver', '/etc/init.d/tomcat-newserver', 'newserver', '2', '1', 0, 0, 0, NULL);
```
(2).字段解释

* [x] app_key
 ```
 应用发布的唯一名字关键词,不能有重复,不能为空,如果为空,则不会加入到自动发布的系统里面.
 ```
- server_name
 ```
 应用的服务器名称或者描述，知道这个应用是什么
 ```
* [x] network_ip
 ```
 应用的服务器地址,内网优先
 ```
- pro_type
 ```
 应用的类型
 ```
* [x] config_dir
 ```
 应用的配置目录
 ```
* [x] pro_dir
 ```
 应用的工程路径
 ```
* [x] pro_key: 
```
进程关键字最好选择的唯一的关键词,在关闭应用失败的时候,会通过应用关键词去KILL应用
```
* [x] pro_init: 
```
应用启动脚本，如果需要传参，该参数只能是start，且是唯一的
```
* [x] local_name: 
```
应用的识别名称(分组名称)，这个识别名称主要是用来区分集群，如案例中server应用，
当server1,server2的local_name设置为server的时候，则这2个应用使用同一个程序包
```
* [x] is_deloy_dir: 
```
发布目录判断目前支持1和2
2代表工程路径和配置路径是隔离开来的,比如:server1的工程路径为: /data/www/html1 配置路径为: /data/www/config1;
1代表工程路径和配置路径是合在一起的,比如flow1,它的工程路径为/data/www/flow1,配置路径为: /data/www/flow1/WEB-INF/classes/
```
* [x] auto_deloy: 
```
是否加入自动发布，1加入，0不加入
```
- backupdir_same_level: 
```
应用程序备份是否在该应用的同1层级，0否(缺省建议)，1是
该配置项只有is_deloy_dir=1时才成立
比如案例中的flow1(工程路径:/data/www/flow1,配置路径为: /data/www/flow1/WEB-INF/classes/),我们会对发布前的程序进行备份。
(1)如果backupdir_same_level=0，则/data/www/flow1
将会备份在 /data/backup/flow/[时间]/ 目录下，/data/backup这个目录是在主配置
文件backup_dir定义的,flow是从数据库中取的{local_name}。
(2)如果backupdir_same_level=1，应用程序将会备份在/data/www/ 这个路径下，和
/data/www/flow1实在同一层级
```
- deploydir_same_level: 
```
应用发布程序是否在该应用的同1层级，0否(缺省建议)，1是
该配置项只有is_deloy_dir=1时才成立
比如案例中的flow1(工程路径:/data/www/flow1,配置路径为: /data/www/flow1/WEB-INF/classes/)。
(1)如果deploydir_same_level=0，则新发布的程序包
将会在 /data/www/apps 目录下，/data/www/apps 这个目录是在主配置
文件baseapp_dir定义的,同时会建立一个软链接指向 /data/www/flow1。
(2)如果deploydir_same_level=1，则新发布的程序包将会在/data/www/ 这个路径下，和
/data/www/flow1实在同一层级，同时会建立一个软链接指向 /data/www/flow1
```
- configure_file_status: 
```
是否分离配置文件 0:拷贝整个配置目录 1读取configure_file_list的配置列表
这个配置项主要是针对is_deloy_dir=1时，配置在程序包里面的情况,比如flow1的
/data/www/flow1/WEB-INF/classes/，
(1)如果configure_file_status=1，这个目录下有4个配置文件(applicationContext.xml config.properties generatorConfig.xml log4j.properties)，1个com的代码目录。
我实际使用的配置只有以上4个配置文件，我并不需要com目录，同时也避免原有代码
覆盖的情况。这个时候我就可以configure_file_status=applicationContext.xml,config.properties,generatorConfig.xml,log4j.properties，当我们执行同步应用、配置到待发布目录(rex  Deploy:Core:syncpro --k='flow1')，这个操作会抽取remotecomdir下对
应的4个配置文件到待发布的配置目录中configuredir中。
(2)如果configure_file_status=0 则会拷贝整个配置目录中configuredir中 
```
- configure_file_list: 
```
分离配置文件组，多个文件以，分隔
```

(3).接入集群

如果接入是集群的话，比如如上newserver有2个以上，我们如何写入数据库配置？
>答: 与上唯一不同的是，将app_key配置成newserver1,newserver2.....newserver{N}
>或者定义成你自定义的不同的app_key.其他配置项保持不变(当然你的IP(network_ip)
>可能有变化)。如果你的集群都是部署在同一台服务器也是可以的，同一个集群，
>保证app_key是唯一，同时local_name是一致的即可。

- **类型二: 配置和应用集成部署**

>新应用接入信息

| 服务名称描述  | 类型 |
| :--- | :--- | 
| 集成部署服务 | tomcat |

| 名字  | 服务器 | 配置文件路径 | 工程文件路径 |
| :---: | :--- | :--- | :--- | 
| combine | 127.0.0.1  | /data/www/combine/WEB-INF/classes | /data/www/combine | 

| 进程关键词 | 启动脚本 | 识别名称 | 程序配置是否分离 | 开启发布 | 
| :---: | :--- | :--- | :--- | :--- | :--- | 
| tomcat-combine | /etc/init.d/tomcat-combine | combine  | 否 | 是 | 
 
| 备份目录是否同级 | 发布目录是否同级 | 是否开启配置组 | 配置组|  
| :---: | :--- | :--- | :--- | :--- |
| 否 | 否 | 是  | 有 |



(1).上SQL
```
INSERT INTO `pre_server_detail` 
(`app_key`, `server_name`, `network_ip`, `pro_type`, `config_dir`, `pro_dir`, `pro_key`, `pro_init`, `local_name`, `is_deloy_dir`, `auto_deloy`, `backupdir_same_level`, `deploydir_same_level`, `configure_file_status`, `configure_file_list`) VALUES 
('combine', '集成部署服务', '127.0.0.1', 'tomcat', '/data/www/combine/WEB-INF/classes', '/data/www/combine', 'tomcat-combine', '/etc/init.d/tomcat-combine', 'combine', '1', '1', 0, 0, 1, 'applicationContext.xml,config.properties,generatorConfig.xml,log4j.properties');
```

(2).字段解释
> 字段解释如上   
> 和类型一唯一不同的是，这个程序包中包含配置文件，所以is_deloy_dir=1，同时configure_file_status=1，configure_file_list为配置文件列表，这个配置文件列表是相对于
> config_dir下的路径。

(3).接入集群

如果接入是集群的话，比如如上combine有2个以上，我们如何写入数据库配置？
>答: 与上唯一不同的是，将app_key配置成combine1,combine2.....combine{N}
>或者定义成你自定义的不同的app_key.其他配置项保持不变(当然你的IP(network_ip)
>可能有变化)。如果你的集群都是部署在同一台服务器也是可以的，同一个集群，
>保证app_key是唯一，同时local_name是一致的即可。

- **类型三: 原始配置应用集成部署**

>新应用接入信息

| 服务名称描述  | 类型 |工程文件路径|
| :--- | :--- | :--- | 
| 原始集成服务 | tomcat |  /usr/local/tomcat-original/webapps/original |

| 名字  | 服务器 | 配置文件路径  | 
| :---: | :--- | :--- | :--- | 
| original | 127.0.0.1  | /usr/local/tomcat-original/webapps/original/WEB-INF/classes/ | 

| 进程关键词 | 启动脚本 | 识别名称 | 程序配置是否分离 | 开启发布 | 
| :---: | :--- | :--- | :--- | :--- | :--- | 
| tomcat-original | /etc/init.d/tomcat-original | original  | 否 | 是 | 
 
| 备份目录是否同级 | 发布目录是否同级 | 是否开启配置组 | 配置组|  
| :---: | :--- | :--- | :--- | :--- |
| 否 | 否 | 是  | 有 |

(1).上SQL
```
INSERT INTO `pre_server_detail` 
(`app_key`, `server_name`, `network_ip`, `pro_type`, `config_dir`, `pro_dir`, `pro_key`, `pro_init`, `local_name`, `is_deloy_dir`, `auto_deloy`, `backupdir_same_level`, `deploydir_same_level`, `configure_file_status`, `configure_file_list`) VALUES 
('original', '原始集成服务', '127.0.0.1', 'tomcat', '/usr/local/tomcat-original/webapps/original/WEB-INF/classes/', '/usr/local/tomcat-original/webapps/original', 'tomcat-original', '/etc/init.d/tomcat-original', 'original', '1', '1', 0, 0, 1, 'applicationContext.xml,config.properties,generatorConfig.xml,log4j.properties');
```

(2).字段解释  
> 字段解释如上   
> 和类型一唯一不同的是，这个程序包中包含配置文件，所以is_deloy_dir=1，同时configure_file_status=1，configure_file_list为配置文件列表，这个配置文件列表是相对于
> config_dir下的路径。
> 同时需要注意的是 这个工程是部署在webapps目录，所以对这个应用做备份和发布的时候的时候不能在webapps目录下操作，不然tomcat会同时启动多个相同的应用。也就是说
> backupdir_same_level和deploydir_same_level一定要配置为0

(3).接入集群

如果接入是集群的话，比如如上original有2个以上，我们如何写入数据库配置？
>答: 与上唯一不同的是，将app_key配置成original1,original2.....original{N}
>或者定义成你自定义的不同的app_key.其他配置项保持不变(当然你的IP(network_ip)
>可能有变化)。如果你的集群都是部署在同一台服务器也是可以的，同一个集群，
>保证app_key是唯一，同时local_name是一致的即可。








