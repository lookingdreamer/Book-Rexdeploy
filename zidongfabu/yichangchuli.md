# 异常处理

## 发布过程中意外终止？

比如你在执行发布的过程，不小心取消了执行，再次执行出现以下提示。
![](https://book.osichina.net/assets/deploying.png)
这是因为每次对该应用发布时,都会对该应用加1个状态锁，防止正在发布
的应用重复发布，当你取消或者异常停止的时候，如果再次执行出现以上的
提示的话,那么该应用还是锁住的状态，不能再继续发布。
如果你需要再次执行发布的话，请执行初始化应用操作。
```
root@914f8eee810c RexDeployV3]# rex Deploy:Db:initdb
[2018-03-29 09:16:45] INFO - Running task Deploy:Db:initdb on <local>
[2018-03-29 09:16:45] INFO - 初始化发布状态完成.
[2018-03-29 09:16:46] INFO - All tasks successful on all hosts

```


## 开启调试模式

如果你想知道perl底层到底在干啥，可以在执行命令的时候加上 -d 参数。
该参数需要加载模块之前。
比如
```
rex -d run --k='server1' --cmd='uptime'
```
